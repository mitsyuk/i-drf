package org.processmining.iskra.dialogs;

import org.deckfour.uitopia.api.event.TaskListener;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.iskra.infrastucture.SlideShowCallback;
import org.processmining.iskra.infrastucture.SlideShowEnd;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by user on 08.07.14.
 */
public class SlideShower {
    public static boolean show(UIPluginContext context, List<JComponent> Show, SlideShowCallback callback) {
        ListIterator<JComponent> iterator = Show.listIterator();
        boolean finished = false;
        JComponent stepPanel = iterator.next();
        do {

            TaskListener.InteractionResult interactionResult = null != stepPanel ? context.showWizard("SlideShow",
                    !iterator.hasNext(), true, stepPanel) : TaskListener.InteractionResult.NEXT;
            callback.itHappens(stepPanel);
            switch (interactionResult) {
                case FINISHED:
                case NEXT:
                    if (iterator.hasNext()) {
                        stepPanel = iterator.next();
                    } else {
                        finished = true;
                    }
                    break;
                case PREV:
                    if (iterator.hasPrevious()) {
                        stepPanel = iterator.previous();
                        break;
                    }
                case CANCEL:
                    return false;
                /*case FINISHED:
                    finished = true;*/
            }
        } while (!finished);
        return true;
    }

    public static JPanel getNestedPanelShower(final List<JComponent> show, final SlideShowEnd onCancel, final SlideShowCallback onChange, final SlideShowEnd onFinish) {
        return new JPanel() {
            {
                setLayout(new BorderLayout());
                final JPanel space = new JPanel(new BorderLayout());
                space.setPreferredSize(new Dimension(640, 480));
                JPanel controls = new JPanel();
                controls.setLayout(new BoxLayout(controls, BoxLayout.X_AXIS));
                space.setBackground(Color.red);
                JButton back = new JButton();
                JButton forward = new JButton();
                back.setPreferredSize(new Dimension(100, 30));
                forward.setPreferredSize(new Dimension(100, 30));
                controls.add(Box.createHorizontalGlue());
                controls.add(back);
                controls.add(Box.createHorizontalStrut(10));
                controls.add(forward);
                controls.add(Box.createHorizontalGlue());
                add(space, BorderLayout.CENTER);
                add(controls, BorderLayout.PAGE_END);
                final ListIterator<JComponent> iterator = show.listIterator();
                space.add(iterator.next());
                back.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        swapViews(iterator, false, space, onCancel);
                    }
                });
                forward.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        onChange.itHappens(space.getComponent(0));
                        swapViews(iterator, true, space, onFinish);
                    }
                });
            }
        };
    }

    private static void swapViews(ListIterator<JComponent> iterator, boolean forward, JComponent space, SlideShowEnd callback) {
        Component tmp = space.getComponent(0);
        space.remove(tmp);
        tmp = null;
        while ((forward && iterator.hasNext()) || ((!forward) & iterator.hasPrevious())) {
            tmp = forward ? iterator.next() : iterator.previous();
            if (tmp != null) {
                break;
            }
        }
        if (tmp != null) {
            space.add(tmp);
            tmp.setVisible(true);
            space.revalidate();
        } else {
            callback.itHappens();
        }
    }
}
