package org.processmining.iskra.infrastucture;

/**
 * Timing calculations base interface
 *
 * @author Alexey Mitsyuk
 */

public interface IskraTimingResult {

    long getDecompositionTime();

    long getPartialConformanceCheckingTime();

    long getRepairTime();

    long getCompositionTime();

    long getFinalEvaluationTime();

    long getTotalTime();

}