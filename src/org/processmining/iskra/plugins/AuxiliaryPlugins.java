package org.processmining.iskra.plugins;

import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.iskra.pluginwraps.repair.impl.ILPRepairer;
import org.processmining.iskra.pluginwraps.repair.impl.InductiveRepairer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

/**
 * Created by Ivan on 17.07.2016.
 */
public class AuxiliaryPlugins {
    @Plugin(name = "Apply timed inductive miner", returnLabels = "Net", returnTypes = Petrinet.class, parameterLabels = "Log")
    @UITopiaVariant(affiliation = "", email = "", author = "")
    public Petrinet mineInductive(UIPluginContext context, XLog log)
    {
        InductiveRepairer repairer = new InductiveRepairer();
        return repairer.repair(context, log);
    }

    @Plugin(name = "Apply timed ILP miner", returnLabels = "Net", returnTypes = Petrinet.class, parameterLabels = "Log")
    @UITopiaVariant(affiliation = "", email = "", author = "")
    public Petrinet mineILP(UIPluginContext context, XLog log)
    {
        ILPRepairer repairer = new ILPRepairer();
        return repairer.repair(context, log);
    }

    @Plugin(name = "print double", returnLabels = {}, returnTypes = {}, parameterLabels = "double")
    @UITopiaVariant(affiliation = "", email = "", author = "")
    public void printDouble(UIPluginContext context, Double value)
    {
        String string = value.toString();
        string = string.replace('.', ',');
        System.out.println("value: " + string);
    }

    @Plugin(name = "measure model", returnLabels = {}, returnTypes = {}, parameterLabels = "Net")
    @UITopiaVariant(affiliation = "", email = "", author = "")
    public void measureModel(UIPluginContext context, Petrinet petrinet)
    {
        System.out.println("Net size: " + petrinet.getNodes().size());
    }
}
