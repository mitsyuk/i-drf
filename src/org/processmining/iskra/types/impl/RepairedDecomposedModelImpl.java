package org.processmining.iskra.types.impl;

import org.processmining.iskra.types.RepairedDecomposedModel;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.log.models.EventLogArray;

public class RepairedDecomposedModelImpl extends DecomposedModelImpl implements RepairedDecomposedModel {

    private AcceptingPetriNetArray netArray;

    public RepairedDecomposedModelImpl(AcceptingPetriNetArray modelParts, EventLogArray logParts, AcceptingPetriNetArray netArray/*TODO последний аргумент исключительно для тестов*/) {
        super(modelParts, logParts);
        this.netArray = netArray;
    }

    @Override
    public AcceptingPetriNetArray getNetsAfterRepairing() {
        return netArray;
    }

}
