package org.processmining.iskra.infrastucture.impl;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;

/**
 * Created by Ivan on 18.04.2016.
 */
public class WrappingResult {
    private final AcceptingPetriNetArray badParts;
    private final AcceptingPetriNetArray otherParts;

    public WrappingResult(AcceptingPetriNetArray badParts, AcceptingPetriNetArray otherParts) {
        this.badParts = badParts;
        this.otherParts = otherParts;
    }

    public AcceptingPetriNetArray getOtherParts() {
        return otherParts;
    }

    public AcceptingPetriNetArray getBadParts() {
        return badParts;
    }
}
