package org.processmining.iskra.types;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.iskra.infrastucture.IskraTimingResult;
import org.processmining.log.models.EventLogArray;

/**
 * Created by Ivan Shugurov on 05.11.2014.
 */
public class IskraResultBuilder {

    private AcceptingPetriNet initialModel;
    private AcceptingPetriNet finalModel;
    private FinalEvaluationResult evaluationResult;
    private IskraTimingResult timingResult;
    private String decomposerName;
    private String repairerName;
    private AcceptingPetriNetArray decomposedNets;
    private EventLogArray decomposedLogs;
    private AcceptingPetriNetArray reminedNets;

    public IskraResultBuilder() {
    }

    public IskraResultBuilder initialModel(AcceptingPetriNet initialModel) {
        this.initialModel = initialModel;
        return this;
    }

    public IskraResultBuilder finalModel(AcceptingPetriNet finalModel) {
        this.finalModel = finalModel;
        return this;
    }

    public IskraResultBuilder evaluationResult(FinalEvaluationResult evaluationResult) {
        this.evaluationResult = evaluationResult;
        return this;
    }

    public IskraResultBuilder timingResult(IskraTimingResult timingResult) {
        this.timingResult = timingResult;
        return this;
    }

    public IskraResultBuilder decomposerName(String decomposerName) {
        this.decomposerName = decomposerName;
        return this;
    }

    public IskraResultBuilder repairerName(String repairerName) {
        this.repairerName = repairerName;
        return this;
    }

    public IskraResultBuilder decomposedNets(AcceptingPetriNetArray decomposedNets) {
        this.decomposedNets = decomposedNets;
        return this;
    }

    public IskraResultBuilder decomposedLogs(EventLogArray decomposedLogs) {
        this.decomposedLogs = decomposedLogs;
        return this;
    }

    public IskraResultBuilder reminedNets(AcceptingPetriNetArray reminedNets) {
        this.reminedNets = reminedNets;
        return this;
    }

    public IskraResult build() {
        return new IskraResultImpl(initialModel, finalModel, evaluationResult, timingResult, decomposerName, repairerName, decomposedNets, decomposedLogs, reminedNets);
    }

    private static class IskraResultImpl implements IskraResult {

        private final AcceptingPetriNet initialModel;
        private final AcceptingPetriNet finalModel;
        private final FinalEvaluationResult evaluationResult;
        private final IskraTimingResult timingResult;
        private final String decomposerName;
        private final String repairerName;
        private final AcceptingPetriNetArray decomposedNets;
        private final EventLogArray decomposedLogs;
        private AcceptingPetriNetArray reminedNets;

        private IskraResultImpl(AcceptingPetriNet acceptingPetriNet, AcceptingPetriNet acceptingPetriNet2,
                                FinalEvaluationResult evaluationResult,
                                IskraTimingResult timingResult,
                                String decomposerName, String repairerName,
                                AcceptingPetriNetArray decomposedNets,
                                EventLogArray decomposedLogs, AcceptingPetriNetArray reminedNets /*TODO размайненные сети исключительно для тестов*/) {
            this.initialModel = acceptingPetriNet;
            this.finalModel = acceptingPetriNet2;
            this.evaluationResult = evaluationResult;
            this.timingResult = timingResult;
            this.decomposerName = decomposerName;
            this.repairerName = repairerName;
            this.decomposedNets = decomposedNets;
            this.decomposedLogs = decomposedLogs;
            this.reminedNets = reminedNets;
        }

        @Override
        public IskraTimingResult getTimingResult() {
            return timingResult;
        }

        @Override
        public AcceptingPetriNet getInitialModel() {
            return initialModel;
        }

        @Override
        public AcceptingPetriNet getFinalModel() {
            return finalModel;
        }

        @Override
        public FinalEvaluationResult getEvaluationResult() {
            return evaluationResult;
        }

        @Override
        public String getDecomposerName() {
            return decomposerName;
        }

        @Override
        public String getRepairerName() {
            return repairerName;
        }

        @Override
        public AcceptingPetriNetArray getDecomposedNets() {
            return decomposedNets;
        }

        @Override
        public EventLogArray getDecomposedLogs() {
            return decomposedLogs;
        }

        @Override
        public AcceptingPetriNetArray getReminedNets() {
            return reminedNets;
        }

    }
}
