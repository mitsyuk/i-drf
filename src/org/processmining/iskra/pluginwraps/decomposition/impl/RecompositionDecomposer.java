package org.processmining.iskra.pluginwraps.decomposition.impl;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.DecomposedModel;

/**
 * Created by Ivan Shugurov on 10.11.2014.
 */
@KeepInProMCache
public class RecompositionDecomposer extends IskraDecomposer {
    public RecompositionDecomposer() {
        super("Recomposer");
    }

    @Override
    public DecomposedModel decompose(PluginContext context, ComposedModel initialModel) {

        /*org.processmining.acceptingpetrinet.models.AcceptingPetriNet initialAcceptingNet = initialModel.getNet();
        Petrinet initialPetrinet = initialAcceptingNet.getNet();

        AcceptingPetriNet renamedAcceptingNet = copyNetAddingPluses(initialPetrinet);

        CreateFromAcceptingPetriNetPlugin matrixPlugin = new CreateFromAcceptingPetriNetPlugin();
        CausalActivityMatrix causalActivityMatrix = matrixPlugin.runDefault(context, renamedAcceptingNet);

        WeightedCausalActivityGraph weightedCausalActivityGraph = ConvertCausalActivityMatrixToWeightedCausalActivityGraphPlugin.convertDefault(context, causalActivityMatrix);

        ConvertCausalActivityMatrixToCausalActivityGraphPlugin causalActivityGraphPlugin = new ConvertCausalActivityMatrixToCausalActivityGraphPlugin();
        CausalActivityGraph causalActivityGraph = causalActivityGraphPlugin.runDefault(context, causalActivityMatrix);

        ConvertCausalActivityGraphToActivityClusterArrayPlugin activityClusterArrayPlugin = new ConvertCausalActivityGraphToActivityClusterArrayPlugin();
        ActivityClusterArray activityClusterArray = activityClusterArrayPlugin.runDefault(context, causalActivityGraph);

        RecomposeActivityClusterArrayPlugin recompositionPlugin = new RecomposeActivityClusterArrayPlugin();
        ActivityClusterArray recomposerActivityClustterArray = recompositionPlugin.recomposeDefault(context, activityClusterArray, weightedCausalActivityGraph);
        AcceptingPetriNetArray decomposedNet = new DecomposeAcceptingPetriNetUsingActivityClusterArrayPlugin().runDefault(context, renamedAcceptingNet, recomposerActivityClustterArray);


        //finished decomposing the net, starting to decompose logs
        DecomposeEventLogUsingActivityClusterArrayPlugin logDecomposePlugin = new DecomposeEventLogUsingActivityClusterArrayPlugin();
        EventLogArray decomposedLogs = logDecomposePlugin.runDefault(context, initialModel.getLog(), recomposerActivityClustterArray);  TODO спользуются устаревшие классы*/

        //return new DecomposedModelImpl(decomposedNet, decomposedLogs);
        return null;
    }
}
