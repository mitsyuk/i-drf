package org.processmining.iskra.infrastucture.impl;

import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.infrastucture.Chain;
import org.processmining.iskra.infrastucture.IskraTimingResult;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluation;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.*;
import org.processmining.iskra.types.impl.ComposedModelImpl;
import org.processmining.log.models.impl.EventLogArrayFactory;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

import java.util.ArrayList;
import java.util.List;

import static org.processmining.iskra.utils.AcceptingPetriNetMerger.mergePetriNetArray;
import static org.processmining.iskra.utils.BoundryPlacesFinder.checkPresenceOfBoundaryPlaces;
import static org.processmining.iskra.utils.LogProjection.projectNetOnLog;
import static org.processmining.iskra.utils.RemovalOfBoundaryPlaces.removeBoundaryPlaces;

/**
 * Created by Ivan on 07.07.2016.
 */
@KeepInProMCache
@Chain(NaiveRepairChainWithWrappers.CHAIN_NAME)
public class NaiveRepairChainWithWrappers extends AdvancedGreedyIskra {
    public static final String CHAIN_NAME = "Naive with wrappers (Advanced Repair)";

    public NaiveRepairChainWithWrappers(IskraDecomposer decomposer, IskraRepairer repairer, IskraFinalEvaluation evaluator,
                                        ConformanceCheckingUsingAlignments conformanceChecker,
                                        Metrics metrics, double fitnessThreshold) {
        super(decomposer, repairer, evaluator, conformanceChecker, metrics, fitnessThreshold);
    }

    @Override
    public IskraResult execute(PluginContext context, ComposedModel initialModel) throws Exception {
        //double initialConformance = measureConformance(initialModel);

        int numberOfWrappers = 0;
        int totalNumberOfFragments = 0;
        int totalSizeOfBadWrappers = 0;

        long start = System.currentTimeMillis();

        ComposedModel changeableModel = new ComposedModelImpl(initialModel.getNet(), initialModel.getLog());

        // if (initialConformance < fitnessThreshold) {
            /*if (USE_PROVIDED_OBJECTS) {
                System.out.println("Initial conformance: " + initialConformance);
            }     */

        BadPartsWrapper wrapper = new BadPartsWrapper();

        if (USE_PROVIDED_OBJECTS) {
            System.out.println("before decomposition on step ");
        }

        DecomposedModel decomposedModel = decomposer.decompose(context, changeableModel);
        org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray decomposedNets = decomposedModel.getNetArray();
        totalNumberOfFragments = decomposedModel.getSize();

        if (USE_PROVIDED_OBJECTS) {
            System.out.println("after decomposition on step ");

            addDecompositionToProvidedObjects(
                    context, decomposedNets, decomposedModel.getLogArray(),
                    "Net decomposition",
                    "Log decomposition");
        }

        DecomposedConformanceResult decomposedConformanceResult = measureDecomposedConformance(decomposedModel);

        if (USE_PROVIDED_OBJECTS) {
            printConformanceOfDecompositionParts(0, decomposedConformanceResult);
        }

        List<AcceptingPetriNet> badParts = new ArrayList<>();

        for (int i = 0; i < decomposedNets.getSize(); i++) {
            if (decomposedConformanceResult.isBadPart(i)) {
                badParts.add(decomposedNets.getNet(i));
            }
        }

        Petrinet changeablePetrinet = changeableModel.getNet().getNet();

        addPetrinetWithHighlightedBadPartsToProvidedObjects(context, changeablePetrinet, badParts, 0, true);

        WrappingResult wrappingResult = wrapper.wrapBadParts(context, decomposedNets, badParts);
        AcceptingPetriNetArray wrappers = wrappingResult.getBadParts();

        numberOfWrappers = wrappers.getSize();

        if (USE_PROVIDED_OBJECTS) {
            addAcceptingPetriNetArrayToProvidedObjects(context, wrappers, "Wrappers on step " + 0);
        }

        XLog originalLog = initialModel.getLog();
        AcceptingPetriNetArray netParts = wrappingResult.getOtherParts();

        for (int i = 0; i < wrappers.getSize(); i++) {
            AcceptingPetriNet acceptingNet = wrappers.getNet(i);
            Petrinet petrinet = acceptingNet.getNet();
            totalSizeOfBadWrappers += petrinet.getNodes().size();
            XLog projection = projectNetOnLog(petrinet, originalLog);

            Pair<Boolean, Boolean> presenceOfBoundaryPlaces = checkPresenceOfBoundaryPlaces(petrinet);
            boolean containsInitialPlaces = presenceOfBoundaryPlaces.getFirst();
            boolean containsFinalPlaces = presenceOfBoundaryPlaces.getSecond();

            Petrinet repairedWrapper = repairer.repair(context, projection);

            removeBoundaryPlaces(repairedWrapper, containsInitialPlaces, containsFinalPlaces);
            AcceptingPetriNet repairedAcceptingNet = AcceptingPetriNetFactory.createAcceptingPetriNet(repairedWrapper);

            netParts.addNet(repairedAcceptingNet);

            if (USE_PROVIDED_OBJECTS) {
                addPetriNetToProvidedObjects(context, repairedWrapper, "Repaired wrapper № " + i, false);
            }
        }

        AcceptingPetriNet mergedNet = mergePetriNetArray(context, netParts);
        //handleBoundaryPlaces(mergedNet);

        changeableModel = new ComposedModelImpl(mergedNet, originalLog);
        // }

        long end = System.currentTimeMillis();

        copyNetRemovingPluses(changeableModel.getNet().getNet());

        System.out.println("total number of fragments: " + totalNumberOfFragments);
        System.out.println("total number of bad wrappers: " + numberOfWrappers);
        System.out.println("Total time: " + (end - start));
        System.out.println("Size of final model: " + changeableModel.getNet().getNet().getNodes().size());
        System.out.println("Total size of wrappers: " + totalSizeOfBadWrappers);

        IskraTimingResultBuilder timingResultBuilder = new IskraTimingResultBuilder();
        IskraTimingResult timingResult = timingResultBuilder.build();

        IskraResultBuilder resultBuilder = new IskraResultBuilder();
        resultBuilder.decomposerName(decomposer.getPluginName()).repairerName(repairer.getPluginName());

        System.out.println("Before final evaluation");
        FinalEvaluationResult evaluationResult =
                evaluator.evaluate(context, initialModel, changeableModel, timingResultBuilder, CHAIN_NAME);
        System.out.println("After final evaluation");

        resultBuilder.initialModel(initialModel.getNet()).finalModel(changeableModel.getNet()).
                evaluationResult(evaluationResult).timingResult(timingResult).
                decomposedNets(AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray()).decomposedLogs(
                EventLogArrayFactory.createEventLogArray()).reminedNets(AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray());

        return resultBuilder.build();
    }
}
