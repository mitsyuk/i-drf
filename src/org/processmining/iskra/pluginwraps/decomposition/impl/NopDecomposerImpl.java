package org.processmining.iskra.pluginwraps.decomposition.impl;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.log.models.EventLogArray;
import org.processmining.log.models.impl.EventLogArrayFactory;

@KeepInProMCache
public class NopDecomposerImpl extends IskraDecomposer {
    public NopDecomposerImpl() {
        super("Stub decomposer");
    }

    @Override
    public DecomposedModel decompose(PluginContext context, ComposedModel initialModel) {
        org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray nets = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();
        EventLogArray events = EventLogArrayFactory.createEventLogArray();
        AcceptingPetriNet macroNet = copyNetAddingPluses(initialModel.getNet().getNet());
        nets.addNet(macroNet);
        events.addLog(initialModel.getLog());
        return new DecomposedModelImpl(nets, events);
    }
}
