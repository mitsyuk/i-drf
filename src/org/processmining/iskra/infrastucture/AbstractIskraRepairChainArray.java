package org.processmining.iskra.infrastucture;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;
import org.processmining.iskra.types.AbstractIskraCombinedResult;
import org.processmining.iskra.types.ComposedModel;

import java.util.List;

/**
 * Idea of chain pack
 *
 * @author Dmitriy Yakovlev
 */
public interface AbstractIskraRepairChainArray {
    /**
     * Method for retrieving pack
     *
     * @return current pack
     */
    List<IskraRepairChainImpl> getPack();

    /**
     * Executes inner chain pack on given parameters
     *
     * @param context          Prom context for communicating with framework
     * @param initialModel     PetriNet + XES log on chains should be executed
     * @param fitnessThreshold obviously?
     * @return combined chain result
     */
    AbstractIskraCombinedResult execute(PluginContext context,
                                        ComposedModel initialModel,
                                        double fitnessThreshold) throws Exception;
}
