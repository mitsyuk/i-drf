package org.processmining.iskra.utils;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ivan on 18.04.2016.
 */
public class PlusRemoval {
    public static Petrinet removePluses(Petrinet petrinet) {
        Petrinet netWithoutPluses = PetrinetFactory.newPetrinet("");

        Map<Transition, Transition> oldTransitionsToNewTransitions = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            String newLabel = removePlus(transition.getLabel());

            Transition newTransition = netWithoutPluses.addTransition(newLabel);
            newTransition.setInvisible(transition.isInvisible());

            oldTransitionsToNewTransitions.put(transition, newTransition);
        }

        for (Place place : petrinet.getPlaces()) {
            Place newPlace = netWithoutPluses.addPlace(place.getLabel());

            for (PetrinetEdge<?, ?> inEdge : petrinet.getInEdges(place)) {
                Transition precedingTransition = (Transition) inEdge.getSource();
                Transition newPrecedingTransition = oldTransitionsToNewTransitions.get(precedingTransition);

                netWithoutPluses.addArc(newPrecedingTransition, newPlace);
            }

            for (PetrinetEdge<?, ?> outEdge : petrinet.getOutEdges(place)) {
                Transition succeedingTransition = (Transition) outEdge.getTarget();
                Transition newSucceedingTransition = oldTransitionsToNewTransitions.get(succeedingTransition);

                netWithoutPluses.addArc(newPlace, newSucceedingTransition);
            }
        }

        return netWithoutPluses;
    }

    public static AcceptingPetriNet removePluses(AcceptingPetriNet net)
    {
        Petrinet petrinet = removePluses(net.getNet());

        return AcceptingPetriNetFactory.createAcceptingPetriNet(petrinet);
    }

    public static String removePlus(String label) {
        int plusIndex = label.indexOf("+");
        if (plusIndex < 0) {
            return label;
        } else {
            return label.substring(0, plusIndex);
        }
    }
}
