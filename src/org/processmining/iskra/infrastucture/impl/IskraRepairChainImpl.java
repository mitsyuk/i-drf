package org.processmining.iskra.infrastucture.impl;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.infrastucture.AbstractRepairChain;
import org.processmining.iskra.infrastucture.Chain;
import org.processmining.iskra.infrastucture.IskraTimingResult;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluation;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.*;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

import static org.processmining.iskra.utils.DanglingTransitionsRemover.removeDanglingTransitions;

/**
 * The main idea of repair chain
 *
 * @author Alexey Mitsyuk
 */
@KeepInProMCache
@Chain(IskraRepairChainImpl.CHAIN_NAME)
public class IskraRepairChainImpl extends AbstractRepairChain {

    public static final String CHAIN_NAME = "Naive chain (Naive Repair)";

    public IskraRepairChainImpl(
            IskraDecomposer decomposer, IskraRepairer repairer,
            IskraFinalEvaluation evaluator,
            ConformanceCheckingUsingAlignments conformanceChecker,
            Metrics metrics, double fitnessThreshold) {
        super(decomposer, repairer, evaluator, conformanceChecker, metrics, fitnessThreshold);
    }

    @Override
    public IskraResult execute(PluginContext context,
                               ComposedModel initialModel) throws Exception {


        if (USE_PROVIDED_OBJECTS) {
            System.out.println("Naive chain");
        }

        context.getProgress().setIndeterminate(true);

        long start = System.currentTimeMillis();

        IskraResultBuilder resultBuilder = new IskraResultBuilder();
        resultBuilder.decomposerName(decomposer.getPluginName()).repairerName(repairer.getPluginName());

      // timingResultBuilder.decompositionStart();
        DecomposedModel decomposedModel = decomposer.decompose(context, initialModel);
       // timingResultBuilder.decompositionEnd();

        if (USE_PROVIDED_OBJECTS) {
            addDecompositionToProvidedObjects(context, decomposedModel.getNetArray(), decomposedModel.getLogArray(), "decomposed net", "decomposed log");
        }

        //timingResultBuilder.partialConformanceCheckingStart();
        DecomposedConformanceResult selection = measureDecomposedConformance(decomposedModel);
        //timingResultBuilder.partialConformanceCheckingEnd();

        //timingResultBuilder.repairStart();
        RepairedDecomposedModel repairedDecomposedModel = repairer.repair(context, decomposedModel, selection);
        //timingResultBuilder.repairEnd();


        //timingResultBuilder.compositionStart();
        ComposedModel finalModel = decomposer.compose(context, repairedDecomposedModel, initialModel.getLog());
        //timingResultBuilder.compositionEnd();

        // here we remove places with same pre- and post- transitions
        /*Set<Place> places = new HashSet<>(finalModel.getNet().getNet().getPlaces());

        Set<Place> placesToRemove = new HashSet<>();
        for (Place place : places) {
            for (Place place2 : places) {

                if (!place.equals(place2)) {

                    if (finalModel.getNet().getNet().getInEdges(place)
                            .equals(finalModel.getNet().getNet().getInEdges(place2))) {

                        if (finalModel.getNet().getNet().getOutEdges(place)
                                .equals(finalModel.getNet().getNet().getOutEdges(place2))) {

                            placesToRemove.add(place2);

                        }
                    }
                }
            }
        }

        for (Place place : placesToRemove) {
            finalModel.getNet().getNet().removePlace(place);
        }     */

        removeDanglingTransitions(finalModel.getNet().getNet());

        if (USE_PROVIDED_OBJECTS) {
            addPetriNetToProvidedObjects(context, finalModel.getNet().getNet(), "repaired net", false);
        }

        long end = System.currentTimeMillis();

        Petrinet finalPetriNet = finalModel.getNet().getNet();
        copyNetRemovingPluses(finalPetriNet);

        System.out.println("total number of fragments: " + decomposedModel.getSize());
        System.out.println("Total time: " + (end - start));
        System.out.println("Size of final model: " + finalModel.getNet().getNet().getNodes().size());

        IskraTimingResultBuilder timingResultBuilder = new IskraTimingResultBuilder();

        //addPetriNetToProvidedObjects(context, finalPetriNet, "result");

        FinalEvaluationResult evaluationResult =
                evaluator.evaluate(context, initialModel, finalModel, timingResultBuilder, CHAIN_NAME);

        IskraTimingResult timingResult = timingResultBuilder.build();

        resultBuilder.initialModel(initialModel.getNet()).finalModel(finalModel.getNet()).evaluationResult(evaluationResult).
                timingResult(timingResult).decomposerName(decomposer.getPluginName()).repairerName(repairer.toString()).
                decomposedNets(decomposedModel.getNetArray()).decomposedLogs(decomposedModel.getLogArray()).reminedNets(repairedDecomposedModel.getNetsAfterRepairing());

        return resultBuilder.build();
    }

    @Override
    public String toString() {
        return decomposer.toString() + " " + repairer.toString() + " " + evaluator.toString() + " " + fitnessThreshold;
    }
}
