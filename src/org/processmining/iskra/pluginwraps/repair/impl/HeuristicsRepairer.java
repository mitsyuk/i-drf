package org.processmining.iskra.pluginwraps.repair.impl;

import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.plugins.heuristicsnet.miner.heuristics.converter.HeuristicsNetToPetriNetConverter;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.HeuristicsMiner;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.gui.ParametersPanel;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.settings.HeuristicsMinerSettings;

import javax.swing.*;

/**
 * Wrapper for Heuristic miner
 *
 * @author Ivan Shugurov
 */
@KeepInProMCache
public class HeuristicsRepairer extends IskraRepairer {
    private ParametersPanel parametersPanel;

    public HeuristicsRepairer() {
        super("Heuristic miner");
    }

    @Override
    public Petrinet repair(PluginContext context, XLog log) {  //TODo будет падать без вызова графики(

        ParametersPanel parameters = new ParametersPanel();
        parameters.removeAndThreshold();
        ((UIPluginContext) context).showConfiguration("Heuristics Miner Parameters", parameters);
        HeuristicsMiner miner = new HeuristicsMiner(context, log, parameters.getSettings());
        HeuristicsNet net = miner.mine();
        return (Petrinet) HeuristicsNetToPetriNetConverter.converter(context, net)[0];
    }

    @Override
    public JComponent getSettingsComponent() {
        parametersPanel = new ParametersPanel();
        parametersPanel.removeAndThreshold();
        return parametersPanel;
    }

    @Override
    public void saveSettings() {
    }

    @Override
    public String getComment() {
        String comment;
        if (parametersPanel != null) {
            HeuristicsMinerSettings settings = parametersPanel.getSettings();
            comment = settings.toString();
            comment += "All tasks connected = " + settings.isUseAllConnectedHeuristics();
            comment += "\nLong distance dependency = " + settings.isUseLongDistanceDependency();
        } else {
            comment = "";
        }
        return comment;
    }
}
