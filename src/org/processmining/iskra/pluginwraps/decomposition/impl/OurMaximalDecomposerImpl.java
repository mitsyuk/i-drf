package org.processmining.iskra.pluginwraps.decomposition.impl;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.iskra.utils.LogDecomposer;
import org.processmining.iskra.utils.MaximalDecomposer;
import org.processmining.log.models.EventLogArray;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

/**
 * @author Dmitriy &lt;Zimy(x)&gt; Yakovlev
 */
@KeepInProMCache
public class OurMaximalDecomposerImpl extends IskraDecomposer {

    public OurMaximalDecomposerImpl() {
        super("Our maximal decomposer");
    }

    @Override
    public DecomposedModel decompose(PluginContext context, ComposedModel initialModel) {

        Petrinet initialPetriNet = initialModel.getNet().getNet();
        AcceptingPetriNet acceptingPetriNet = copyNetAddingPluses(initialPetriNet);

        MaximalDecomposer decomposer = new MaximalDecomposer();
        AcceptingPetriNetArray decomposedNet = decomposer.decompose(acceptingPetriNet);

        LogDecomposer logDecomposer = new LogDecomposer();
        EventLogArray decomposedLogs = logDecomposer.decompose(decomposedNet, initialModel.getLog());

        return new DecomposedModelImpl(decomposedNet, decomposedLogs);
    }
}
