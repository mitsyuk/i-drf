package org.processmining.iskra.pluginwraps.decomposition.impl;

import org.deckfour.xes.classification.XEventNameClassifier;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.events.Logger;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.iskra.utils.LogDecomposer;
import org.processmining.log.models.EventLogArray;
import org.processmining.log.models.impl.EventLogArrayFactory;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;
import org.processmining.models.passage.Passage;
import org.processmining.models.passage.PassageGraph;
import org.processmining.models.passage.PassageNode;
import org.processmining.models.passage.Passages;

import java.util.Collection;

@KeepInProMCache
public class PassageDecomposerImpl extends IskraDecomposer {
    public PassageDecomposerImpl() {
        super("Passages decomposer");
    }

    @Override
    public DecomposedModel decompose(PluginContext context, ComposedModel initialModel) {

        AcceptingPetriNet initialAcceptingNet = initialModel.getNet();
        Petrinet initialPetrinet = initialAcceptingNet.getNet();
        initialPetrinet = copyRemovingPluses(initialPetrinet);
        XLog eventLog = initialModel.getLog();

        Passages passages = new Passages(context, initialPetrinet, initialPetrinet.getTransitions(), eventLog, new XEventNameClassifier());
        PassageGraph passageGraph = passages.getGraph();

        AcceptingPetriNetArray resultArray = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        EventLogArray logArray = EventLogArrayFactory.createEventLogArray();
        for (PassageNode passageNode : passageGraph.getNodes()) {
            Passage passage = passageNode.getPassage();
            XLog log = passages.getLog(passage);
            Petrinet netPartition = passages.getNet(passage);
            AcceptingPetriNet acceptingNet = copyNetAddingPluses(netPartition);
            resultArray.addNet(acceptingNet);
            logArray.addLog(log);
        }

        handleBoundaryPlaces(initialPetrinet, eventLog, resultArray, logArray);

        context.log("Decomposition with parts: " + resultArray.getSize(), Logger.MessageLevel.DEBUG);
        return new DecomposedModelImpl(resultArray, logArray);
    }

    private void handleBoundaryPlaces(Petrinet initialPetrinet, XLog log, AcceptingPetriNetArray resultArray, EventLogArray logArray) {
        AcceptingPetriNetArray extraDecompositionParts = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        for (Place place : initialPetrinet.getPlaces()) {

            @SuppressWarnings("unchecked")
            Collection<PetrinetEdge<Transition, Place>> inArcs = (Collection) initialPetrinet.getInEdges(place);

            @SuppressWarnings("unchecked")
            Collection<PetrinetEdge<Place, Transition>> outArcs = (Collection) initialPetrinet.getOutEdges(place);

            if (inArcs.isEmpty()) {
                Petrinet extraDecompositionPart = PetrinetFactory.newPetrinet("");

                Place newPlace = extraDecompositionPart.addPlace(place.getLabel());

                for (PetrinetEdge<Place, Transition> outArc : outArcs) {
                    Transition consequentTransition = outArc.getTarget();
                    Transition newConsequentTransition = extraDecompositionPart.addTransition(consequentTransition.getLabel());

                    extraDecompositionPart.addArc(newPlace, newConsequentTransition);
                }

                AcceptingPetriNet extraAcceptingPetriNet = copyNetAddingPluses(extraDecompositionPart);

                extraDecompositionParts.addNet(extraAcceptingPetriNet);
            }

            if (outArcs.isEmpty()) {
                Petrinet extraDecompositionPart = PetrinetFactory.newPetrinet("");

                Place newPlace = extraDecompositionPart.addPlace(place.getLabel());

                for (PetrinetEdge<Transition, Place> inArc : inArcs) {
                    Transition precedingTransition = inArc.getSource();
                    Transition newPrecedingTransition = extraDecompositionPart.addTransition(precedingTransition.getLabel());

                    extraDecompositionPart.addArc(newPrecedingTransition, newPlace);
                }

                AcceptingPetriNet extraAcceptingPetriNet = copyNetAddingPluses(extraDecompositionPart);

                extraDecompositionParts.addNet(extraAcceptingPetriNet);
            }
        }

        if (extraDecompositionParts.getSize() != 0) {
            LogDecomposer logDecomposer = new LogDecomposer();
            EventLogArray extraLogArray = logDecomposer.decompose(extraDecompositionParts, log);

            for (int i = 0; i < extraLogArray.getSize(); i++) {
                resultArray.addNet(extraDecompositionParts.getNet(i));
                logArray.addLog(extraLogArray.getLog(i));
            }

        }
    }
}
