package org.processmining.iskra.pluginwraps.conformance;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.Pair;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.plugins.petrinet.replay.ReplayAction;
import org.processmining.plugins.petrinet.replay.conformance.ConformanceResult;
import org.processmining.plugins.petrinet.replay.conformance.ReplayConformancePlugin;
import org.processmining.plugins.petrinet.replay.conformance.TotalConformanceResult;
import org.processmining.plugins.petrinet.replayfitness.ReplayFitnessSetting;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ivan Shugurov on 16.11.2014.
 */
public class ConformanceByReplay {  //TODO отрефакторить(
    // dummy event class (for unmapped transitions)
    public final static XEventClass DUMMY = new XEventClass("DUMMY", -1) {
        public boolean equals(Object o) {
            return this == o;
        }
    };
    //private final UIPluginContext context;
    private final Petrinet petrinet;
    private final XLog eventLog;
    //private Map<Transition, XEventClass> map;
    private ReplayFitnessSetting setting = new ReplayFitnessSetting();
    //private LogPetrinetAssUI mapping;

    public ConformanceByReplay(Petrinet petrinet, XLog eventLog) {
        //this.context = context;
        this.petrinet = petrinet;
        this.eventLog = eventLog;
    }

    public void setSettings() {
        /*  TODO закоментил для автоматического сопоставлений transitions и events
        // list possible classifiers
        List<XEventClassifier> classList = new ArrayList<XEventClassifier>(eventLog.getClassifiers());
        // add default classifiers
        if (!classList.contains(XLogInfoImpl.RESOURCE_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.RESOURCE_CLASSIFIER);
        }
        //				if (!classList.contains(XLogInfoImpl.LIFECYCLE_TRANSITION_CLASSIFIER)){
        //					classList.add(0, XLogInfoImpl.LIFECYCLE_TRANSITION_CLASSIFIER);
        //				}
        if (!classList.contains(XLogInfoImpl.NAME_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.NAME_CLASSIFIER);
        }
        if (!classList.contains(XLogInfoImpl.STANDARD_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.STANDARD_CLASSIFIER);
        }

        Object[] availableEventClass = classList.toArray(new Object[classList.size()]);




        //Build and show the UI to make the mapping
        mapping = new LogPetrinetAssUI(eventLog, petrinet, availableEventClass);
        //InteractionResult result = context.showWizard("Mapping Petrinet - Log", false, true,  lpcfui.initComponents());

        TaskListener.InteractionResult result = TaskListener.InteractionResult.NEXT;

        boolean sem = true;

        int currentStep = 0;


        ReplayAnalysisUI ui;
        JComponent config;

        currentStep++;
        while (sem) {

            switch (result) {
                case NEXT:
                    if (currentStep == 1) {
                        result = context.showWizard("Mapping Petrinet - Log", false, false, mapping);
                        currentStep++;
                        break;
                    }
                    if (currentStep == 2) {
                        //suggestActions(setting, log, net,mapping.getSelectedClassifier());
                        setting.setMaximalCost(550);
                        setting.setAction(ReplayAction.INSERT_ENABLED_INVISIBLE, true);
                        setting.setAction(ReplayAction.INSERT_DISABLED_MATCH, true);
                        setting.setAction(ReplayAction.REMOVE_HEAD, false);
                        setting.setAction(ReplayAction.INSERT_ENABLED_MISMATCH, false);
                        setting.setAction(ReplayAction.INSERT_DISABLED_MISMATCH, false);
                        setting.setAction(ReplayAction.INSERT_ENABLED_MATCH, true);
                        ui = new ReplayAnalysisUI(setting);
                        config = ui.initComponents();
                        result = context.showWizard("Configure Conformance Settings", false, true, config);

                        ui.setWeights();
                    }

                    break;
                case FINISHED:
                    //BEGIN TODO      переделываю mapping
                    /*Collection<Pair<Transition, XEventClass>> res = new HashSet<Pair<Transition, XEventClass>>();
                    for (Transition trans : mapTrans2ComboBox.keySet()) {
                        Object selectedValue = mapTrans2ComboBox.get(trans)
                                .getSelectedItem();
                        if (selectedValue instanceof XEventClass) {
                            // a real event class
                            res.add(new Pair<Transition, XEventClass>(trans,
                                    (XEventClass) selectedValue));
                        } else {
                            // this is "NONE"
                            if (selectedValue.equals("NONE")) {
                                trans.setInvisible(true);
                                res.add(new Pair<Transition, XEventClass>(trans, DUMMY));
                            }
                        }
                    }
                    map = getMap(res);
                    extractEventClasses(eventLog);//TODO delete?
                    //END TODO
                    //map = getMap(mapping.getMap());
                    sem = false;
                    break;
                default:
                    context.log("press Cancel");
                    context.getFutureResult(0).cancel(true);
                    return;
            }
        }    */
    }

    public double measureConformance(PluginContext context, Marking initialMarking) {
        ReplayConformancePlugin conformancePlugin = new ReplayConformancePlugin();
        //BEGIN TODO для мэппинга без графики
        //suggestActions(setting, log, net,mapping.getSelectedClassifier());
        setting.setMaximalCost(550);
        setting.setAction(ReplayAction.INSERT_ENABLED_INVISIBLE, true);
        setting.setAction(ReplayAction.INSERT_DISABLED_MATCH, true);
        setting.setAction(ReplayAction.REMOVE_HEAD, false);
        setting.setAction(ReplayAction.INSERT_ENABLED_MISMATCH, false);
        setting.setAction(ReplayAction.INSERT_DISABLED_MISMATCH, false);
        setting.setAction(ReplayAction.INSERT_ENABLED_MATCH, true);
        //END TODO

        //TotalConformanceResult totalResult = conformancePlugin.getConformanceDetails(context, eventLog, petrinet);
        Map<Transition, XEventClass> transitionToEventMapping = extractEventClasses(eventLog);
        TotalConformanceResult totalResult = conformancePlugin.getConformanceDetails(context, eventLog, petrinet, initialMarking, setting, transitionToEventMapping, XLogInfoImpl.STANDARD_CLASSIFIER /*TODO сключительно для мэппинга без графики*/);
        ConformanceResult result = totalResult.getTotal();
        return result.getConformance();
    }

    private Map<Transition, XEventClass> getMap(Collection<Pair<Transition, XEventClass>> map) {
        Map<Transition, XEventClass> maps = new HashMap<Transition, XEventClass>();
        for (Pair<Transition, XEventClass> coppia : map) {
            XEventClass sec = coppia.getSecond();
            if (!sec.toString().equals("DUMMY")) {
                maps.put(coppia.getFirst(), coppia.getSecond());
            }
        }

        return maps;
    }

    private Map<Transition, XEventClass> extractEventClasses(XLog log) {
        XLogInfo summary = XLogInfoFactory.createLogInfo(log);
        XEventClasses eventClasses = summary.getEventClasses();

        Map<Transition, XEventClass> map = new HashMap<Transition, XEventClass>();
        for (Transition transition : petrinet.getTransitions()) {
            String label = transition.getLabel();
            XEventClass neededEventClass = eventClasses.getByIdentity(label);
            if (neededEventClass == null) {
                neededEventClass = eventClasses.getByIdentity(label + "+");
                if (neededEventClass != null) {
                    map.put(transition, neededEventClass);
                }
            } else {
                map.put(transition, neededEventClass);
            }
        }
        return map;
    }
}
