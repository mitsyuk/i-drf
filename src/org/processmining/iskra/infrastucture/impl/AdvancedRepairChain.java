package org.processmining.iskra.infrastucture.impl;

import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.infrastucture.Chain;
import org.processmining.iskra.infrastucture.IskraTimingResult;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluation;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.*;
import org.processmining.iskra.types.impl.ComposedModelImpl;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.log.models.EventLogArray;
import org.processmining.log.models.impl.EventLogArrayFactory;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

import java.util.ArrayList;
import java.util.List;

import static org.processmining.iskra.utils.AcceptingPetriNetMerger.mergePetriNetArray;
import static org.processmining.iskra.utils.BoundryPlacesFinder.checkPresenceOfBoundaryPlaces;
import static org.processmining.iskra.utils.LogProjection.projectNetOnLog;
import static org.processmining.iskra.utils.RemovalOfBoundaryPlaces.removeBoundaryPlaces;

/**
 * Created by Ivan Shugurov on 14.01.2015.
 */
@KeepInProMCache
@Chain(AdvancedRepairChain.CHAIN_NAME)
public class AdvancedRepairChain extends IskraGreedyRepairChain {

    public static final String CHAIN_NAME = "Another chain (Greedy0Repair)";

    public AdvancedRepairChain(
            IskraDecomposer decomposer, IskraRepairer repairer,
            IskraFinalEvaluation evaluator,
            ConformanceCheckingUsingAlignments conformanceChecker,
            Metrics metrics, double fitnessThreshold) {
        super(decomposer, repairer, evaluator, conformanceChecker, metrics, fitnessThreshold);
    }


    @Override
    public IskraResult execute(PluginContext context, ComposedModel initialModel) throws Exception {

        //double initialConformance = measureConformance(initialModel);

        long start = System.currentTimeMillis();

        ComposedModel changeableModel = new ComposedModelImpl(initialModel.getNet(), initialModel.getLog());

        int numberOfFragments = 0;
        int numberOfWrappers = 0;
        int sizeOfWorstWrapper = 0;
        int numberOfIterations = 0;

       // if (initialConformance < fitnessThreshold) {

           // double conformanceOnPreviousStep;
            double conformance;

            /*if (USE_PROVIDED_OBJECTS) {
                System.out.println("Initial conformance: " + initialConformance);
            }   */

            int stepNumber = 1;
            BadPartsWrapper wrapper = new BadPartsWrapper();

            do {
                numberOfIterations++;

                if (USE_PROVIDED_OBJECTS) {
                    System.out.println("before decomposition on step " + stepNumber);
                }

                DecomposedModel decomposedModel = decomposer.decompose(context, changeableModel);
                numberOfFragments = decomposedModel.getSize();
                org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray decomposedNets = decomposedModel.getNetArray();

                if (USE_PROVIDED_OBJECTS) {
                    System.out.println("after decomposition on step " + stepNumber);
                }

                if (USE_PROVIDED_OBJECTS) {
                    addDecompositionToProvidedObjects(
                            context, decomposedNets, decomposedModel.getLogArray(),
                            "Net decomposition on step " + stepNumber,
                            "Log decomposition on step " + stepNumber);
                }

                DecomposedConformanceResult decomposedConformanceResult = measureDecomposedConformance(decomposedModel);

                if (USE_PROVIDED_OBJECTS) {
                    printConformanceOfDecompositionParts(stepNumber, decomposedConformanceResult);
                }

                List<AcceptingPetriNet> badParts = new ArrayList<AcceptingPetriNet>();

                for (int i = 0; i < decomposedNets.getSize(); i++) {
                    if (decomposedConformanceResult.isBadPart(i)) {
                        badParts.add(decomposedNets.getNet(i));
                    }
                }

                Petrinet changeablePetrinet = changeableModel.getNet().getNet();

                if (USE_PROVIDED_OBJECTS) {
                    addPetrinetWithHighlightedBadPartsToProvidedObjects(context, changeablePetrinet, badParts, stepNumber, true);
                }

                WrappingResult wrappingResult = wrapper.wrapBadParts(context, decomposedNets, badParts);
                AcceptingPetriNetArray wrappers = wrappingResult.getBadParts();

                numberOfWrappers = wrappers.getSize();

                if (USE_PROVIDED_OBJECTS) {
                    addAcceptingPetriNetArrayToProvidedObjects(context, wrappers, "Wrappers on step " + stepNumber);
                }

                XLog originalLog = initialModel.getLog();
                EventLogArray projectedLogs = EventLogArrayFactory.createEventLogArray();

                for (int i = 0; i < wrappers.getSize(); i++) {
                    AcceptingPetriNet acceptingNet = wrappers.getNet(i);
                    Petrinet petrinet = acceptingNet.getNet();
                    XLog projection = projectNetOnLog(petrinet, originalLog);
                    projectedLogs.addLog(projection);
                }

                if (USE_PROVIDED_OBJECTS) {
                    addEventLogArrayToProvidedObjects(context, projectedLogs, "Log decomposition for wrappers on step " + stepNumber);
                }

                DecomposedConformanceResult conformanceResultOfWrappers = measureDecomposedConformance(new DecomposedModelImpl(wrappers, projectedLogs));

                int indexOfWorstPart = -1;
                double worstConformance = 2;
                for (int i = 0; i < wrappers.getSize(); i++) {
                    if (worstConformance > conformanceResultOfWrappers.getConformance(i)) {
                        indexOfWorstPart = i;
                        worstConformance = conformanceResultOfWrappers.getConformance(i);
                    }
                }

                if (indexOfWorstPart < 0) {
                    break;
                }

                Petrinet worstNet = wrappers.getNet(indexOfWorstPart).getNet();
                sizeOfWorstWrapper = worstNet.getNodes().size();

                if (USE_PROVIDED_OBJECTS) {
                    System.out.println("Initial marking of wrapper: " + wrappers.getNet(indexOfWorstPart).getInitialMarking());
                    System.out.println("Final marking of wrapper: " + wrappers.getNet(indexOfWorstPart).getFinalMarkings());
                }

                Pair<Boolean, Boolean> presenceOfBoundaryPlaces = checkPresenceOfBoundaryPlaces(worstNet);
                boolean containsInitialPlaces = presenceOfBoundaryPlaces.getFirst();
                boolean containsFinalPlaces = presenceOfBoundaryPlaces.getSecond();

                Petrinet repairedNet = repairer.repair(context, projectedLogs.getLog(indexOfWorstPart));
                removeBoundaryPlaces(repairedNet, containsInitialPlaces, containsFinalPlaces);
                wrappers.removeNet(indexOfWorstPart);

                if (USE_PROVIDED_OBJECTS) {
                    addPetriNetToProvidedObjects(context, repairedNet, "Remined net on step " + stepNumber, false);
                }

                AcceptingPetriNet repairedAcceptingNet = AcceptingPetriNetFactory.createAcceptingPetriNet(repairedNet);

                AcceptingPetriNetArray netParts = wrappingResult.getOtherParts();
                netParts.addNet(repairedAcceptingNet);

                for (int i = 0; i < wrappers.getSize(); i++) {
                    netParts.addNet(wrappers.getNet(i));
                }

                AcceptingPetriNet mergedNet = mergePetriNetArray(context, netParts);

                if (USE_PROVIDED_OBJECTS) {
                    addPetriNetToProvidedObjects(context, mergedNet, "Net after step " + stepNumber);
                }

                changeableModel = new ComposedModelImpl(mergedNet, originalLog);

                conformance = measureConformanceOfNewNet(changeableModel);

                if (USE_PROVIDED_OBJECTS) {
                    System.out.println("Conformance after step " + stepNumber + " is " + conformance);
                }

                stepNumber++;
            } while (conformance < fitnessThreshold);
       // }

        long end = System.currentTimeMillis();

        Petrinet finalPetriNet = changeableModel.getNet().getNet();
        copyNetRemovingPluses(finalPetriNet);

        System.out.println("total number of fragments: " + numberOfFragments);
        System.out.println("total number of bad wrappers: " + numberOfWrappers);
        System.out.println("Total time: " + (end - start));
        System.out.println("Size of final model: " + changeableModel.getNet().getNet().getNodes().size());
        System.out.println("Size of worst wrapper: " + sizeOfWorstWrapper);
        System.out.println("Iterations: " + numberOfIterations);

        IskraTimingResultBuilder timingResultBuilder = new IskraTimingResultBuilder();
        IskraTimingResult timingResult = timingResultBuilder.build();

        IskraResultBuilder resultBuilder = new IskraResultBuilder();
        resultBuilder.decomposerName(decomposer.getPluginName()).repairerName(repairer.getPluginName());

        System.out.println("Before final evaluation");
        FinalEvaluationResult evaluationResult =
                evaluator.evaluate(context, initialModel, changeableModel, timingResultBuilder, CHAIN_NAME);
        System.out.println("After final evaluation");

        resultBuilder.initialModel(initialModel.getNet()).finalModel(changeableModel.getNet()).
                evaluationResult(evaluationResult).timingResult(timingResult).
                decomposedNets(AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray()).decomposedLogs(EventLogArrayFactory.createEventLogArray()).reminedNets(AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray());

        return resultBuilder.build();
    }
}
