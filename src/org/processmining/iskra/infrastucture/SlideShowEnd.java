package org.processmining.iskra.infrastucture;

/**
 * End slide show callback for SlideShower
 * Created by user on 09.07.14.
 */
public interface SlideShowEnd {
    /**
     * SlideShow ends!
     */
    void itHappens();
}
