package org.processmining.iskra.plugins.steps;

import nl.tue.astar.AStarException;
import org.deckfour.uitopia.api.event.TaskListener;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.acceptingpetrinet.plugins.MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.PluginManager;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.iskra.plugins.IskraBase;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.pluginwraps.repair.Repairer;
import org.processmining.iskra.types.ImpossibilityToMeasureConformanceException;
import org.processmining.iskra.utils.LogDecomposer;
import org.processmining.iskra.utils.PlusRemoval;
import org.processmining.log.models.EventLogArray;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import ru.hse.pais.shugurov.widgets.elements.InputTextElement;
import ru.hse.pais.shugurov.widgets.panels.EmptyPanel;
import ru.hse.pais.shugurov.widgets.panels.SingleChoicePanel;

import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.processmining.iskra.utils.DanglingTransitionsRemover.removeDanglingTransitions;

/**
 * Created by Ivan on 28.03.2016.
 */
public class NaiveMiningStep extends IskraBase {

    protected ConformanceCheckingUsingAlignments conformanceChecker;
    private int screenNumber;
    private Collection<IskraRepairer> allRepiarers;
    private IskraRepairer selectedRepairer;

    @Plugin(name = "Step by step ISKRA: repair bad parts (naive)", returnLabels = "AcceptingPetriNet", returnTypes = AcceptingPetriNetArray.class, parameterLabels = {"Accepting net", "Log"})
    @UITopiaVariant(affiliation = "HSE", email = "", author = "")
    public AcceptingPetriNetArray repairBadPats(UIPluginContext context, AcceptingPetriNetArray netArray, XLog log) throws ImpossibilityToMeasureConformanceException, AStarException {
         return repairBadPats(context, netArray, log, true);
    }

    public AcceptingPetriNetArray repairBadPats(UIPluginContext context, AcceptingPetriNetArray netArray, XLog log, boolean usingThreshold) throws ImpossibilityToMeasureConformanceException, AStarException {
        this.log = log;
        AcceptingPetriNet mergedNet = new MergeAcceptingPetriNetArrayIntoAcceptingPetriNetPlugin().runDefault(
                context, netArray);
        this.petrinet = mergedNet.getNet();
        this.context = context;

        EventLogArray decompsedLog = decomposeEventLogs(netArray, log);

        TaskListener.InteractionResult result = null;

        while (true) {

            switch (screenNumber) {
                case 0:
                    result = configureRepairer(context);
                    break;
                case 1:
                    if (usingThreshold) {
                        result = configureThreshold(context);
                    }
                    break;
                case 2:
                    result = configureClassMapping();
                    break;
                case 3:
                    result = configureReplayer(true);
                    break;
            }

            switch (result) {
                case CANCEL:
                    return null;
                case PREV:
                    screenNumber--;
                    break;
                case NEXT:
                    screenNumber++;
                    break;
                case FINISHED:
                    conformanceChecker = new ConformanceCheckingUsingAlignments(algorithmStep.getAlgorithm(), replayParameter);
                    return repair(netArray, decompsedLog, fitnessThreshold, selectedRepairer, context); //TODO fix threshold
            }
        }
    }

    private TaskListener.InteractionResult configureThreshold(UIPluginContext context) {
        EmptyPanel emptyPanel = new EmptyPanel();

        InputTextElement textElement = new InputTextElement("Fitness threshold", Double.toString(fitnessThreshold));

        emptyPanel.add(textElement);

        boolean correctResult = false;

        TaskListener.InteractionResult result;

        do {
            result = context.showWizard("Fitness threshold", false, false, emptyPanel);

            if (result != TaskListener.InteractionResult.CANCEL) {
                String thresholdAsString = textElement.getValue();

                try {
                    double threshold = Double.parseDouble(thresholdAsString);

                    if (threshold < 0 || threshold > 1) {
                        correctResult = false;
                    } else {
                        correctResult = true;
                        this.fitnessThreshold = threshold;
                    }
                } catch (NumberFormatException e) {
                    correctResult = false;
                }

            }

        } while (!correctResult);

        return result;
    }

    private TaskListener.InteractionResult configureRepairer(UIPluginContext context) {
        if (allRepiarers == null) {
            allRepiarers = load(Repairer.class, IskraRepairer.class, context);
        }

        SingleChoicePanel<IskraRepairer> repairerChoicePanel = new SingleChoicePanel<>(allRepiarers);

        TaskListener.InteractionResult result;

        do {
            result = context.showWizard("Perairer selection", true, false, repairerChoicePanel);
            selectedRepairer = repairerChoicePanel.getChosenOption();

            if (result == TaskListener.InteractionResult.CANCEL) {
                return TaskListener.InteractionResult.CANCEL;
            }

        } while (selectedRepairer == null);

        return result;
    }

    protected AcceptingPetriNetArray repair(AcceptingPetriNetArray netArray, EventLogArray decompsedLog,
                                            double fitnessThreshold, IskraRepairer repairer, PluginContext context)
            throws AStarException, ImpossibilityToMeasureConformanceException {

        AcceptingPetriNetArray repairedNets = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        for (int i = 0; i < netArray.getSize(); i++) {
            AcceptingPetriNet acceptingNet = netArray.getNet(i);
            XLog log = decompsedLog.getLog(i);

            double conformance = measureConformanceOfNewNet(acceptingNet, log);

            System.out.println("Net " + i + " conformance: " + conformance);

            if (conformance < fitnessThreshold) {
                System.out.println("repairing net " + i);
                Petrinet petrinet = repairer.repair(context, log);

                removeInitialFinalPlaces(petrinet);
                removeDanglingTransitions(petrinet);
                petrinet = PlusRemoval.removePluses(petrinet);

                AcceptingPetriNet repairedAcceptingNet = AcceptingPetriNetFactory.createAcceptingPetriNet(petrinet);

                repairedNets.addNet(repairedAcceptingNet);
            } else {
                System.out.println("took net " + i + " without repairing");
                repairedNets.addNet(acceptingNet);
            }
        }

        return repairedNets;
    }

    protected void removeInitialFinalPlaces(Petrinet petrinet) {

        List<Place> placesToBeRemoved = new ArrayList<>();

        for (Place place : petrinet.getPlaces()) {
            if (petrinet.getInEdges(place).isEmpty() || petrinet.getOutEdges(place).isEmpty()) {
                placesToBeRemoved.add(place);
            }
        }

        for (Place place : placesToBeRemoved) {
            petrinet.removeNode(place);
        }
    }

    protected double measureConformanceOfNewNet(AcceptingPetriNet petriNet, XLog log) throws AStarException, ImpossibilityToMeasureConformanceException {

        return conformanceChecker.measureConformanceOfNewNet(petriNet, log).getFitness();
    }

    private <T> List<T> load(Class<? extends Annotation> annotation, Class<T> objectClass, PluginContext context) {
        List<T> instantiatedPlugins = new ArrayList<>();

        PluginManager pluginManager = context.getPluginManager();

        Set<Class<?>> pluginClasses = pluginManager.getKnownClassesAnnotatedWith(annotation);

        for (Class<?> pluginClass : pluginClasses) {

            if (objectClass.isAssignableFrom(pluginClass)) {

                if (!Modifier.isAbstract(pluginClass.getModifiers())) {
                    try {
                        T object = (T) pluginClass.newInstance();

                        instantiatedPlugins.add(object);
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return instantiatedPlugins;
    }

    private EventLogArray decomposeEventLogs(AcceptingPetriNetArray netArray, XLog eventLog) {
        LogDecomposer logDecomposer = new LogDecomposer();
        return logDecomposer.decompose(netArray, eventLog);
    }

}
