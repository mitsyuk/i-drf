package org.processmining.iskra.types;

import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.util.Collection;

/**
 * Created by Ivan on 19.03.2015.
 */
public class PetrinetHelper {

    public static Petrinet addPluses(Petrinet petrinet)
    {
        Petrinet copy = PetrinetFactory.clonePetrinet(petrinet);

        for (Transition transition : petrinet.getTransitions()) {
            String label = transition.getLabel();
            if (!label.endsWith("+") && !transition.isInvisible()) {
                for (Transition transitionToBeRenamed : copy.getTransitions()) {
                    if (transitionToBeRenamed.getLabel().equals(label)) {

                        Collection<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> inEdges = copy.getInEdges(transitionToBeRenamed);
                        Collection<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> outEdges = copy.getOutEdges(transitionToBeRenamed);

                        copy.removeTransition(transitionToBeRenamed);
                        Transition createdTransition = copy.addTransition(label + "+");

                        for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> edge : inEdges) {
                            Place precedingPlace = (Place) edge.getSource();
                            copy.addArc(precedingPlace, createdTransition);
                        }

                        for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> edge : outEdges) {
                            Place succeedingPlace = (Place) edge.getTarget();
                            copy.addArc(createdTransition, succeedingPlace);
                        }
                        break;
                    }
                }
            }
        }

        return copy;
    }
}
