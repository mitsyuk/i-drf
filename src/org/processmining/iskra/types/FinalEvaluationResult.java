package org.processmining.iskra.types;

/**
 * Final evaluation report
 */
public interface FinalEvaluationResult {

    double getRepairedModelFitness();

    double getRepairedModelPrecision();

    double getRepairedModelGeneralization();

    int getRepairedModelComplexity();

    double getRepairedModelConformance();

    double getSimilarity();

    double getInitialModelFitness();

    double getInitialModelPrecision();

    double getInitialModelGeneralization();

    int getInitialModelComplexity();

    double getInitialModelConformance();

    double getTotalEvaluation();

    String getChainName();
}
