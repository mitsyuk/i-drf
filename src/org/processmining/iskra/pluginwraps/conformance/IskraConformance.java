package org.processmining.iskra.pluginwraps.conformance;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.ConformanceParameters;
import org.processmining.iskra.types.IskraConformanceResult;

/**
 * The basic idea of conformance checker
 *
 * @author Alexey Mitsyik
 */
//TODO �� ��������� ���������. ������ �� �� ������?
public interface IskraConformance {

    /**
     * Checks conformance
     *
     * @param context    Prom 6 plugin context
     * @param model      Petri net + XES log
     * @param parameters extra parameters to conformance checker
     * @return conformance report
     */
    IskraConformanceResult doConformanceCheck(PluginContext context, ComposedModel model,
                                              ConformanceParameters parameters);
}
