package org.processmining.iskra.pluginwraps;

/**
 * @author Alexey Mitsyuk
 */
public abstract class AbstractIskraPlugin {
    private final String pluginName;

    /**
     * Constructor which is used to build plugins with given name
     *
     * @param pluginName is the name of result plugin
     */
    protected AbstractIskraPlugin(String pluginName) {
        if (pluginName == null) {
            throw new NullPointerException("Plugin name cannot be null");
        }
        this.pluginName = pluginName;
    }

    /**
     * Property for obtaining plugin name
     *
     * @return stored plugin name
     */
    public String getPluginName() {
        return pluginName;
    }

    @Override
    public String toString() {
        return pluginName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof AbstractIskraPlugin)) return false;

        AbstractIskraPlugin that = (AbstractIskraPlugin) o;

        return pluginName.equals(that.pluginName);

    }

    @Override
    public int hashCode() {
        return pluginName.hashCode() * 31 - 13;
    }
}
