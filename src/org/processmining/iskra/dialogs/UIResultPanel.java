package org.processmining.iskra.dialogs;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.plugins.VisualizeAcceptingPetriNetArrayPlugin;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.iskra.types.FinalEvaluationResult;
import org.processmining.iskra.types.IskraResult;
import org.processmining.models.jgraph.ProMJGraphVisualizer;
import org.processmining.models.jgraph.visualization.ProMJGraphPanel;
import ru.hse.pais.shugurov.widgets.panels.EmptyPanel;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;

public class UIResultPanel extends EmptyPanel {

    private JPanel table;

    public UIResultPanel(UIPluginContext context, IskraResult result) {
        this(new IskraResult[]{result});

        ProMJGraphVisualizer visualizer = ProMJGraphVisualizer.instance();
        ProMJGraphPanel visualizedInitialModel = visualizer.visualizeGraph(context, result.getInitialModel().getNet());
        ProMJGraphPanel visualizedFinalModel = visualizer.visualizeGraph(context, result.getFinalModel().getNet());

        AcceptingPetriNetArray decomposedModel = result.getDecomposedNets();
        VisualizeAcceptingPetriNetArrayPlugin visualizationPlugin = new VisualizeAcceptingPetriNetArrayPlugin();
        JComponent visualizedDecomposedModel = visualizationPlugin.visualize(context, decomposedModel);

        AcceptingPetriNetArray reminedNets = result.getReminedNets();
        JComponent visualizedReminedNets = visualizationPlugin.visualize(context, reminedNets);

        JTabbedPane visualizedNets = new JTabbedPane();
        visualizedNets.addTab("Initial model", visualizedInitialModel);
        visualizedNets.addTab("Final model", visualizedFinalModel);
        visualizedNets.addTab("Decomposition", visualizedDecomposedModel);
        visualizedNets.addTab("Remined nets", visualizedReminedNets);
        add(visualizedNets);
    }

    public UIResultPanel(IskraResult[] results) {

        table = new JPanel(new GridBagLayout());
        table.setMaximumSize(new Dimension(2000, 40 + results.length * 20));
        createFitnessHeader();
        createConformanceHeader();
        createPrecisionHeader();
        createGeneralizationHeader();
        createComplexityHeader();
        addLabel("Similarity", 10, 0, 1, 2);
        addLabel("Total evaluation", 11, 0, 1, 2);
        addLabel("Decomposer", 12, 0, 1, 2);
        addLabel("Repairer", 13, 0, 1, 2);
        addLabel("Chain", 14, 0, 1, 2);

        showTable(results);

        add(table);

    }

    private void showTable(final IskraResult[] results) {
        DecimalFormat decimalFormat = new DecimalFormat("#.####");
        for (int row = 0; row < results.length; row++) {
            FinalEvaluationResult evaluationResult = results[row].getEvaluationResult();

            int gridX = 0;
            int actualTableRow = row + 2;
            double initialModelFitness = evaluationResult.getInitialModelFitness();
            addLabel(decimalFormat.format(initialModelFitness), gridX++, actualTableRow, 1, 1);
            double repairedModelFitness = evaluationResult.getRepairedModelFitness();
            addLabel(decimalFormat.format(repairedModelFitness), gridX++, actualTableRow, 1, 1);

            double initialModelConformance = evaluationResult.getInitialModelConformance();
            addLabel(decimalFormat.format(initialModelConformance), gridX++, actualTableRow, 1, 1);
            double repairedModelConformance = evaluationResult.getRepairedModelConformance();
            addLabel(decimalFormat.format(repairedModelConformance), gridX++, actualTableRow, 1, 1);

            double initialModelPrecision = evaluationResult.getInitialModelPrecision();
            addLabel(decimalFormat.format(initialModelPrecision), gridX++, actualTableRow, 1, 1);
            double repairedModelPrecision = evaluationResult.getRepairedModelPrecision();
            addLabel(decimalFormat.format(repairedModelPrecision), gridX++, actualTableRow, 1, 1);

            double initialModelGeneralization = evaluationResult.getInitialModelGeneralization();
            addLabel(decimalFormat.format(initialModelGeneralization), gridX++, actualTableRow, 1, 1);
            double repairedModelGeneralization = evaluationResult.getRepairedModelGeneralization();
            addLabel(decimalFormat.format(repairedModelGeneralization), gridX++, actualTableRow, 1, 1);

            int initialModelComplexity = evaluationResult.getInitialModelComplexity();
            addLabel(Integer.toString(initialModelComplexity), gridX++, actualTableRow, 1, 1);
            int repairedModelComplexity = evaluationResult.getRepairedModelComplexity();
            addLabel(Integer.toString(repairedModelComplexity), gridX++, actualTableRow, 1, 1);

            double similarity = evaluationResult.getSimilarity();
            addLabel(decimalFormat.format(similarity), gridX++, actualTableRow, 1, 1);

            double totalEvaluation = evaluationResult.getTotalEvaluation();
            addLabel(decimalFormat.format(totalEvaluation), gridX++, actualTableRow, 1, 1);

            String decomposerName = results[row].getDecomposerName();
            addLabel(decomposerName, gridX++, actualTableRow, 1, 1);
            String repairerName = results[row].getRepairerName();
            addLabel(repairerName, gridX++, actualTableRow, 1, 1);

            GridBagConstraints constraints = new GridBagConstraints();
            constraints.fill = GridBagConstraints.BOTH;
            constraints.weightx = 1;
            constraints.gridx = gridX;
            constraints.gridy = actualTableRow;
            constraints.gridwidth = 1;
            constraints.gridheight = 1;

            String chainName = evaluationResult.getChainName();
            addLabel(chainName, gridX, actualTableRow, 1, 1);

        }
    }

    private void createFitnessHeader() {
        addLabel("Fitness", 0, 0, 2, 1);
        addLabel("Initial model", 0, 1, 1, 1);
        addLabel("Repaired model", 1, 1, 1, 1);
    }

    private void createConformanceHeader() {
        addLabel("Conformance", 2, 0, 2, 1);
        addLabel("Initial model", 2, 1, 1, 1);
        addLabel("Repaired model", 3, 1, 1, 1);
    }

    private void createPrecisionHeader() {
        addLabel("Precision", 4, 0, 2, 1);
        addLabel("Initial model", 4, 1, 1, 1);
        addLabel("Repaired model", 5, 1, 1, 1);
    }

    private void createGeneralizationHeader() {
        addLabel("Generalization", 6, 0, 2, 1);
        addLabel("Initial model", 6, 1, 1, 1);
        addLabel("Repaired model", 7, 1, 1, 1);
    }

    private void createComplexityHeader() {
        addLabel("Complexity", 8, 0, 2, 1);
        addLabel("Initial model", 8, 1, 1, 1);
        addLabel("Repaired model", 9, 1, 1, 1);
    }

    private void addLabel(String title, int gridX, int gridY, int gridWidth, int gridHeight) {
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1;
        constraints.gridx = gridX;
        constraints.gridy = gridY;
        constraints.gridwidth = gridWidth;
        constraints.gridheight = gridHeight;

        JLabel label = new JLabel(title, SwingConstants.CENTER);
        label.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 1, 0, 1, Color.BLACK), BorderFactory.createEmptyBorder(0, 5, 0, 5)));
        table.add(label, constraints);
    }
}