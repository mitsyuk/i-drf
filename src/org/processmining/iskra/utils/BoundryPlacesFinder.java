package org.processmining.iskra.utils;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.framework.util.Pair;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;

/**
 * Created by Ivan on 12.07.2016.
 */
public class BoundryPlacesFinder {
    /**
     * returns a pair (contains initial places; contains final places)
     */
    public static Pair<Boolean, Boolean> checkPresenceOfBoundaryPlaces(Petrinet petrinet) {
        boolean containsInitialPlaces = false;
        boolean containsFinalPlaces = false;

        for (Place place : petrinet.getPlaces()) {
            if (petrinet.getInEdges(place).isEmpty()) {
                containsInitialPlaces = true;
            } else if (petrinet.getOutEdges(place).isEmpty()) {
                containsFinalPlaces = true;
            }
        }

        return new Pair<>(containsInitialPlaces, containsFinalPlaces);
    }

    /**
     * returns a pair (contains initial places; contains final places)
     */
    public static Pair<Boolean, Boolean> checkPresenceOfBoundaryPlaces(AcceptingPetriNet petrinet) {
        return checkPresenceOfBoundaryPlaces(petrinet.getNet());
    }
}
