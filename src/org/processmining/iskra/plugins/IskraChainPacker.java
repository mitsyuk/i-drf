package org.processmining.iskra.plugins;

import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.iskra.dialogs.UIPackPanel;
import org.processmining.iskra.infrastucture.AbstractIskraRepairChainArray;
import org.processmining.iskra.infrastucture.impl.IndependentIskraRepairChain;
import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;

/**
 * Created by user on 07.07.14.
 */
@Plugin(name = "Iskra chain packer", parameterLabels = {"Iskra repair chain pack", "Extra chain"},
        returnLabels = {"Iskra repair chain pack"}, returnTypes = {AbstractIskraRepairChainArray.class})
public class IskraChainPacker {
    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alexey Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
            email = "amitsyuk" + (char) 0x40 + "hse.ru",
            pack = "DecomposedReplay")
    @PluginVariant(variantLabel = "Iskra chain packer, Default", requiredParameterLabels = {})
    public AbstractIskraRepairChainArray generateDefault(UIPluginContext context) {
        AbstractIskraRepairChainArray chainPack = new IndependentIskraRepairChain();
        return edit(context, chainPack);
    }

    /*
        Process:
        create base panel
        create editableListPanel with callback that will hide it, get
     */
    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alexey Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
            email = "amitsyuk" + (char) 0x40 + "hse.ru",
            pack = "DecomposedReplay")
    @PluginVariant(variantLabel = "Iskra chain packer, Editor", requiredParameterLabels = {0})
    public AbstractIskraRepairChainArray edit(UIPluginContext context, AbstractIskraRepairChainArray target) {
        context.showWizard("Package", true, true, new UIPackPanel(target.getPack()));
        return target;
    }

    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alexey Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
            email = "amitsyuk" + (char) 0x40 + "hse.ru",
            pack = "DecomposedReplay")
    @PluginVariant(variantLabel = "Iskra chain packer, Combiner", requiredParameterLabels = {0, 1})
    public AbstractIskraRepairChainArray combine(PluginContext context, AbstractIskraRepairChainArray target, IskraRepairChainImpl addition) {
        target.getPack().add(addition);
        return target;
    }
}
