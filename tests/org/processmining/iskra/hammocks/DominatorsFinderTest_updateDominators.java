package org.processmining.iskra.hammocks;

import org.junit.Test;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by Ivan on 29.07.2016.
 */
public class DominatorsFinderTest_updateDominators {
    DominatorsFinder finder = new DominatorsFinder();

    @Test
    public void updateDominators_A() {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Map<Transition, Set<Transition>> preDominators = new HashMap<>();
        Map<Transition, Set<Transition>> postDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            preDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            postDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        preDominators.put(a, preDominatorsForA);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        postDominators.put(e, postDominatorsForE);

        finder.updatePreDominators(preDominators, a);
        finder.updatePostDominators(postDominators, a);

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> correctPreDominatorsForA = new HashSet<>();
        correctPreDominatorsForA.add(a);
        correctPreDominators.put(a, correctPreDominatorsForA);

        Set<Transition> correctPostDominatorsForE = new HashSet<>();
        correctPostDominatorsForE.add(e);
        correctPostDominators.put(e, correctPostDominatorsForE);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }

    @Test
    public void updateDominators_B() {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Map<Transition, Set<Transition>> preDominators = new HashMap<>();
        Map<Transition, Set<Transition>> postDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            preDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            postDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        preDominators.put(a, preDominatorsForA);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        postDominators.put(e, postDominatorsForE);

        finder.updatePreDominators(preDominators, b);
        finder.updatePostDominators(postDominators, b);

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> correctPreDominatorsForA = new HashSet<>();
        correctPreDominatorsForA.add(a);
        correctPreDominators.put(a, correctPreDominatorsForA);

        Set<Transition> correctPreDominatorsForB = new HashSet<>();
        correctPreDominatorsForB.add(a);
        correctPreDominatorsForB.add(b);
        correctPreDominators.put(b, correctPreDominatorsForB);

        Set<Transition> correctPostDominatorsForE = new HashSet<>();
        correctPostDominatorsForE.add(e);
        correctPostDominators.put(e, correctPostDominatorsForE);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }

    @Test
    public void updateDominators_C() {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Map<Transition, Set<Transition>> preDominators = new HashMap<>();
        Map<Transition, Set<Transition>> postDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            preDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            postDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        preDominators.put(a, preDominatorsForA);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        postDominators.put(e, postDominatorsForE);

        finder.updatePreDominators(preDominators, c);
        finder.updatePostDominators(postDominators, c);

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> correctPreDominatorsForA = new HashSet<>();
        correctPreDominatorsForA.add(a);
        correctPreDominators.put(a, correctPreDominatorsForA);

        Set<Transition> correctPreDominatorsForC = new HashSet<>();
        correctPreDominatorsForC.add(a);
        correctPreDominatorsForC.add(c);
        correctPreDominators.put(c, correctPreDominatorsForC);

        Set<Transition> correctPostDominatorsForE = new HashSet<>();
        correctPostDominatorsForE.add(e);
        correctPostDominators.put(e, correctPostDominatorsForE);

        Set<Transition> correctPostDominatorsForC = new HashSet<>();
        correctPostDominatorsForC.add(e);
        correctPostDominatorsForC.add(c);
        correctPostDominators.put(c, correctPostDominatorsForC);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }

    @Test
    public void updateDominators_D() {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Map<Transition, Set<Transition>> preDominators = new HashMap<>();
        Map<Transition, Set<Transition>> postDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            preDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            postDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        preDominators.put(a, preDominatorsForA);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        postDominators.put(e, postDominatorsForE);

        finder.updatePreDominators(preDominators, d);
        finder.updatePostDominators(postDominators, d);

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> correctPreDominatorsForA = new HashSet<>();
        correctPreDominatorsForA.add(a);
        correctPreDominators.put(a, correctPreDominatorsForA);

        Set<Transition> correctPostDominatorsForE = new HashSet<>();
        correctPostDominatorsForE.add(e);
        correctPostDominators.put(e, correctPostDominatorsForE);

        Set<Transition> correctPostDominatorsForD = new HashSet<>();
        correctPostDominatorsForD.add(e);
        correctPostDominatorsForD.add(d);
        correctPostDominators.put(d, correctPostDominatorsForD);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }

    @Test
    public void updateDominators_E() {
        Petrinet petrinet = PetrinetFactory.newPetrinet("");

        Place initialPlace = petrinet.addPlace("initial");
        Place finalPlace = petrinet.addPlace("final");

        Place p1 = petrinet.addPlace("p1");
        Place p2 = petrinet.addPlace("p2");
        Place p3 = petrinet.addPlace("p3");

        Transition a = petrinet.addTransition("A");
        Transition b = petrinet.addTransition("B");
        Transition c = petrinet.addTransition("C");
        Transition d = petrinet.addTransition("D");
        Transition e = petrinet.addTransition("E");

        petrinet.addArc(initialPlace, a);
        petrinet.addArc(a, p1);
        petrinet.addArc(p1, b);
        petrinet.addArc(p1, c);
        petrinet.addArc(b, p2);
        petrinet.addArc(p2, d);
        petrinet.addArc(d, p3);
        petrinet.addArc(c, p3);
        petrinet.addArc(p3, e);
        petrinet.addArc(e, finalPlace);

        Map<Transition, Set<Transition>> preDominators = new HashMap<>();
        Map<Transition, Set<Transition>> postDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            preDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            postDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> preDominatorsForA = new HashSet<>();
        preDominatorsForA.add(a);
        preDominators.put(a, preDominatorsForA);

        Set<Transition> postDominatorsForE = new HashSet<>();
        postDominatorsForE.add(e);
        postDominators.put(e, postDominatorsForE);

        finder.updatePreDominators(preDominators, e);
        finder.updatePostDominators(postDominators, e);

        Map<Transition, Set<Transition>> correctPreDominators = new HashMap<>();
        Map<Transition, Set<Transition>> correctPostDominators = new HashMap<>();

        for (Transition transition : petrinet.getTransitions()) {
            correctPreDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
            correctPostDominators.put(transition, new HashSet<>(petrinet.getTransitions()));
        }

        Set<Transition> correctPreDominatorsForA = new HashSet<>();
        correctPreDominatorsForA.add(a);
        correctPreDominators.put(a, correctPreDominatorsForA);

        Set<Transition> correctPostDominatorsForE = new HashSet<>();
        correctPostDominatorsForE.add(e);
        correctPostDominators.put(e, correctPostDominatorsForE);

        assertEquals(correctPreDominators, preDominators);
        assertEquals(correctPostDominators, postDominators);
    }
}