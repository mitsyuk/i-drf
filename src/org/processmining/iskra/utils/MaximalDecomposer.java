package org.processmining.iskra.utils;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;
import org.processmining.models.semantics.petrinet.Marking;

import java.util.*;

/**
 * Created by Ivan on 25.04.2015.
 */
public class MaximalDecomposer {
    public AcceptingPetriNetArray decompose(Petrinet petrinet) {
        Map<Place, Place> oldPlacesToNewPlaces = new HashMap<Place, Place>();

        AcceptingPetriNetArray netArray = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        for (Place place : petrinet.getPlaces()) {

            if (!oldPlacesToNewPlaces.containsKey(place)) {

                Petrinet decomposedPart = PetrinetFactory.newPetrinet("");

                Map<Place, Place> nextOldPlacesToNewPlaces = handlePlace(petrinet, place, decomposedPart);
                oldPlacesToNewPlaces.putAll(nextOldPlacesToNewPlaces);

                AcceptingPetriNet acceptingDecomposedPart = AcceptingPetriNetFactory.createAcceptingPetriNet(decomposedPart);

                netArray.addNet(acceptingDecomposedPart);
            }
        }

        decomposeDanglingTransitions(netArray, petrinet);

        return netArray;
    }

    public AcceptingPetriNetArray decompose(AcceptingPetriNet acceptingPetriNet) {
        Petrinet petrinet = acceptingPetriNet.getNet();

        AcceptingPetriNetArray netArray = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();
        Map<Place, Place> oldPlacesToNewPlaces = new HashMap<Place, Place>();

        Marking initialMarking = acceptingPetriNet.getInitialMarking();
        if (initialMarking == null) {
            initialMarking = new Marking();
        }

        Set<Marking> finalMarkings = acceptingPetriNet.getFinalMarkings();
        if (finalMarkings == null) {
            finalMarkings = new HashSet<>();
        }

        for (Place place : petrinet.getPlaces()) {

            if (!oldPlacesToNewPlaces.containsKey(place)) {

                Petrinet decomposedPart = PetrinetFactory.newPetrinet("");

                Map<Place, Place> nextOldPlacesToNewPlaces = handlePlace(petrinet, place, decomposedPart);

                Marking decomposedInitialMarking = new Marking();
                for (Place initialPlace : initialMarking) {
                    if (nextOldPlacesToNewPlaces.containsKey(initialPlace)) {
                        decomposedInitialMarking.add(nextOldPlacesToNewPlaces.get(initialPlace));
                    }
                }

                Set<Marking> decomposedFinalMarkings = new HashSet<>();
                for (Marking finalMarking : finalMarkings) {
                    Marking decomposedFinalMarking = new Marking();

                    for (Place finalPlace : finalMarking) {
                        if (nextOldPlacesToNewPlaces.containsKey(finalPlace)) {
                            Place decomposedFinalPlace = nextOldPlacesToNewPlaces.get(finalPlace);
                            decomposedFinalMarking.add(decomposedFinalPlace);
                        }
                    }

                    if (!decomposedFinalMarking.isEmpty()) {
                        decomposedFinalMarkings.add(decomposedFinalMarking);
                    }
                }

                oldPlacesToNewPlaces.putAll(nextOldPlacesToNewPlaces);

                AcceptingPetriNet acceptingDecomposedPart = AcceptingPetriNetFactory.createAcceptingPetriNet(decomposedPart);
                acceptingDecomposedPart.setInitialMarking(decomposedInitialMarking);
                acceptingDecomposedPart.setFinalMarkings(decomposedFinalMarkings);

                netArray.addNet(acceptingDecomposedPart);
            }
        }

        decomposeDanglingTransitions(netArray, petrinet);

        return netArray;
    }

    public void decomposeDanglingTransitions(AcceptingPetriNetArray array, Petrinet petrinet) {
        for (Transition transition : petrinet.getTransitions()) {
            if (petrinet.getInEdges(transition).isEmpty() && petrinet.getOutEdges(transition).isEmpty()) {
                Petrinet fragmentNet = PetrinetFactory.newPetrinet("");
                fragmentNet.addTransition(transition.getLabel());
                AcceptingPetriNet acceptingFragment = AcceptingPetriNetFactory.createAcceptingPetriNet(fragmentNet);
                array.addNet(acceptingFragment);
            }
        }
    }

    private Map<Place, Place> handlePlace(
            Petrinet petrinet,
            Place place, Petrinet decomposedPart) {

        Map<Place, Place> existingPlacesToDecomposedPlaces = new HashMap<Place, Place>();
        Map<Transition, Transition> existingTransitionsToDecomposedTransitions = new HashMap<Transition, Transition>();

        handlePlace(
                petrinet, place, decomposedPart,
                existingPlacesToDecomposedPlaces,
                existingTransitionsToDecomposedTransitions);

        return existingPlacesToDecomposedPlaces;
    }

    private void handlePlace(
            Petrinet petrinet,
            Place place, Petrinet decomposedPart,
            Map<Place, Place> existingPlacesToDecomposedPlaces,
            Map<Transition, Transition> existingTransitionsToDecomposedTransitions) {

        if (existingPlacesToDecomposedPlaces.containsKey(place)) {
            return;
        }

        Place correspondingPlace = decomposedPart.addPlace(place.getLabel());
        existingPlacesToDecomposedPlaces.put(place, correspondingPlace);

        handlePrecedingTransitions(
                petrinet, place, decomposedPart,
                existingPlacesToDecomposedPlaces,
                existingTransitionsToDecomposedTransitions);

        handleSubsequentTransitions(
                petrinet, place, decomposedPart,
                existingPlacesToDecomposedPlaces,
                existingTransitionsToDecomposedTransitions);
    }

    private void handleSubsequentTransitions(
            Petrinet petrinet,
            Place place, Petrinet decomposedPart,
            Map<Place, Place> existingPlacesToDecomposedPlaces,
            Map<Transition, Transition> existingTransitionsToDecomposedTransitions) {

        @SuppressWarnings("unchecked")
        Collection<PetrinetEdge<Place, Transition>> outgoingEdges = (Collection) petrinet.getOutEdges(place);

        Place correspondingPlace = existingPlacesToDecomposedPlaces.get(place);

        for (PetrinetEdge<Place, Transition> outgoingEdge : outgoingEdges) {
            Transition subsequentTransition = outgoingEdge.getTarget();

            Transition correspondingTransition = getCorrespondingTransition(decomposedPart, existingTransitionsToDecomposedTransitions, subsequentTransition);

            decomposedPart.addArc(correspondingPlace, correspondingTransition);

            if (correspondingTransition.isInvisible()) {
                @SuppressWarnings("unchecked")
                Collection<PetrinetEdge<Transition, Place>> outEdges = (Collection) petrinet.getOutEdges(subsequentTransition);

                for (PetrinetEdge<Transition, Place> edge : outEdges) {
                    Place subsequentPlace = edge.getTarget();
                    handlePlace(
                            petrinet, subsequentPlace,
                            decomposedPart, existingPlacesToDecomposedPlaces,
                            existingTransitionsToDecomposedTransitions);
                }
            }
        }
    }

    private Transition getCorrespondingTransition(
            Petrinet decomposedPart, Map<Transition,
            Transition> existingTransitionsToDecomposedTransitions,
            Transition oldTransition) {

        Transition correspondingTransition;

        if (existingTransitionsToDecomposedTransitions.containsKey(oldTransition)) {
            correspondingTransition = existingTransitionsToDecomposedTransitions.get(oldTransition);
        } else {
            correspondingTransition = decomposedPart.addTransition(oldTransition.getLabel());
            correspondingTransition.setInvisible(oldTransition.isInvisible());
            existingTransitionsToDecomposedTransitions.put(oldTransition, correspondingTransition);
        }
        return correspondingTransition;
    }

    private void handlePrecedingTransitions(
            Petrinet petrinet, Place place,
            Petrinet decomposedPart,
            Map<Place, Place> existingPlacesToDecomposedPlaces,
            Map<Transition, Transition> existingTransitionsToDecomposedTransitions) {

        @SuppressWarnings("unchecked")
        Collection<PetrinetEdge<Transition, Place>> incomingEdges = (Collection) petrinet.getInEdges(place);
        Place correspondingPlace = existingPlacesToDecomposedPlaces.get(place);

        for (PetrinetEdge<Transition, Place> incomingEdge : incomingEdges) {
            Transition precedingTransition = incomingEdge.getSource();

            Transition correspondingTransition = getCorrespondingTransition(decomposedPart, existingTransitionsToDecomposedTransitions, precedingTransition);

            decomposedPart.addArc(correspondingTransition, correspondingPlace);

            if (correspondingTransition.isInvisible()) {
                @SuppressWarnings("unchecked")
                Collection<PetrinetEdge<Place, Transition>> inEdges = (Collection) petrinet.getInEdges(precedingTransition);

                for (PetrinetEdge<Place, Transition> edge : inEdges) {
                    Place place1 = edge.getSource();
                    handlePlace(
                            petrinet, place1, decomposedPart,
                            existingPlacesToDecomposedPlaces,
                            existingTransitionsToDecomposedTransitions);
                }
            }
        }
    }
}
