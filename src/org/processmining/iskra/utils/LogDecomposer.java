package org.processmining.iskra.utils;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryNaiveImpl;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.log.models.EventLogArray;
import org.processmining.log.models.impl.EventLogArrayFactory;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ivan on 05.05.2015.
 */
public class LogDecomposer {

    public EventLogArray decompose(AcceptingPetriNetArray netArray, XLog log) {
        EventLogArray decomposedLogs = EventLogArrayFactory.createEventLogArray();
        Map<String, List<Integer>> labelsToIndexesOfNets = new HashMap<>();

        XFactory xFactory = new XFactoryNaiveImpl();

        for (int i = 0; i < netArray.getSize(); i++) {
            AcceptingPetriNet acceptingPetriNet = netArray.getNet(i);
            Petrinet petrinet = acceptingPetriNet.getNet();

            for (Transition transition : petrinet.getTransitions()) {
                if (transition.isInvisible()) {
                    continue;
                }

                String label = transition.getLabel();
                label = removePlus(label);

                List<Integer> indexesOfNetsWithThisTransition = labelsToIndexesOfNets.get(label);

                if (indexesOfNetsWithThisTransition == null) {
                    indexesOfNetsWithThisTransition = new ArrayList<>();
                    labelsToIndexesOfNets.put(label, indexesOfNetsWithThisTransition);
                }

                indexesOfNetsWithThisTransition.add(i);
            }

            XLog xLog = xFactory.createLog();
            decomposedLogs.addLog(xLog);
        }

        XConceptExtension conceptExtension = XConceptExtension.instance();

        for (XTrace trace : log) {

            String traceName = conceptExtension.extractName(trace);

            XTrace[] decomposedTraces = new XTrace[netArray.getSize()];
            for (int i = 0; i < decomposedTraces.length; i++)
            {
                decomposedTraces[i] = xFactory.createTrace();
                conceptExtension.assignName(decomposedTraces[i], traceName);
            }

            for (XEvent event : trace) {
                String name = conceptExtension.extractName(event);
                List<Integer> indexesOfNetsWithThisEvent = labelsToIndexesOfNets.get(name);

                if (indexesOfNetsWithThisEvent == null) {
                    continue;
                }

                for (int i : indexesOfNetsWithThisEvent)
                {
                    XEvent eventCopy = (XEvent) event.clone();
                    decomposedTraces[i].add(eventCopy);
                }
            }

            for (int i = 0; i < decomposedTraces.length; i++)
            {
                XTrace decomposedTrace = decomposedTraces[i];

                if (!decomposedTrace.isEmpty())
                {
                    decomposedLogs.getLog(i).add(decomposedTrace);
                }
            }
        }

        return decomposedLogs;
    }

    private String removePlus(String label) {
        int plusIndex = label.indexOf("+");
        if (plusIndex < 0) {
            return label;
        } else {
            return label.substring(0, plusIndex);
        }
    }

}
