package org.processmining.iskra.hammocks;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.framework.util.Pair;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.util.*;

/**
 * Created by Ivan on 29.07.2016.
 */
public class HammocksFinder {
    public Set<Petrinet> find(Petrinet petrinet) {
        return find(petrinet, false, true);
    }

    public Set<Petrinet> findAll(Petrinet petrinet) {
        return find(petrinet, false, false);
    }

    public Set<Petrinet> findWithQuasiHammocks(Petrinet petrinet) {
        return find(petrinet, true, true);
    }

    public Set<Petrinet> findAllWithQuasiHammocks(Petrinet petrinet) {
        return find(petrinet, true, false);
    }

    public Set<Petrinet> find(Petrinet petrinet, boolean addQuasiHammocks, boolean onlyMinimalHammocks) {
        Dominators dominators = new DominatorsFinder().find(petrinet);
        Map<Transition, Set<Transition>> postDominators = dominators.getPostDominators();
        Map<Transition, Set<Transition>> preDominators = dominators.getPreDominators();

        return find(petrinet, postDominators, preDominators, addQuasiHammocks, onlyMinimalHammocks);
    }

    public AcceptingPetriNetArray findAsAcceptingPetriNetArray(Petrinet petrinet) {
        return findAsAcceptingPetriNetArray(petrinet, false, true);
    }

    public AcceptingPetriNetArray findWithQuasiHammocksAsAcceptingPetriNetArray(Petrinet petrinet) {
        return findAsAcceptingPetriNetArray(petrinet, true, true);
    }

    public AcceptingPetriNetArray findAllWithQuasiHammocksAsAcceptingPetriNetArray(Petrinet petrinet) {
        return findAsAcceptingPetriNetArray(petrinet, true, false);
    }

    public AcceptingPetriNetArray findAllAsAcceptingPetriNetArray(Petrinet petrinet) {
        return findAsAcceptingPetriNetArray(petrinet, false, false);
    }

    public AcceptingPetriNetArray findAsAcceptingPetriNetArray(Petrinet petrinet, boolean addQuasiHammocks, boolean onlyMinimalHammocks) {
        Set<Petrinet> hammocks = find(petrinet, addQuasiHammocks, onlyMinimalHammocks);

        AcceptingPetriNetArray array = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        for (Petrinet hammock : hammocks) {
            array.addNet(AcceptingPetriNetFactory.createAcceptingPetriNet(hammock));
        }

        return array;
    }

    private Set<Petrinet> find(Petrinet petrinet, Map<Transition, Set<Transition>> postDominators, Map<Transition,
            Set<Transition>> preDominators, boolean addQuasiHammocks, boolean onlyMinimalHammocks) {
        Set<Petrinet> hammocks = new HashSet<>();

        Set<Pair<Transition, Transition>> potentiallyCorrectHammocks = new HashSet<>();

        boolean allHammocksAreAddedCorrectly = find(petrinet, preDominators, postDominators, hammocks,
                potentiallyCorrectHammocks, addQuasiHammocks, onlyMinimalHammocks);

        if (!allHammocksAreAddedCorrectly) {
            for (Pair<Transition, Transition> hammock : potentiallyCorrectHammocks) {
                boolean canBeDecomposed = checkDecomposition(preDominators, postDominators, hammock.getFirst(),
                        hammock.getSecond(), onlyMinimalHammocks);

                if (!canBeDecomposed) {
                    addHammock(petrinet, hammocks, hammock.getFirst(), hammock.getSecond(), addQuasiHammocks);
                }
            }
        }

        return hammocks;
    }

    private boolean find(Petrinet petrinet, Map<Transition, Set<Transition>> preDominators,
                         Map<Transition, Set<Transition>> postDominators,
                         Set<Petrinet> hammocks, Set<Pair<Transition,
            Transition>> potentiallyCorrectHammocks, boolean addQuasiHammocks, boolean onlyMinimalHammocks) {
        boolean allHammocksAreAddedCorrectly = true;

        for (Map.Entry<Transition, Set<Transition>> postDominatorEntry : postDominators.entrySet()) {
            Transition transition = postDominatorEntry.getKey();

            if (postDominatorEntry.getValue().size() > 1) {
                allHammocksAreAddedCorrectly = find(petrinet, preDominators, postDominators, hammocks, potentiallyCorrectHammocks,
                        allHammocksAreAddedCorrectly, transition, addQuasiHammocks, onlyMinimalHammocks);
            }
        }
        return allHammocksAreAddedCorrectly;
    }

    private boolean find(Petrinet petrinet, Map<Transition, Set<Transition>> preDominators, Map<Transition, Set<Transition>> postDominators,
                         Set<Petrinet> hammocks, Set<Pair<Transition, Transition>> potentiallyCorrectHammocks,
                         boolean allHammocksAreAddedCorrectly, Transition hammockStart, boolean addQuasiHammocks, boolean onlyMinimalHammocks) {

        for (Transition hammockEnd : postDominators.get(hammockStart)) {
            if (hammockStart != hammockEnd) {
                if (preDominators.get(hammockEnd).contains(hammockStart)) {
                    //it is a hammock
                    boolean foundDecomposition = checkDecomposition(preDominators, postDominators, hammockStart,
                            hammockEnd, onlyMinimalHammocks);

                    if (foundDecomposition) {
                        potentiallyCorrectHammocks.add(new Pair<>(hammockStart, hammockEnd));
                    } else {
                        boolean correctHammock = addHammock(petrinet, hammocks, hammockStart, hammockEnd, addQuasiHammocks);
                        allHammocksAreAddedCorrectly &= correctHammock;

                        if (!correctHammock) {
                            preDominators.get(hammockEnd).remove(hammockStart);
                        }
                    }
                }
            }
        }
        return allHammocksAreAddedCorrectly;
    }

    private boolean checkDecomposition(Map<Transition, Set<Transition>> preDominators, Map<Transition,
            Set<Transition>> postDominators, Transition hammockStart, Transition hammockEnd, boolean onlyMinimalHammocks) {

        if (!onlyMinimalHammocks) {
            return false;
        }

        for (Transition postDominator2 : postDominators.get(hammockStart)) {
            //searching for smaller hammocks
            if (hammockStart != postDominator2 && hammockEnd != postDominator2) {
                if (preDominators.get(hammockEnd).contains(postDominator2)) {
                    //found that the hammock (transition : postDominator) can
                    // be decomposed into hammocks ((transition: postDominator2), (postDominator2: postDominator))

                    // do not add as a hammock
                    return true;
                }
            }
        }
        return false;
    }

    private boolean addHammock(Petrinet petrinet, Set<Petrinet> hammocks, Transition hammockStart, Transition hammockEnd, boolean addQuasiHammocks) {
        Petrinet hammock = constructHammock(petrinet, hammockStart, hammockEnd, addQuasiHammocks);

        boolean correctHammock = checkCorrectnessOfHammock(hammock, addQuasiHammocks);

        if (correctHammock) {
            hammocks.add(hammock);
        }

        return correctHammock;
    }

    private boolean checkCorrectnessOfHammock(Petrinet hammock, boolean addQuasiHammocks) {
        if (hammock == null) {
            return false;
        }

        if (addQuasiHammocks) {
            return true;
        }

        boolean hasInitialTransition = false;
        boolean hasFinalTransition = false;

        for (Transition transition : hammock.getTransitions()) {
            if (hammock.getInEdges(transition).isEmpty()) {
                if (hasInitialTransition) {
                    //several initial transitions
                    return false;
                }

                hasInitialTransition = true;
            }

            if (hammock.getOutEdges(transition).isEmpty()) {
                if (hasFinalTransition) {
                    //several final transitions
                    return false;
                }

                hasFinalTransition = true;
            }
        }

        return hasInitialTransition && hasFinalTransition;
    }

    private Petrinet constructHammock(Petrinet originalPetrinet, Transition originalBeginning, Transition originalEnd, boolean addQuasiHammocks) {
        Petrinet hammock = PetrinetFactory.newPetrinet("(" + originalBeginning.getLabel() + " : " + originalEnd.getLabel() + ")");

        Transition beginning = hammock.addTransition(originalBeginning.getLabel());
        Transition end = hammock.addTransition(originalEnd.getLabel());

        Set<PetrinetNode> possibleInnerNodes = new HashSet<>();

        Map<PetrinetNode, PetrinetNode> originalNodesToNew = new HashMap<>();
        originalNodesToNew.put(originalBeginning, beginning);
        originalNodesToNew.put(originalEnd, end);

        for (PetrinetEdge outEdge : originalPetrinet.getOutEdges(originalBeginning)) {
            Place successivePlace = (Place) outEdge.getTarget();
            addPlaceToHammock(originalNodesToNew, originalPetrinet, hammock, beginning, successivePlace, possibleInnerNodes);
        }

        if (!addQuasiHammocks) {
            for (PetrinetNode node : possibleInnerNodes) {
                if (originalNodesToNew.get(node) == null) {
                    //it's a quasi hammock
                    return null;
                }
            }
        }

        return hammock;
    }

    private void addPlaceToHammock(Map<PetrinetNode, PetrinetNode> originalNodesToNew, Petrinet originalPetrinet,
                                   Petrinet hammock, Transition precedingTransition, Place place,
                                   Set<PetrinetNode> possibleInnerNodes) {
        if (originalNodesToNew.containsKey(place)) {
            hammock.addArc(precedingTransition, (Place) originalNodesToNew.get(place));
        } else {
            Place newPlace = hammock.addPlace(place.getLabel());
            hammock.addArc(precedingTransition, newPlace);
            originalNodesToNew.put(place, newPlace);

            detectPossibleInnerNodes(originalPetrinet, precedingTransition, place, possibleInnerNodes);

            for (PetrinetEdge outEdge : originalPetrinet.getOutEdges(place)) {
                Transition outTransition = (Transition) outEdge.getTarget();
                addTransitionToHammock(originalNodesToNew, originalPetrinet, hammock, newPlace, outTransition, possibleInnerNodes);
            }
        }
    }

    private void detectPossibleInnerNodes(Petrinet originalPetrinet, PetrinetNode precedingNode, PetrinetNode currentNode, Set<PetrinetNode> possibleInnerNodes) {
        if (originalPetrinet.getInEdges(currentNode).size() > 1) {
            //several incoming arcs

            for (PetrinetEdge inEdge : originalPetrinet.getInEdges(currentNode)) {
                PetrinetNode possibleInnerNode = (PetrinetNode) inEdge.getSource();

                if (possibleInnerNode != precedingNode) {
                    possibleInnerNodes.add(possibleInnerNode);
                }
            }
        }
    }

    private void addTransitionToHammock(Map<PetrinetNode, PetrinetNode> originalNodesToNew,
                                        Petrinet originalPetrinet, Petrinet hammock,
                                        Place precedingPlace, Transition transition,
                                        Set<PetrinetNode> possibleInnerNodes) {
        if (originalNodesToNew.containsKey(transition)) {
            hammock.addArc(precedingPlace, (Transition) originalNodesToNew.get(transition));
        } else {
            Transition newTransition = hammock.addTransition(transition.getLabel());
            hammock.addArc(precedingPlace, newTransition);
            originalNodesToNew.put(transition, newTransition);

            detectPossibleInnerNodes(originalPetrinet, precedingPlace, transition, possibleInnerNodes);

            for (PetrinetEdge outEdge : originalPetrinet.getOutEdges(transition)) {
                Place outPlace = (Place) outEdge.getTarget();
                addPlaceToHammock(originalNodesToNew, originalPetrinet, hammock, newTransition, outPlace, possibleInnerNodes);
            }
        }
    }

}
