package org.processmining.iskra.dialogs;

import org.processmining.iskra.types.Metrics;
import ru.hse.pais.shugurov.widgets.elements.InputTextElement;
import ru.hse.pais.shugurov.widgets.panels.EmptyPanel;

import javax.swing.*;

/**
 * Created by Иван on 10.07.2014.
 */
public class MetricsPanel extends EmptyPanel
{
    private Metrics metrics;

    private InputTextElement initialFitnessElement;
    private InputTextElement finalFitnessElement;

    private InputTextElement initialGeneralizationElement;
    private InputTextElement finalGeneralizationElement;

    private InputTextElement initialSimplicityElement;
    private InputTextElement finalSimplicityElement;

    private InputTextElement initialPrecisionElement;
    private InputTextElement finalPrecisionElement;

    private InputTextElement similarityElement;


    public MetricsPanel(Metrics metrics)
    {
        this.metrics = metrics;

        double finalFitness = metrics.getFinalFitness();
        finalFitnessElement = new InputTextElement("Final fitness coefficient", Double.toString(finalFitness));

        double finalGeneralization = metrics.getFinalGeneralization();
        finalGeneralizationElement = new InputTextElement("Final generalization coefficient", Double.toString(finalGeneralization));

        double finalSimplicity = metrics.getFinalSimplicity();
        finalSimplicityElement = new InputTextElement("Final simplicity coefficient", Double.toString(finalSimplicity));

        double initialFitness = metrics.getInitialFitness();
        initialFitnessElement = new InputTextElement("Initial fitness coefficient", Double.toString(initialFitness));

        double initialPrecision = metrics.getInitialPrecision();
        initialPrecisionElement = new InputTextElement("Initial precision coefficient", Double.toString(initialPrecision));

        double initialGeneralization = metrics.getInitialGeneralization();
        initialGeneralizationElement = new InputTextElement("Initial generalization coefficient", Double.toString(initialGeneralization));

        double initialSimplicity = metrics.getInitialSimplicity();
        initialSimplicityElement = new InputTextElement("Initial simplicity coefficient", Double.toString(initialSimplicity));

        double similarity = metrics.getSimilarity();
        similarityElement = new InputTextElement("Similarity coefficient", Double.toString(similarity));

        double finalPrecision = metrics.getFinalPrecision();
        finalPrecisionElement = new InputTextElement("Final precision coefficient", Double.toString(finalPrecision));

        add(initialFitnessElement);
        add(finalFitnessElement);
        add(Box.createVerticalStrut(5));

        add(initialGeneralizationElement);
        add(finalGeneralizationElement);
        add(Box.createVerticalStrut(5));

        add(initialSimplicityElement);
        add(finalSimplicityElement);
        add(Box.createVerticalStrut(5));

        add(initialPrecisionElement);
        add(finalPrecisionElement);
        add(Box.createVerticalStrut(5));

        add(similarityElement);

    }

    public boolean verify()
    {
        boolean isEverythingCorrect;

        try
        {
            double initialFitness = Double.parseDouble(initialFitnessElement.getValue());
            isEverythingCorrect = metrics.setInitialFitness(initialFitness);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }

        try
        {
            double finalFitness = Double.parseDouble(finalFitnessElement.getValue());
            isEverythingCorrect &= metrics.setFinalFitness(finalFitness);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }

        try
        {
            double initialGeneralization = Double.parseDouble(initialGeneralizationElement.getValue());
            isEverythingCorrect &= metrics.setInitialGeneralization(initialGeneralization);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }

        try
        {
            double finalGeneralization = Double.parseDouble(finalGeneralizationElement.getValue());
            isEverythingCorrect &= metrics.setFinalGeneralization(finalGeneralization);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }


        try
        {
            double initialSimplicity = Double.parseDouble(initialSimplicityElement.getValue());
            isEverythingCorrect &= metrics.setInitialSimplicity(initialSimplicity);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }

        try
        {
            double finalSimplicity = Double.parseDouble(finalSimplicityElement.getValue());
            isEverythingCorrect &= metrics.setFinalSimplicity(finalSimplicity);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }


        try
        {
            double initialPrecision = Double.parseDouble(initialPrecisionElement.getValue());
            isEverythingCorrect &= metrics.setInitialPrecision(initialPrecision);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }
        try
        {
            double finalPrecision = Double.parseDouble(finalPrecisionElement.getValue());
            isEverythingCorrect &= metrics.setFinalPrecision(finalPrecision);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }

        try
        {
            double similarity = Double.parseDouble(similarityElement.getValue());
            isEverythingCorrect &= metrics.setSimilarity(similarity);
        } catch (NumberFormatException e)
        {
            isEverythingCorrect = false;
        }


        return isEverythingCorrect;
    }
}
