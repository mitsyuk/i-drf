package org.processmining.iskra.types.impl;

import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;

import java.util.HashSet;
import java.util.Set;

public class ComposedModelImpl implements ComposedModel {

    private AcceptingPetriNet net;

    private XLog log;

    private TransEvClassMapping mapping;

    public ComposedModelImpl(Petrinet net, XLog log) {
        this.net = AcceptingPetriNetFactory.createAcceptingPetriNet(net);
        this.log = log;
    }

    public ComposedModelImpl(AcceptingPetriNet net, XLog log) {
        this.net = net;
        this.log = log;

        Marking initialMarking = new Marking();
        Marking finalMarking = new Marking();

        Petrinet petrinet = net.getNet();

        for (Place place : petrinet.getPlaces()) {
            if (petrinet.getInEdges(place).isEmpty()) {
                initialMarking.add(place);
            } else if (petrinet.getOutEdges(place).isEmpty()) {
                finalMarking.add(place);
            }
        }

        net.setInitialMarking(initialMarking);
        Set<Marking> finalMarkings = new HashSet<>();
        finalMarkings.add(finalMarking);
        net.setFinalMarkings(finalMarkings);
    }

    public AcceptingPetriNet getNet() {
        return this.net;
    }

    public XLog getLog() {
        return this.log;
    }

    public TransEvClassMapping getMapping() {
        return this.mapping;
    }

    public void setMapping(TransEvClassMapping mapping) {
        this.mapping = mapping;
    }

}
