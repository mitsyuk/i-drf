package org.processmining.iskra.pluginwraps;


import nl.tue.astar.AStarException;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.events.Logger;
import org.processmining.iskra.pluginwraps.conformance.Conformance;
import org.processmining.iskra.pluginwraps.conformance.ConformanceByReplay;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.types.*;
import org.processmining.iskra.utils.ControlFlowComplexityMeasurer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;

/**
 * Final evaluator algorithm for Iskra
 *
 * @author Alexey Mitsyuk
 */
public class IskraFinalEvaluationImpl extends AbstractIskraPlugin implements IskraFinalEvaluation {

	/*
     * Formula variants:
	 * 1 final model quality only
	 * 2 final to initial model quality ratio 
	 * 3 final to initial model similarity
	 * 4 final to initial model quality ratio plus final to initial model similarity
	 * 
	 * Model Similarity Metric:
	 * 1 Graph Edit Distance
	 */

    private Metrics metrics;
    private ConformanceCheckingUsingAlignments conformanceChecker;

    public IskraFinalEvaluationImpl(Metrics metrics, ConformanceCheckingUsingAlignments conformanceChecker) {
        super("Final Evaluation");
        this.metrics = metrics;
        this.conformanceChecker = conformanceChecker;
    }

    @Override
    public FinalEvaluationResult evaluate(PluginContext context,
                                          ComposedModel initialModel,
                                          ComposedModel repairedModel,
                                          IskraTimingResultBuilder timingResultBuilder,
                                          String chainName) {

        FinalEvaluationResultBuilder resultBuilder = new FinalEvaluationResultBuilder(chainName);

        System.out.println("Before conformance checking of the initial model (Arya)");
        /*try {
            evaluateInitialModel(context, resultBuilder, initialModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("After conformance checking of the initial model (Arya)");

        double initialModelConformance = checkConformanceUsingReplay(context, initialModel, "the initial model");
        resultBuilder.initialModelConformance(initialModelConformance);

        double repairedModelConformance = checkConformanceUsingReplay(context, repairedModel, "the repaired model");
        resultBuilder.repairedModelConformance(repairedModelConformance);


        System.out.println("Before conformance checking of the final model (Arya)");

        try {
            evaluateRepairedModel(context, repairedModel, resultBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("After conformance checking of the final model (Arya)");


        System.out.println("Before simplicity measurement");
        measureComplexity(initialModel, repairedModel, resultBuilder);
        System.out.println("After simplicity measurement");


        AcceptingPetriNet initialAcceptingNet = initialModel.getNet();
        Petrinet initialPetriNet = initialAcceptingNet.getNet();
        AcceptingPetriNet repairedAcceptingNet = repairedModel.getNet();
        Petrinet repairedPetriNet = repairedAcceptingNet.getNet();
        GraphEditDistanceSimilarity sim = new GraphEditDistanceSimilarity();

        timingResultBuilder.finalEvaluationStart();
        System.out.println("Before similarity measurement");
        double similarity =
                sim.calculateSimilarity(initialPetriNet, repairedPetriNet);
        System.out.println("After similarity measurement");
        timingResultBuilder.finalEvaluationEnd();

        resultBuilder.repairedAndInitialModelSimilarity(similarity);*/

        return resultBuilder.build();
    }


    private double checkConformanceUsingReplay(PluginContext context, ComposedModel model, String netLabel) {
        System.out.println("Before conformance checking of " + netLabel + " using replay");
        Petrinet repairedNet = model.getNet().getNet();

        for (Transition transition : repairedNet.getTransitions()) {
            if (transition.isInvisible()) {
                System.out.println("Conformance checking using replay is skipped due to the presence of invisible transitions");
                context.log("Conformance checking using replay is skipped due to the presence of invisible transitions", Logger.MessageLevel.ERROR);
                return -1.0;
            }
        }

        double conformance = measureConformanceByReplay(context, model.getNet(), model.getLog());

        System.out.println("After conformance checking of " + netLabel + " using replay");
        return conformance;
    }

    private double measureConformanceByReplay(PluginContext context, AcceptingPetriNet acceptingPetriNet, XLog eventLog) {
        ConformanceByReplay conformanceByReplay = new ConformanceByReplay(acceptingPetriNet.getNet(), eventLog);
        conformanceByReplay.setSettings();
        return conformanceByReplay.measureConformance(context, acceptingPetriNet.getInitialMarking());
    }

    private void measureComplexity(ComposedModel initialModel, ComposedModel repairedModel, FinalEvaluationResultBuilder resultBuilder) {
        ControlFlowComplexityMeasurer complexityMeasurer = new ControlFlowComplexityMeasurer();

        int initialComplexity =
                complexityMeasurer.measure(initialModel.getNet().getNet());
        resultBuilder.initialModelComplexity(initialComplexity);

        int repairedModelComplexity =
                complexityMeasurer.measure(repairedModel.getNet().getNet());
        resultBuilder.repairedModelComplexity(repairedModelComplexity);
    }

    //TODO никак не меряю время
    private void evaluateRepairedModel(PluginContext context, ComposedModel model, FinalEvaluationResultBuilder resultBuilder) throws Exception {
        Conformance conformance = measureConformanceByReplay(context, model);

        resultBuilder.repairedModelFitness(conformance.getFitness());
        resultBuilder.repairedModelPrecision(conformance.getPrecision());
        resultBuilder.repairedModelGeneralization(conformance.getGeneralization());

        resultBuilder.totalEvaluation(metrics.evaluateFinalModel(conformance));
    }


    //TODO никак не меряю время
    private void evaluateInitialModel(PluginContext context, FinalEvaluationResultBuilder resultBuilder, ComposedModel model) throws Exception {
        Conformance conformance = measureConformanceByReplay(context, model);
        resultBuilder.initialModelFitness(conformance.getFitness());
        resultBuilder.initialModelPrecision(conformance.getPrecision());
        resultBuilder.initialModelGeneralization(conformance.getGeneralization());
    }

    private Conformance measureConformanceByReplay(PluginContext context, ComposedModel model) throws AStarException, ImpossibilityToMeasureConformanceException {
        if (conformanceChecker.canMeasurePrecisionAndGeneralization(model.getNet())) {
            return conformanceChecker.measureConformanceOfNewNet(model.getNet(), model.getLog());
        } else {
            System.out.println("Impossible to measure precision and generalization due to the existence of silent transitions without outgoing arcs");
            context.log("Impossible to measure precision and generalization due to the existence of silent transitions without outgoing arcs", Logger.MessageLevel.ERROR);

            double fitness = conformanceChecker.measureFitnessOfNewNet(model.getNet(), model.getLog());
            return new Conformance(fitness, -1, -1);
        }
    }

}