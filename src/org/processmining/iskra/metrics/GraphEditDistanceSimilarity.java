package org.processmining.iskra.metrics;

import org.deckfour.xes.classification.XEventClassifier;
import org.processmining.framework.util.LevenshteinDistance;
import org.processmining.models.graphbased.directed.DirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraphEdge;
import org.processmining.models.graphbased.directed.DirectedGraphNode;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// This is the algorithm implemented by Bart Hompes in the BartHompes package.
// There is no released .jar for that plugin, so it is used here as code.
// 2014, Alexey Mitsyuk

/**
 * Graph Edit Distance Similarity based on Dijkman et al.: Graph Matching
 * Algorithms for Business Process Similarity Search
 * <p/>
 * The measure is based on the graph-representation of business process models
 * where connector/gateway nodes are removed. Similarity is calculated based on
 * the amount of necessary transformation operations:
 * <ul>
 * <li>Node Substitution</li>
 * <li>Node Insertion / Deletion</li>
 * <li>Edge Insertion / Deletion</li>
 * </ul>
 * <p/>
 * There is a cost function for every transformation. The cost of substituting
 * node n1 with node n2 is calculated by 1 - sim(n1, n2). The default similarity
 * metric for node similarity is {@link LevenshteinSimilarity}.
 * <p/>
 * Similarity of models M1, M2 with activities A1, A2 and edges E1, E2 is then
 * calculated based on the set of substituted nodes subn, inserted/deleted nodes
 * skipn, and inserted/deleted edges skipe as follows: fskipn = |skipn|/(|A1| +
 * |A2|), fskipe = |skipe|/(|E1| + |E2|), fsubn = 2 * sum(1 - sim(a1,a2))
 * <p/>
 * sim(M1, M2) = (wskipn*fskipn + wskipe*fskipe + wsubn*fsubn) / (wskipn +
 * wskipe + wsubn)
 * <p/>
 * B. Hompes: This plug-in was copied from the ProM v5 plug-in, and modified to support Petrinets that have non-unique place labels.
 *
 * @author b.f.a.hompes
 */
public class GraphEditDistanceSimilarity {

    protected static final double DEFAULT_WSKIPN = 1;
    protected static final double DEFAULT_WSKIPE = 1;
    protected static final double DEFAULT_WSUBN = 1;
    protected XEventClassifier classifier;
    protected double wskipn;
    protected double wskipe;
    protected double wsubn;

    public GraphEditDistanceSimilarity() {
        this(DEFAULT_WSKIPN, DEFAULT_WSKIPE, DEFAULT_WSUBN);
    }

    public GraphEditDistanceSimilarity(double wskipn, double wskipe, double wsubn) {
        this.wskipn = wskipn;
        this.wskipe = wskipe;
        this.wsubn = wsubn;
    }

    /**
     * Calculates the actual GED Similarity between two models.
     *
     * @param modelA The first model.
     * @param modelB The second model.
     * @return The Graph Edit Distance Similarity between the two models, as a value between 0 and 1.
     * @author b.f.a.hompes
     */
    @SuppressWarnings("rawtypes")
    public double calculateSimilarity(Petrinet modelA, Petrinet modelB) {
        // PetriNetGraph is of type
        // DirectedGraph<PetrinetNode, PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>>
        DirectedGraph<?, ?> graphA = modelA.getGraph();
        DirectedGraph<?, ?> graphB = modelB.getGraph();

        Map<DirectedGraphNode, Set<DirectedGraphNode>> mappingsAB = new HashMap<DirectedGraphNode, Set<DirectedGraphNode>>();
        Map<DirectedGraphNode, Set<DirectedGraphNode>> mappingsBA = new HashMap<DirectedGraphNode, Set<DirectedGraphNode>>();
        Map<DirectedGraphNode, Map<DirectedGraphNode, Double>> similarities = new HashMap<DirectedGraphNode, Map<DirectedGraphNode, Double>>();

        for (DirectedGraphNode nodeA : graphA.getNodes()) {
            double maxSim = 0;
            Set<DirectedGraphNode> matches = new HashSet<DirectedGraphNode>();

            for (DirectedGraphNode nodeB : graphB.getNodes()) {
                double simAB = calculateSimilarity(nodeA, nodeB, modelA, modelB);

                if (simAB >= maxSim) {
                    if (simAB > maxSim && similarities.get(nodeA) != null) {
                        similarities.get(nodeA).clear();
                    }

                    if (similarities.get(nodeA) == null) {
                        similarities.put(nodeA, new HashMap<DirectedGraphNode, Double>());
                    }
                    similarities.get(nodeA).put(nodeB, simAB);

                    if (mappingsAB.get(nodeA) == null)
                        mappingsAB.put(nodeA, new HashSet<DirectedGraphNode>());
                    mappingsAB.get(nodeA).add(nodeB);

                    maxSim = simAB;
                    matches.add(nodeB);
                }
            }

            if (!matches.isEmpty()) {
                for (DirectedGraphNode match : matches) {
                    if (mappingsBA.get(match) == null)
                        mappingsBA.put(match, new HashSet<DirectedGraphNode>());
                    mappingsBA.get(match).add(nodeA);
                }
            }
        }

        Set<DirectedGraphNode> deletedNodes = new HashSet<DirectedGraphNode>();
        Set<DirectedGraphNode> addedNodes = new HashSet<DirectedGraphNode>();

        // Calculate the set of deleted nodes.
        for (DirectedGraphNode node : graphA.getNodes()) {
            if (!mappingsAB.containsKey(node)) {
                deletedNodes.add(node);
            }
        }

        // Calculate the set of added nodes.
        for (DirectedGraphNode node : graphB.getNodes()) {
            if (!mappingsBA.containsKey(node)) {
                addedNodes.add(node);
            }
        }

        // Calculate the set of deleted and added edges.
        Set<DirectedGraphEdge> deletedEdges = getEdgesOnlyInOneModel(graphA, graphB, mappingsAB);
        Set<DirectedGraphEdge> addedEdges = getEdgesOnlyInOneModel(graphB, graphA, mappingsBA);

        double simDist = 0.0;
        for (DirectedGraphNode nodeA : similarities.keySet()) {
            for (DirectedGraphNode nodeB : similarities.get(nodeA).keySet()) {
                simDist += 1.0 - similarities.get(nodeA).get(nodeB);
            }
        }

        double fskipn = 0;
        double fskipe = 0;
        double fsubn = 0;

        if (graphA.getNodes().size() > 0 && graphB.getNodes().size() > 0) {
            fskipn = (double) (deletedNodes.size() + addedNodes.size()) / (double) (graphA.getNodes().size() + graphB.getNodes().size());
        }
        if (graphA.getEdges().size() > 0 && graphB.getEdges().size() > 0) {
            fskipe = (double) (deletedEdges.size() + addedEdges.size()) / (double) (graphA.getEdges().size() + graphB.getEdges().size());
        }
        if (graphA.getNodes().size() > 0 && graphB.getNodes().size() > 0) {
            fsubn = 2.0 * simDist / (double)(graphA.getNodes().size() + graphB.getNodes().size() - deletedNodes.size() - addedNodes.size());
        }

//    	System.out.println("SkipN: "+fskipn+", SkipE: "+fskipe+", SubN: "+fsubn);
        return 1.0 - (wskipn * fskipn + wskipe * fskipe + wsubn * fsubn) / (wskipn + wskipe + wsubn);
    }

    /**
     * Calculates the similarity between two nodes.
     * <p/>
     * For Places:
     * Similarity based on common input and output transitions and {@link LevenshteinDistance}.
     * <p/>
     * For Transitions:
     * Similarity based on {@link LevenshteinDistance}.
     * <p/>
     * sim(s1, s2) = 1 - ed(s1,s2) / max(|s1|,|s2|)
     * Where ed(s1,s2) ... Levenshtein Distance (String Edit Distance) between strings 1 and 2
     *
     * @param nodeA  The node from the first model.
     * @param nodeB  The node from the second model.
     * @param modelA The first model.
     * @param modelB The second model.
     * @return The Levenshtein Distance between the labels of the nodes.
     */
    private double calculateSimilarity(DirectedGraphNode nodeA, DirectedGraphNode nodeB, Petrinet modelA, Petrinet modelB) {
        double similarity = 0;

        if (!nodeA.getClass().equals(nodeB.getClass()))
            return similarity;

        double edgeScore = 0;
        double labelScore = 0;

        if (nodeA instanceof Place && nodeB instanceof Place) {
            Set<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> inEdgesA = new HashSet<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>>(modelA.getInEdges(nodeA));
            Set<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> inEdgesB = new HashSet<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>>(modelB.getInEdges(nodeB));
            Set<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> outEdgesA = new HashSet<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>>(modelA.getOutEdges(nodeA));
            Set<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> outEdgesB = new HashSet<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>>(modelB.getOutEdges(nodeB));

//			System.out.println(inEdgesA.size()+" "+ outEdgesA.size());

            int inEdgesOverlap = 0;
            int outEdgesOverlap = 0;

            for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> inEdgeA : inEdgesA) {
                for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> inEdgeB : inEdgesB) {
                    if (inEdgeA.getSource().getLabel().equals(inEdgeB.getSource().getLabel()))
                        inEdgesOverlap++;
                }
            }

            for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> outEdgeA : outEdgesA) {
                for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> outEdgeB : outEdgesB) {
                    if (outEdgeA.getTarget().getLabel().equals(outEdgeB.getTarget().getLabel()))
                        outEdgesOverlap++;
                }
            }

            double inEdgeSimilarity = 0;
            double outEdgeSimilarity = 0;
            int divider = 0;
            if (inEdgesA.size() > 0 || inEdgesB.size() > 0) {
                inEdgeSimilarity = (inEdgesOverlap / Math.max(inEdgesA.size(), inEdgesB.size()));
                divider++;
            }
            if (outEdgesA.size() > 0 || outEdgesB.size() > 0) {
                outEdgeSimilarity = (outEdgesOverlap / Math.max(outEdgesA.size(), outEdgesB.size()));
                divider++;
            }
            edgeScore = divider > 0 ? (inEdgeSimilarity + outEdgeSimilarity) / divider : 0;
        }

        String labelA = nodeA.getLabel();
        String labelB = nodeB.getLabel();

        if (!(labelA.equals("") || labelB.equals(""))) {
            LevenshteinDistance LD = new LevenshteinDistance();
            double distance = LD.getLevenshteinDistanceLinearSpace(labelA, labelB);

            labelScore = 1.0 - distance / Math.max(labelA.length(), labelB.length());
        }

        similarity = nodeA instanceof Place
                ? (edgeScore + labelScore) / 2.0
                : labelScore;

//		System.out.println("Similarity: '"+nodeA+"'-'"+nodeB+"' = "+similarity);
        return similarity;
    }

    /**
     * Returns the set of edges that are only contained in graph A but not in graph B.
     *
     * @param graphA
     * @param graphB
     * @param mappingsAB
     * @return A set of edges only found in graph A and not in graph B.
     */
    @SuppressWarnings("rawtypes")
    private Set<DirectedGraphEdge> getEdgesOnlyInOneModel(DirectedGraph<?, ?> graphA, DirectedGraph<?, ?> graphB, Map<DirectedGraphNode, Set<DirectedGraphNode>> mappingsAB) {
        Set<DirectedGraphEdge> edgesOnlyInOneModel = new HashSet<DirectedGraphEdge>();

        // Search for edged within A but not in B.
        for (DirectedGraphEdge edgeA : graphA.getEdges()) {
            DirectedGraphNode source = edgeA.getSource();
            DirectedGraphNode target = edgeA.getTarget();
            Set<DirectedGraphNode> mappedSources = mappingsAB.get(source);
            Set<DirectedGraphNode> mappedTargets = mappingsAB.get(target);

            if (source == null || target == null) {
                edgesOnlyInOneModel.add(edgeA);
            } else {
                boolean found = false;
                if (mappedSources != null && mappedTargets != null) {
                    for (DirectedGraphNode mappedSource : mappedSources) {
                        for (DirectedGraphNode mappedTarget : mappedTargets) {
                            for (DirectedGraphEdge edgeB : graphB.getEdges()) {
                                if (edgeB.getSource().equals(mappedSource) && edgeB.getTarget().equals(mappedTarget)) {
                                    found = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (!found) {
                    edgesOnlyInOneModel.add(edgeA);
//    				System.out.println("Couldn't find match for: '"+source+"'-'"+target+"'");
                }
            }
        }
        return edgesOnlyInOneModel;
    }
}