package org.processmining.iskra.plugins.steps;

import nl.tue.astar.AStarException;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.iskra.infrastucture.impl.BadPartsWrapper;
import org.processmining.iskra.infrastucture.impl.WrappingResult;
import org.processmining.iskra.pluginwraps.conformance.DecomposedConformanceChecker;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.DecomposedConformanceResult;
import org.processmining.iskra.types.DecomposedModel;
import org.processmining.iskra.types.ImpossibilityToMeasureConformanceException;
import org.processmining.iskra.types.Metrics;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.iskra.utils.PlusRemoval;
import org.processmining.log.models.EventLogArray;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

import java.util.ArrayList;
import java.util.List;

import static org.processmining.iskra.utils.DanglingTransitionsRemover.removeDanglingTransitions;
import static org.processmining.iskra.utils.LogProjection.projectNetOnLog;

/**
 * Created by Ivan on 18.04.2016.
 */
public class GreedyMiningStep extends NaiveMiningStep {
    private DecomposedConformanceChecker decomposedConformanceChecker;

    @Plugin(name = "Step by step ISKRA: repair bad parts (greedy)", returnLabels = "AcceptingPetriNet", returnTypes = AcceptingPetriNetArray.class, parameterLabels = {"Accepting net", "Log"})
    @UITopiaVariant(affiliation = "HSE", email = "", author = "")
    @Override
    public AcceptingPetriNetArray repairBadPats(UIPluginContext context, AcceptingPetriNetArray netArray, XLog log) throws ImpossibilityToMeasureConformanceException, AStarException {
        return super.repairBadPats(context, netArray, log, false);

    }

    @Override
    protected AcceptingPetriNetArray repair(AcceptingPetriNetArray netArray, EventLogArray decompsedLog, double fitnessThreshold, IskraRepairer repairer, PluginContext context) throws AStarException, ImpossibilityToMeasureConformanceException {
        decomposedConformanceChecker = new DecomposedConformanceChecker(new Metrics(), conformanceChecker, fitnessThreshold);

        DecomposedModel decomposedModel = new DecomposedModelImpl(netArray, decompsedLog);

        DecomposedConformanceResult decomposedConformanceResult = decomposedConformanceChecker.measureDecomposedConformance(decomposedModel, true);

        AcceptingPetriNetArray decomposedNets = decomposedModel.getNetArray();

        int indexOfTheWorstPart = -1;
        double worstConformance = 2;
        for (int i = 0; i < decomposedNets.getSize(); i++) {
            if (decomposedConformanceResult.getConformance(i) < worstConformance) {
                indexOfTheWorstPart = i;
                worstConformance = decomposedConformanceResult.getConformance(i);
            }
        }

        BadPartsWrapper wrapper = new BadPartsWrapper();

        AcceptingPetriNet worstPart = decomposedNets.getNet(indexOfTheWorstPart);
        List<AcceptingPetriNet> badParts = new ArrayList<>();
        badParts.add(worstPart);

        WrappingResult wrappingResult = wrapper.wrapBadParts(context, decomposedNets, badParts);
        AcceptingPetriNet biggerCluster = wrappingResult.getBadParts().getNet(0);

        Petrinet mergedPetrinetFromBiggerCluster = biggerCluster.getNet();
        XLog log = projectNetOnLog(mergedPetrinetFromBiggerCluster, this.log);

        Petrinet repairedNet = repairer.repair(context, log);

        removeInitialFinalPlaces(repairedNet);
        removeDanglingTransitions(repairedNet);
        repairedNet = PlusRemoval.removePluses(repairedNet);

        AcceptingPetriNet repairedAcceptingNet = AcceptingPetriNetFactory.createAcceptingPetriNet(repairedNet);
        decomposedNets.addNet(repairedAcceptingNet);

        return decomposedNets;
    }
}
