package org.processmining.iskra.plugins;

import org.deckfour.uitopia.api.event.TaskListener;
import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.iskra.infrastucture.RepairChain;
import org.processmining.iskra.infrastucture.impl.AdvancedRepairChain;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluationImpl;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.decomposition.DecomposersFactory;
import org.processmining.iskra.pluginwraps.repair.RepairersFactory;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.iskra.types.IskraResult;
import org.processmining.iskra.types.impl.ComposedModelImpl;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.semantics.petrinet.Marking;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Ivan Shugurov on 14.01.2015.
 */
public class AnotherGreedyIskra extends IskraBase {

//    @Plugin(name = "Another iskra",
//            parameterLabels = {"Initial Model (Petri Net)", "Event log"},
//            returnLabels = {"Decomposed Replay Results", "Final Model"},
//            returnTypes = {IskraResult.class, Petrinet.class},
//            userAccessible = true,
//            help = "Iskra: Decomposed Repair Framework"
//    )
//
//    // UI - OneChain + One Experiment
//
//    @UITopiaVariant(affiliation = "HSE PAIS Lab",
//            author = "Alex Mitsyuk, Ivan Shugurov, Dmitry Yakovlev",
//            email = "amitsyuk@hse.ru",
//            pack = "DecomposedReplay")
    //@PluginVariant(requiredParameterLabels = {0, 1, 2})
    public Object[] iskraPlugin(UIPluginContext context,
                                Petrinet petrinet,
                                XLog log) throws Exception {
        this.context = context;
        this.petrinet = petrinet;
        this.log = log;
        decomposer = DecomposersFactory.getMaximalDecomposer();
        repairer = RepairersFactory.getInductiveRepairer();
        fitnessThreshold = 0.97;

        int screenNumber = 0;
        TaskListener.InteractionResult interactionResult = TaskListener.InteractionResult.CANCEL;

        ComposedModel composedModel = new ComposedModelImpl(petrinet, log);

        // Plugin setup interface
        //
        while (true) {
            switch (screenNumber) {
                case 0:
                    interactionResult = specifyMarking();
                    break;
                case 1:
                    interactionResult = configureDecomposerSettings(interactionResult, composedModel);
                    break;
                case 2:
                    interactionResult = configureRepairerSettings(interactionResult);
                    break;
                case 3:
                    interactionResult = configureClassMapping();
                    break;
                case 5:
                    interactionResult = configureReplayer(false);
                    break;
                case 6:
                    interactionResult = configureMetrics();
                    break;
            }

            switch (interactionResult) {
                case NEXT:
                    screenNumber++;
                    break;
                case PREV:
                    screenNumber--;
                    break;
                case CANCEL:
                    return null;
                case FINISHED:
                    ConformanceCheckingUsingAlignments conformanceChecker = new ConformanceCheckingUsingAlignments(algorithmStep.getAlgorithm(), replayParameter);

                    RepairChain chain =
                            new AdvancedRepairChain(decomposer, repairer,
                                    evaluator, conformanceChecker, metrics, fitnessThreshold);

                    evaluator = new IskraFinalEvaluationImpl(metrics, conformanceChecker);

                    composedModel.getNet().setInitialMarking(initialMarking);
                    Set<Marking> finalMarkings = new HashSet<Marking>();
                    finalMarkings.add(finalMarking);
                    composedModel.getNet().setFinalMarkings(finalMarkings);

                    composedModel.setMapping(mapping);

                    //todo Throw out mapping from chain.execute
                    IskraResult result =
                            chain.execute(context, composedModel);

                    /*ExportIskraResultAsCSVPlugin exportPlugin = new ExportIskraResultAsCSVPlugin();
                    try {
                        exportPlugin.exportToConstantFile(result);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } */

                    return new Object[]{result, result.getFinalModel().getNet()};
            }
        }
    }
}
