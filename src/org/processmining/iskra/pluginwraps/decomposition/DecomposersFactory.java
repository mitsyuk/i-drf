package org.processmining.iskra.pluginwraps.decomposition;

import org.processmining.iskra.pluginwraps.decomposition.impl.*;

/**
 * Factory for obtaining decomposer objects
 *
 * @author Ivan Shugurov
 */
public class DecomposersFactory {
    private DecomposersFactory() {
    }

    public static IskraDecomposer getPassageDecomposer() {
        return new PassageDecomposerImpl();
    }

    public static IskraDecomposer getSESEDecomposer() {
        return new SESEDecomposerImpl();
    }

    public static IskraDecomposer getMaximalDecomposer() {
        return new OurMaximalDecomposerImpl();
    }

    public static IskraDecomposer getRecomposer() {
        return new RecompositionDecomposer();
    }

    public static IskraDecomposer getNopDecomposer() {
        return new NopDecomposerImpl();
    }

    /**
     * Method for returning all configured decomposition
     *
     * @return array with decomposition
     */
    public static IskraDecomposer[] getAllDecomposers() {
        IskraDecomposer[] composers = new IskraDecomposer[5];
        composers[0] = getSESEDecomposer();
        composers[1] = getPassageDecomposer();
        composers[2] = getMaximalDecomposer();
        composers[3] = getRecomposer();
        composers[4] = getNopDecomposer();
        return composers;
    }

}
