package org.processmining.iskra.plugins;

import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.iskra.dialogs.UIResultPanel;
import org.processmining.iskra.types.IskraResult;

import javax.swing.*;


@Plugin(name = "Visualize Iskra Result",
        returnLabels = {"Visualized Iskra Result"},
        returnTypes = {JComponent.class},
        parameterLabels = {"Iskra Result"},
        userAccessible = false)
@Visualizer
public class VisualizeIskraResult {

    @PluginVariant(requiredParameterLabels = {0})
    public JComponent visualize(UIPluginContext context, IskraResult result) {
        return new UIResultPanel(context, result);
    }

    @PluginVariant(requiredParameterLabels = {0})
    public JComponent visualize(UIPluginContext context, IskraResult[] results) {
        return new UIResultPanel(results);
    }

}
