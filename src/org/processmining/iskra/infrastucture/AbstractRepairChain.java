package org.processmining.iskra.infrastucture;

import nl.tue.astar.AStarException;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.hub.ProMResourceManager;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.providedobjects.ProvidedObjectManager;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluation;
import org.processmining.iskra.pluginwraps.conformance.Conformance;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.conformance.DecomposedConformanceChecker;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraGeneralRepair;
import org.processmining.iskra.pluginwraps.repair.IskraGeneralRepairImpl;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.*;
import org.processmining.log.models.EventLogArray;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;
import org.processmining.models.semantics.petrinet.Marking;

import java.awt.*;
import java.util.*;
import java.util.List;

import static org.processmining.iskra.utils.DanglingTransitionsRemover.removeDanglingTransitions;

/**
 * Created by Ivan Shugurov on 08.01.2015.
 */
public abstract class AbstractRepairChain implements RepairChain {

    private static final Random random = new Random();
    protected IskraDecomposer decomposer;
    protected IskraGeneralRepair repairer;
    protected IskraFinalEvaluation evaluator;
    protected double fitnessThreshold;
    private ConformanceCheckingUsingAlignments conformanceChecker;
    private Metrics metrics;
    private DecomposedConformanceChecker decomposedConformanceChecker;

    protected AbstractRepairChain(IskraDecomposer decomposer, IskraRepairer repairer,
                                  IskraFinalEvaluation evaluator, ConformanceCheckingUsingAlignments conformanceChecker,
                                  Metrics metrics, double fitnessThreshold) {     //TODO а точно    fitnessThreshold надо так передавать? и использую ли я его вообще?
        this.decomposer = decomposer;
        this.conformanceChecker = conformanceChecker;
        this.repairer = new IskraGeneralRepairImpl(repairer);    //TODO что это? что за странный отказ от dependency injection?
        this.evaluator = evaluator;
        this.metrics = metrics;
        this.fitnessThreshold = fitnessThreshold;
        decomposedConformanceChecker = new DecomposedConformanceChecker(metrics, conformanceChecker, fitnessThreshold);
    }

    protected static AcceptingPetriNet copyNetAddingPluses(Petrinet petrinet) {

        Petrinet copy = PetrinetFactory.clonePetrinet(petrinet);

        for (Transition transition : petrinet.getTransitions()) {
            String label = transition.getLabel();
            if (!label.endsWith("+") && !transition.isInvisible()) {
                Iterator<Transition> iterator = copy.getTransitions().iterator();
                while (iterator.hasNext()) {
                    Transition transitionToBeRenamed = iterator.next();
                    if (transitionToBeRenamed.getLabel().equals(label)) {
                        Collection<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> inEdges = copy.getInEdges(transitionToBeRenamed);
                        Collection<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> outEdges = copy.getOutEdges(transitionToBeRenamed);
                        copy.removeTransition(transitionToBeRenamed);
                        Transition createdTransition = copy.addTransition(label + "+");
                        for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> edge : inEdges) {
                            Place precedingPlace = (Place) edge.getSource();
                            copy.addArc(precedingPlace, createdTransition);
                        }
                        for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> edge : outEdges) {
                            Place succeedingPlace = (Place) edge.getTarget();
                            copy.addArc(createdTransition, succeedingPlace);
                        }
                        break;
                    }
                }
            }
        }

        return AcceptingPetriNetFactory.createAcceptingPetriNet(copy);
    }

    protected static void copyNetRemovingPluses(Petrinet petrinet) {
        Petrinet copy = PetrinetFactory.clonePetrinet(petrinet);

        for (Transition transition : copy.getTransitions()) {
            String label = transition.getLabel();

            if (label.endsWith("+")) {
                String newLabel = label.replace("+", "");

                for (Transition realTransition : petrinet.getTransitions()) {
                    if (realTransition.getLabel().equals(label)) {
                        Collection<PetrinetEdge<?, ?>> inEdges = petrinet.getInEdges(realTransition);
                        Collection<PetrinetEdge<?, ?>> outEdges = petrinet.getOutEdges(realTransition);
                        petrinet.removeTransition(realTransition);
                        Transition replacementTransition = petrinet.addTransition(newLabel);

                        for (PetrinetEdge inEdge : inEdges) {
                            Place precedingPlace = (Place) inEdge.getSource();
                            petrinet.addArc(precedingPlace, replacementTransition);
                        }

                        for (PetrinetEdge outEdge : outEdges) {
                            Place succeedingPlace = (Place) outEdge.getTarget();
                            petrinet.addArc(replacementTransition, succeedingPlace);
                        }
                        break;
                    }
                }
            }
        }
    }

    protected void addPetrinetWithHighlightedBadPartsToProvidedObjects(PluginContext context, Petrinet originalNet, Collection<AcceptingPetriNet> badParts, int stepNumber, boolean copyNet) { //TODO тут опять отрезаю плюсы, везде
        Petrinet netCopy;

        if (copyNet) {
            netCopy = PetrinetFactory.clonePetrinet(originalNet);
        } else {
            netCopy = originalNet;
        }

        Map<String, Transition> labelToTransitionMap = new HashMap<>();
        Map<String, Place> labelToPlaceMap = new HashMap<>();

        for (Transition transition : netCopy.getTransitions()) {
            String label = transition.getLabel();
            label = removePlus(label);
            labelToTransitionMap.put(label, transition);
        }

        for (Place place : netCopy.getPlaces()) {
            String label = place.getLabel();
            labelToPlaceMap.put(label, place);
        }

        List<Integer> reds = new LinkedList<>();
        List<Integer> greens = new LinkedList<>();
        List<Integer> blues = new LinkedList<>();

        int basketSize = 255 / (badParts.size() + 1);
        for (int i = 1; i <= badParts.size(); i++) {
            int number = i * basketSize;
            reds.add(number);
            greens.add(number);
            blues.add(number);
        }

        Set<String> visitedTransitions = new HashSet<>();

        for (AcceptingPetriNet acceptingBadPart : badParts) {
            Petrinet badPart = acceptingBadPart.getNet();

            int redIndex = random.nextInt(reds.size());
            int red = reds.remove(redIndex);

            int greenIndex = random.nextInt(greens.size());
            int green = greens.remove(greenIndex);

            int blueIndex = random.nextInt(blues.size());
            int blue = blues.remove(blueIndex);

            Color placesColor = new Color(red, green, blue);

            for (Transition badPartTransition : badPart.getTransitions()) {
                String transitionLabel = badPartTransition.getLabel();
                if(visitedTransitions.add(transitionLabel)) {
                    transitionLabel = removePlus(transitionLabel);
                    Transition correspondingTransitionInCopy = labelToTransitionMap.get(transitionLabel);

                    if (correspondingTransitionInCopy == null) {
                        System.out.println("No transition for the label " + transitionLabel);
                        return;
                    }

                    AttributeMap attributeMap = correspondingTransitionInCopy.getAttributeMap();

                    if (attributeMap.get(AttributeMap.FILLCOLOR) != null/* && attributeMap.get(AttributeMap.FILLCOLOR).equals(Color.YELLOW)*/) {
                        attributeMap.put(AttributeMap.FILLCOLOR, Color.GREEN);
                    } else {
                        attributeMap.put(AttributeMap.FILLCOLOR, Color.RED);
                    }
                }
            }

            for (Place badPartPlace : badPart.getPlaces()) {
                String placeLabel = badPartPlace.getLabel();
                Place correspondingPlace = labelToPlaceMap.get(placeLabel);

                if (correspondingPlace == null) {
                    System.out.println("No place for the label " + placeLabel);
                    continue;
                    //return;
                }

                AttributeMap attributeMap = correspondingPlace.getAttributeMap();
                attributeMap.put(AttributeMap.FILLCOLOR, placesColor);
            }
        }

        ProvidedObjectManager objectManager = context.getProvidedObjectManager();
        objectManager.createProvidedObject("Petri net with highlighted bad parts on step " + stepNumber, netCopy, Petrinet.class, context);
        addToFavorite(context, netCopy);

    }

    private String removePlus(String str) {
        int lastPlusIndex = str.lastIndexOf("+");

        if (lastPlusIndex >= 0) {
            return str.substring(0, lastPlusIndex);
        } else {
            return str;
        }
    }

    protected void addDecompositionToProvidedObjects(PluginContext context, AcceptingPetriNetArray decomposedNets, EventLogArray eventLogArray, String netLabel, String logsLabel) {
        addAcceptingPetriNetArrayToProvidedObjects(context, decomposedNets, netLabel);
        addEventLogArrayToProvidedObjects(context, eventLogArray, logsLabel);
    }

    protected void addAcceptingPetriNetArrayToProvidedObjects(PluginContext context, AcceptingPetriNetArray acceptingPetriNetArray, String label) {
        AcceptingPetriNetArray decompositionCopy = copyAcceptingPetriNetArray(acceptingPetriNetArray);
        ProvidedObjectManager objectManager = context.getProvidedObjectManager();

        objectManager.createProvidedObject(label, decompositionCopy, AcceptingPetriNetArray.class, context);

        addToFavorite(context, decompositionCopy);
    }

    protected void addEventLogArrayToProvidedObjects(
            PluginContext context,
            EventLogArray projectedLogs, String objectName) {

        ProvidedObjectManager objectManager = context.getProvidedObjectManager();
        objectManager.createProvidedObject(objectName, projectedLogs, EventLogArray.class, context);
    }

    protected void addEventLogToProvidedObjects(
            PluginContext context,
            XLog projectedLogs, String objectName) {

        ProvidedObjectManager objectManager = context.getProvidedObjectManager();
        objectManager.createProvidedObject(objectName, projectedLogs, XLog.class, context);
    }

    protected AcceptingPetriNetArray copyAcceptingPetriNetArray(AcceptingPetriNetArray decomposedNets) {
        //TODO перенести в какой-нибудь хелпер?
        //TODO почему не копирую маркинг?
        AcceptingPetriNetArray decompositionCopy = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();
        decompositionCopy.init();
        for (int i = 0; i < decomposedNets.getSize(); i++) {
            AcceptingPetriNet acceptingPetriNet = decomposedNets.getNet(i);
            Petrinet copy = PetrinetFactory.clonePetrinet(acceptingPetriNet.getNet());
            AcceptingPetriNet acceptingCopy = AcceptingPetriNetFactory.createAcceptingPetriNet(copy);
            decompositionCopy.addNet(acceptingCopy);
        }
        return decompositionCopy;
    }

    protected void printConformanceOfDecompositionParts(int stepNumber, DecomposedConformanceResult decomposedConformanceResult) {
        System.out.println("Conformance results on step: " + stepNumber);

        for (int i = 0; i < decomposedConformanceResult.size(); i++) {
            System.out.println("Net " + (i + 1) + ": " + decomposedConformanceResult.getConformance(i));
        }
    }

    protected void addPetriNetToProvidedObjects(PluginContext context, Petrinet petrinet, String label, boolean addToFavorite) {
        Petrinet copy = PetrinetFactory.clonePetrinet(petrinet);
        ProvidedObjectManager objectManager = context.getProvidedObjectManager();
        objectManager.createProvidedObject(label, copy, Petrinet.class, context);

        if (addToFavorite) {
            addToFavorite(context, copy);
        }
    }

    protected void addPetriNetToProvidedObjects(PluginContext context, AcceptingPetriNet petriNet, String label) {
        ProvidedObjectManager objectManager = context.getProvidedObjectManager();
        objectManager.createProvidedObject(label, PetrinetFactory.clonePetrinet(petriNet.getNet()), Petrinet.class, context);
    }

    protected void handleBoundaryPlaces(AcceptingPetriNet acceptingNet) {
        Set<Place> placesToBeRemoved = new HashSet<>();
        Set<Transition> transitionsToBeRemoved = new HashSet<>();

        Marking initialMarking = new Marking();
        Marking finalMarking = new Marking();

        Set<Marking> finalMarkings = new HashSet<>();
        finalMarkings.add(finalMarking);

        Petrinet net = acceptingNet.getNet();

        for (Place place : net.getPlaces()) {
            @SuppressWarnings("unchecked")
            Collection<PetrinetEdge<Transition, Place>> inArcs = (Collection) net.getInEdges(place);
            @SuppressWarnings("unchecked")
            Collection<PetrinetEdge<Place, Transition>> outArcs = (Collection) net.getOutEdges(place);

            if (inArcs.isEmpty()) {
                boolean correctInitialPlace = true;

                for (PetrinetEdge<Place, Transition> outArc : outArcs) {
                    Transition consequentTransition = outArc.getTarget();

                    @SuppressWarnings("unchecked")
                    Collection<PetrinetEdge<Transition, Place>> inArcsOfConsequentTransitions = (Collection) net.getInEdges(consequentTransition);

                    if (inArcsOfConsequentTransitions.size() != 1) {
                        placesToBeRemoved.add(place);
                        correctInitialPlace = false;
                        break;
                    }
                }

                if (correctInitialPlace) {
                    boolean allSubsequentTransitionsAreInvisible = true;

                    for (PetrinetEdge<Place, Transition> outArc : outArcs) {
                        if (!outArc.getTarget().isInvisible()) {
                            allSubsequentTransitionsAreInvisible = false;
                            break;
                        }
                    }

                    if (allSubsequentTransitionsAreInvisible) {
                        correctInitialPlace = false;
                        placesToBeRemoved.add(place);
                    }
                }

                if (correctInitialPlace) {
                    initialMarking.add(place);
                }
            }


            if (outArcs.isEmpty()) {
                boolean correctFinalPlace = true;

                if (inArcs.size() == 1) {
                    Transition precedingTransition = inArcs.iterator().next().getSource();
                    if (precedingTransition.isInvisible()) {
                        transitionsToBeRemoved.add(precedingTransition);
                        placesToBeRemoved.add(place);
                        continue;
                    }
                }

                for (PetrinetEdge<Transition, Place> inArc : inArcs) {
                    Transition precedingTransition = inArc.getSource();

                    @SuppressWarnings("unchecked")
                    Collection<PetrinetEdge<Transition, Place>> outArcsOfPrecedingTransition = (Collection) net.getOutEdges(precedingTransition);


                    if (outArcsOfPrecedingTransition.size() != 1) {
                        placesToBeRemoved.add(place);
                        correctFinalPlace = false;
                        break;
                    }
                }

                if (correctFinalPlace) {
                    finalMarking.add(place);
                }
            }
        }

        for (Place place : placesToBeRemoved) {
            net.removePlace(place);
        }

        for (Transition transition : transitionsToBeRemoved) {
            net.removeTransition(transition);
        }

        removeDanglingTransitions(net);

        acceptingNet.setInitialMarking(initialMarking);
        acceptingNet.setFinalMarkings(finalMarkings);
    }

    //TODO пока нет формулы для рассчёта
    protected double measureConformance(AcceptingPetriNet petriNet, XLog log) throws AStarException, ImpossibilityToMeasureConformanceException {
        double evaluatedConformance;

        if (metrics.measureOnlyFitness()) {
            evaluatedConformance = conformanceChecker.measureFitness(petriNet.getNet(), log);
        } else {
            Conformance conformance = conformanceChecker.measureConformance(petriNet.getNet(), log);
            evaluatedConformance = metrics.evaluateInitialModel(conformance);
        }

        return evaluatedConformance;
    }

    protected double measureConformance(ComposedModel model) throws AStarException, ImpossibilityToMeasureConformanceException {
        return measureConformance(model.getNet(), model.getLog());
    }

    protected DecomposedConformanceResult measureDecomposedConformance(DecomposedModel decomposedModel) throws ImpossibilityToMeasureConformanceException, AStarException {
        return decomposedConformanceChecker.measureDecomposedConformance(decomposedModel, USE_PROVIDED_OBJECTS);
    }

    protected double measureConformanceOfNewNet(AcceptingPetriNet petriNet, XLog log) throws AStarException, ImpossibilityToMeasureConformanceException {
        double evaluatedConformance;

        if (metrics.measureOnlyFitness()) {
            evaluatedConformance = conformanceChecker.measureFitnessOfNewNet(petriNet, log);
        } else {
            Conformance conformance = conformanceChecker.measureConformanceOfNewNet(petriNet, log);
            evaluatedConformance = metrics.evaluateInitialModel(conformance);
        }

        return evaluatedConformance;
    }


    protected double measureConformanceOfNewNet(ComposedModel changeableModel) throws AStarException, ImpossibilityToMeasureConformanceException {
        return measureConformanceOfNewNet(changeableModel.getNet(), changeableModel.getLog());
    }

    protected void addToFavorite(PluginContext context, Object favoriteObject) {
        if (context instanceof UIPluginContext) {
            ProMResourceManager resourceManager = ((UIPluginContext) context).getGlobalContext().getResourceManager();
            resourceManager.getResourceForInstance(favoriteObject).setFavorite(true);
        }
    }
}
