package org.processmining.iskra.pluginwraps.redundant_places;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.parameters.BerthelotParameters;
import org.processmining.plugins.BerthelotPlugin;

/**
 * Created by Ivan Shugurov on 15.01.2015.
 */
//TODO просто скопипастил у Эрика
public class RemoveStructuralRedundantPlaces {

    public static AcceptingPetriNet reduceDefault(PluginContext context, AcceptingPetriNet net) {
        BerthelotParameters berthelotParameters = new BerthelotParameters();
        berthelotParameters.setInitialMarking(net.getInitialMarking());
        berthelotParameters.setFinalMarkings(net.getFinalMarkings());

        BerthelotPlugin berthelotPlugin = new BerthelotPlugin();
        Petrinet pn = berthelotPlugin.run(context, net.getNet(), berthelotParameters);

        AcceptingPetriNet reducedNet = AcceptingPetriNetFactory.createAcceptingPetriNet(pn);
        reducedNet.setInitialMarking(berthelotParameters.getInitialBerthelotMarking());
        reducedNet.setFinalMarkings(berthelotParameters.getFinalBerthelotMarkings());

        return reducedNet;
    }
}
