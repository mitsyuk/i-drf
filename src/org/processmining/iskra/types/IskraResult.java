package org.processmining.iskra.types;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.iskra.infrastucture.IskraTimingResult;
import org.processmining.log.models.EventLogArray;

/**
 * The basic idea of result storage
 */
public interface IskraResult {

    /**
     * Information about running time
     *
     * @return timing result from associated execution
     */
    IskraTimingResult getTimingResult();


    /**
     * Information about provided model
     *
     * @return user-provided model
     */
    AcceptingPetriNet getInitialModel();

    /**
     * Information about repaired model
     *
     * @return repaired model
     */
    AcceptingPetriNet getFinalModel();


    /**
     * Information about evaluation
     *
     * @return evaluation result
     */
    FinalEvaluationResult getEvaluationResult();

    /**
     * Name of used decomposer
     *
     * @return string: decomposer's name
     */
    String getDecomposerName();

    /**
     * Name of used repair
     *
     * @return string: repair's name
     */
    String getRepairerName();


    /**
     * Information about decomposition of petri net in experiment
     *
     * @return decomposed net
     */
    AcceptingPetriNetArray getDecomposedNets();

    /**
     * Information about decomposition of log in experiment
     *
     * @return decomposed log
     */
    EventLogArray getDecomposedLogs();

    AcceptingPetriNetArray getReminedNets(); //TODO исключительно для тестов
}
