package org.processmining.iskra.infrastucture.impl;

import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.providedobjects.ProvidedObjectManager;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.infrastucture.AbstractRepairChain;
import org.processmining.iskra.infrastucture.Chain;
import org.processmining.iskra.infrastucture.IskraTimingResult;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluation;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.*;
import org.processmining.iskra.types.impl.ComposedModelImpl;
import org.processmining.log.models.impl.EventLogArrayFactory;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;

import java.awt.*;
import java.util.*;
import java.util.List;

import static org.processmining.iskra.utils.AcceptingPetriNetMerger.mergePetriNetArray;
import static org.processmining.iskra.utils.BoundryPlacesFinder.checkPresenceOfBoundaryPlaces;
import static org.processmining.iskra.utils.LogProjection.projectNetOnLog;
import static org.processmining.iskra.utils.RemovalOfBoundaryPlaces.removeBoundaryPlaces;

/**
 * Created by Ivan Shugurov on 08.01.2015.
 */
@KeepInProMCache
@Chain(IskraGreedyRepairChain.CHAIN_NAME)
public class IskraGreedyRepairChain extends AbstractRepairChain {

    public static final String CHAIN_NAME = "Greedy chain (Greedy1Repair)";
    private final Random random = new Random();
    protected IskraRepairer repairer;

    public IskraGreedyRepairChain(
            IskraDecomposer decomposer, IskraRepairer repairer,
            IskraFinalEvaluation evaluator,
            ConformanceCheckingUsingAlignments conformanceChecker,
            Metrics metrics, double fitnessThreshold) {

        super(decomposer, repairer, evaluator, conformanceChecker, metrics, fitnessThreshold);
        this.repairer = repairer;
    }

    @Override
    public IskraResult execute(PluginContext context, ComposedModel initialModel) throws Exception {

        if (USE_PROVIDED_OBJECTS) {
            System.out.println("Greedy chain");
        }

        // double initialConformance = measureConformance(initialModel);

        // if (initialConformance < fitnessThreshold) {
        long start = System.currentTimeMillis();

        int numberOfFragments;
        int sizeOfWrapper = 0;
        int numberOfIterations = 0;

        //double conformanceOnPreviousStep;
        double conformance = 0;

            /*if (USE_PROVIDED_OBJECTS) {
                System.out.println("Initial conformance: " + initialConformance);
            } */

        ComposedModel changeableModel = new ComposedModelImpl(initialModel.getNet(), initialModel.getLog());

        int stepNumber = 1;
        int initialNumberOfTransitions = initialModel.getNet().getNet().getTransitions().size();

        do {
            numberOfIterations++;

            // conformanceOnPreviousStep = conformance;

            DecomposedModel decomposedModel = decomposer.decompose(context, changeableModel);

            numberOfFragments = decomposedModel.getSize();

            DecomposedConformanceResult decomposedConformanceResult = measureDecomposedConformance(decomposedModel);

            AcceptingPetriNetArray decomposedNets = decomposedModel.getNetArray();

            if (USE_PROVIDED_OBJECTS) {
                addDecompositionToProvidedObjects(
                        context, decomposedNets, decomposedModel.getLogArray(),
                        "Net decomposition on step " + stepNumber,
                        "Log decomposition on step " + stepNumber);


                printConformanceOfDecompositionParts(stepNumber, decomposedConformanceResult);
            }

            int indexOfTheWorstPart = getIndexOfTheWorstPart(decomposedConformanceResult, decomposedNets);

            if (indexOfTheWorstPart < 0)
            {
                System.out.println("There are no bad parts. Conformance of the entire net: " + conformance);
                break;
            }

            AcceptingPetriNet biggerCluster = wrapNetPart(context, changeableModel.getNet().getNet(), decomposedNets,
                    decomposedNets.getNet(indexOfTheWorstPart), stepNumber, null);

            sizeOfWrapper = biggerCluster.getNet().getNodes().size();

            if (USE_PROVIDED_OBJECTS) {
                addPetriNetToProvidedObjects(context, biggerCluster.getNet(), "Bigger cluster on step : " + stepNumber, false);
            }

            Petrinet mergedPetrinetFromBiggerCluster = biggerCluster.getNet();

            Pair<Boolean, Boolean> presenceOfBoundaryPlaces = checkPresenceOfBoundaryPlaces(mergedPetrinetFromBiggerCluster);
            boolean containsInitialPlaces = presenceOfBoundaryPlaces.getFirst();
            boolean containsFinalPlaces = presenceOfBoundaryPlaces.getSecond();

            XLog log = projectNetOnLog(mergedPetrinetFromBiggerCluster, initialModel.getLog());

            Petrinet repairedNet = repairer.repair(context, log);

            if (USE_PROVIDED_OBJECTS) {
                addPetriNetToProvidedObjects(context, repairedNet, "Cluster repaired on step " + stepNumber, false);
            }

            removeBoundaryPlaces(repairedNet, containsInitialPlaces, containsFinalPlaces);

            if (USE_PROVIDED_OBJECTS) {
                addPetriNetToProvidedObjects(context, repairedNet, "Cluster repaired on step " + stepNumber + " without initial and final places", false);
            }

            AcceptingPetriNet repairedAcceptingNet = copyNetAddingPluses(repairedNet);
            decomposedNets.addNet(repairedAcceptingNet);

            AcceptingPetriNet mergedRepairedNet = mergePetriNetArray(context, decomposedNets);

            /*int numberOfTransitions = mergedRepairedNet.getNet().getTransitions().size();

            if (initialNumberOfTransitions != numberOfTransitions)
            {
                System.out.println("NUMBER OF TRANSITIONS CHANGED ON ITERATION: " + numberOfIterations);

                addEventLogToProvidedObjects(context, log, "Event log which cannot be reconstructed");
                addPetriNetToProvidedObjects(context, biggerCluster.getNet(), "Model which cannot be repaired", false);

                Set<String>  initialTransitionLabels = new HashSet<>();

                for (Transition transition : initialModel.getNet().getNet().getTransitions())
                {
                     initialTransitionLabels.add(removePlus(transition.getLabel()));
                }

                Set<String>  resultingTransitionLabels = new HashSet<>();

                for (Transition transition : mergedRepairedNet.getNet().getTransitions())
                {
                    resultingTransitionLabels.add(removePlus(transition.getLabel()));
                }

                initialTransitionLabels.removeAll(resultingTransitionLabels);

                for (String label : initialTransitionLabels)
                {
                    System.out.println("Transition with label: " + label + " is lost");
                }

                break;
            }   */

            if (USE_PROVIDED_OBJECTS) {
                addPetriNetToProvidedObjects(context, mergedRepairedNet.getNet(), "Merged overall net on step " + stepNumber, false);
            }

            changeableModel = new ComposedModelImpl(mergedRepairedNet, initialModel.getLog());

            conformance = measureConformanceOfNewNet(changeableModel);

            if (USE_PROVIDED_OBJECTS) {
                System.out.println("Conformance after step " + stepNumber + ": " + conformance);
            }

            stepNumber++;
        } while (conformance < fitnessThreshold /*&& (conformanceOnPreviousStep != conformance)*/);

        long end = System.currentTimeMillis();

        System.out.println("total number of fragments: " + numberOfFragments);
        System.out.println("Total time: " + (end - start));
        System.out.println("Size of final model: " + changeableModel.getNet().getNet().getNodes().size());
        System.out.println("Size of wrapper: " + sizeOfWrapper);
        System.out.println("Iterations: " + numberOfIterations);

        IskraTimingResultBuilder timingResultBuilder = new IskraTimingResultBuilder();
        IskraTimingResult timingResult = timingResultBuilder.build();

        IskraResultBuilder resultBuilder = new IskraResultBuilder();
        resultBuilder.decomposerName(decomposer.getPluginName()).repairerName(repairer.getPluginName());

        copyNetRemovingPluses(changeableModel.getNet().getNet());

        FinalEvaluationResult evaluationResult =
                evaluator.evaluate(context, initialModel, changeableModel, timingResultBuilder, CHAIN_NAME);

        resultBuilder.initialModel(initialModel.getNet()).finalModel(changeableModel.getNet()).
                evaluationResult(evaluationResult).timingResult(timingResult).
                decomposedNets(AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray()).decomposedLogs(
                EventLogArrayFactory.createEventLogArray()).reminedNets(AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray());

        return resultBuilder.build();
    }

    protected int getIndexOfTheWorstPart(DecomposedConformanceResult decomposedConformanceResult, AcceptingPetriNetArray decomposedNets) {
        int indexOfTheWorstPart = -1;
        double worstConformance = 2;
        int numberOfBadFragments = 0;

        for (int i = 0; i < decomposedNets.getSize(); i++) {
            if (decomposedConformanceResult.getConformance(i) < worstConformance) {
                indexOfTheWorstPart = i;
                worstConformance = decomposedConformanceResult.getConformance(i);

                if (decomposedConformanceResult.isBadPart(i)) {
                    numberOfBadFragments++;
                }
            }
        }

        System.out.println("number of bad fragments: " + numberOfBadFragments);

        if (worstConformance == 1) {
            indexOfTheWorstPart = -1;
        }

        return indexOfTheWorstPart;
    }


    protected String removePlus(String label) {
        int plusIndex = label.indexOf("+");

        if (plusIndex < 0) {
            return label;
        } else {
            return label.substring(0, plusIndex);
        }
    }

    protected AcceptingPetriNet wrapNetPart(PluginContext context, Petrinet entireNet, AcceptingPetriNetArray allClusters,
                                            AcceptingPetriNet netToWrap, int stepNumber, Collection<AcceptingPetriNet> badParts) {
        AcceptingPetriNetArray biggerCluster = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        if (badParts != null) {
            badParts.remove(netToWrap);
        }

        biggerCluster.addNet(netToWrap);
        allClusters.removeNet(netToWrap);

        Collection<Transition> transitionsOfNetToWrap = netToWrap.getNet().getTransitions();

        for (int i = 0; i < allClusters.getSize(); i++) {
            AcceptingPetriNet acceptingNet = allClusters.getNet(i);

            Petrinet petrinet = acceptingNet.getNet();
            Collection<Transition> transitions = petrinet.getTransitions();

            if (checkIntersection(transitionsOfNetToWrap, transitions)) {
                biggerCluster.addNet(acceptingNet);
                allClusters.removeNet(acceptingNet);
                i--;

                if (badParts != null) {
                    badParts.remove(acceptingNet);
                }
            }
        }

        AcceptingPetriNet mergedBiggerCluster = mergePetriNetArray(context, biggerCluster);
        AcceptingPetriNetArray mergedClusterArray = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();
        mergedClusterArray.addNet(mergedBiggerCluster);

        if (USE_PROVIDED_OBJECTS) {
            addHighlightedClustersCopyToProvidedObjects(context, mergedClusterArray, allClusters, Collections.singletonList(netToWrap), stepNumber);

            addNetWithHighlightedClustersToProvidedObjects(context, entireNet, Collections.singletonList(netToWrap), mergedClusterArray, stepNumber);
        }

        return mergedBiggerCluster;
    }

    protected boolean checkIntersection(Collection<Transition> transitions1, Collection<Transition> transitions2) {
        for (Transition transition : transitions1) {
            if (!transition.isInvisible()) {
                for (Transition transition2 : transitions2) {
                    if (transition.getLabel().equals(transition2.getLabel())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

//    protected void removeFinalPlace(Petrinet petrinet) { //TODo предполагаю, что всего 1 конечный плейс
//        /*for (Place place : petrinet.getPlaces()) {
//            if (place.getLabel().equals("sink")) {
//                petrinet.removePlace(place);
//                break;
//            }
//        } */
//
//        for (Place place : petrinet.getPlaces()) {
//            Collection outEdges = petrinet.getOutEdges(place);
//
//            if (outEdges.isEmpty()) {
//                petrinet.removePlace(place);
//                break;
//            }
//        }
//    }
//
//    protected void removeInitialPlace(Petrinet petrinet) {     //TODo предполагаю, что всего 1 начальный плейс
//        /*for (Place place : petrinet.getPlaces()) {
//            if (place.getLabel().equals("source")) {
//                petrinet.removePlace(place);
//                break;
//            }
//        } */
//
//        for (Place place : petrinet.getPlaces()) {
//            Collection inEdges = petrinet.getInEdges(place);
//
//            if (inEdges.isEmpty()) {
//                petrinet.removePlace(place);
//                break;
//            }
//        }
//    }

    protected boolean equalWithoutPluses(String name1, String name2) {
        String modifiedName1 = removePlus(name1);
        String modifiedName2 = removePlus(name2);

        return modifiedName1.equals(modifiedName2);
    }

    protected void addNetWithHighlightedClustersToProvidedObjects(
            PluginContext context, Petrinet entireNet,
            Collection<AcceptingPetriNet> badParts, AcceptingPetriNetArray mergedClusters,
            int stepNumber) {

        Petrinet netCopy = PetrinetFactory.clonePetrinet(entireNet);

        Map<String, PetrinetNode> labelToNodes = new HashMap<String, PetrinetNode>();

        for (PetrinetNode node : netCopy.getNodes()) {
            String label = removePlus(node.getLabel());
            labelToNodes.put(label, node);
        }

        for (int i = 0; i < mergedClusters.getSize(); i++) {
            AcceptingPetriNet acceptingPetriNet = mergedClusters.getNet(i);
            Petrinet petrinet = acceptingPetriNet.getNet();

            for (PetrinetNode node : petrinet.getNodes()) {
                String label = removePlus(node.getLabel());

                PetrinetNode correspondingNode = labelToNodes.get(label);

                AttributeMap attributeMap = correspondingNode.getAttributeMap();
                attributeMap.put(AttributeMap.FILLCOLOR, Color.YELLOW);
            }
        }

        for (AcceptingPetriNet badPart : badParts) {
            Petrinet petrinet = badPart.getNet();

            for (PetrinetNode node : petrinet.getNodes()) {
                String label = removePlus(node.getLabel());

                PetrinetNode copiedNode = labelToNodes.get(label);

                AttributeMap attributeMap = copiedNode.getAttributeMap();
                attributeMap.put(AttributeMap.FILLCOLOR, Color.RED);
            }
        }

        ProvidedObjectManager objectManager = context.getProvidedObjectManager();
        objectManager.createProvidedObject("Net with highlighted clusters on step " + stepNumber, netCopy, Petrinet.class, context);
        addToFavorite(context, netCopy);
    }

    protected void addHighlightedClustersCopyToProvidedObjects(
            PluginContext context, AcceptingPetriNetArray mergedClusters,
            AcceptingPetriNetArray decomposedNets, Collection<AcceptingPetriNet> badParts, int stepNumber) {

        AcceptingPetriNetArray mergedClustersCopy = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        Set<String> labelsOfBadPartTransitions = new HashSet<String>();

        Map<String, Color> placeLabelsToColors = assignColorsToBadParts(decomposedNets);

        for (AcceptingPetriNet acceptingPetriNet : badParts) {
            Petrinet petrinet = acceptingPetriNet.getNet();
            for (Transition transition : petrinet.getTransitions()) {
                String label = transition.getLabel();
                label = removePlus(label);

                labelsOfBadPartTransitions.add(label);
            }

            for (Place place : petrinet.getPlaces()) {
                placeLabelsToColors.put(place.getLabel(), Color.RED);
            }
        }

        for (int i = 0; i < mergedClusters.getSize(); i++) {

            AcceptingPetriNet acceptingPetriNet = mergedClusters.getNet(i);
            Petrinet petrinet = acceptingPetriNet.getNet();

            Petrinet petrinetCopy = PetrinetFactory.clonePetrinet(petrinet);

            AcceptingPetriNet acceptingPetriNetCopy = AcceptingPetriNetFactory.createAcceptingPetriNet(petrinetCopy);
            mergedClustersCopy.addNet(acceptingPetriNetCopy);

            for (Transition transition : petrinetCopy.getTransitions()) {
                String label = transition.getLabel();
                label = removePlus(label);

                if (labelsOfBadPartTransitions.contains(label)) {
                    AttributeMap attributeMap = transition.getAttributeMap();
                    attributeMap.put(AttributeMap.FILLCOLOR, Color.RED);
                }
            }

            for (Place place : petrinetCopy.getPlaces()) {
                String placeLabel = place.getLabel();
                Color color = placeLabelsToColors.get(placeLabel);

                AttributeMap attributeMap = place.getAttributeMap();
                attributeMap.put(AttributeMap.FILLCOLOR, color);
            }

        }

        ProvidedObjectManager objectManager = context.getProvidedObjectManager();
        objectManager.createProvidedObject("Colored copies of wrappers on step " + stepNumber, mergedClustersCopy, AcceptingPetriNetArray.class, context);

        addToFavorite(context, mergedClustersCopy);
    }

    private Map<String, Color> assignColorsToBadParts(AcceptingPetriNetArray allParts) {
        Map<String, Color> placeLabelsToColors = new HashMap<String, Color>();

        List<Integer> reds = new LinkedList<>();
        List<Integer> greens = new LinkedList<>();
        List<Integer> blues = new LinkedList<>();

        int basketSize = 255 / (allParts.getSize() + 1);
        for (int i = 1; i <= allParts.getSize(); i++) {
            int number = i * basketSize;
            reds.add(number);
            greens.add(number);
            blues.add(number);
        }

        for (int i = 0; i < allParts.getSize(); i++) {
            Petrinet petrinet = allParts.getNet(i).getNet();

            int redIndex = random.nextInt(reds.size());
            int red = reds.remove(redIndex);

            int greenIndex = random.nextInt(greens.size());
            int green = greens.remove(greenIndex);

            int blueIndex = random.nextInt(blues.size());
            int blue = blues.remove(blueIndex);

            Color badPartColor = new Color(red, green, blue);

            for (Place place : petrinet.getPlaces()) {
                placeLabelsToColors.put(place.getLabel(), badPartColor);
            }

        }
        return placeLabelsToColors;
    }
}
