package org.processmining.iskra.infrastucture.impl;


import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetArrayFactory;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.infrastucture.Chain;
import org.processmining.iskra.infrastucture.IskraTimingResult;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluation;
import org.processmining.iskra.pluginwraps.conformance.ConformanceCheckingUsingAlignments;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.*;
import org.processmining.iskra.types.impl.ComposedModelImpl;
import org.processmining.log.models.impl.EventLogArrayFactory;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;

import java.awt.*;
import java.util.*;
import java.util.List;

import static org.processmining.iskra.utils.AcceptingPetriNetMerger.mergePetriNetArray;
import static org.processmining.iskra.utils.BoundryPlacesFinder.checkPresenceOfBoundaryPlaces;
import static org.processmining.iskra.utils.LogProjection.projectNetOnLog;
import static org.processmining.iskra.utils.RemovalOfBoundaryPlaces.removeBoundaryPlaces;

/**
 * Created by Ivan on 04.07.2016.
 */
@KeepInProMCache
@Chain(AdvancedGreedyIskra.CHAIN_NAME)
public class AdvancedGreedyIskra extends IskraGreedyRepairChain {
    public static final String CHAIN_NAME = "Advanced greedy ISKRA (AdvancedGreedy1Repair)";

    public AdvancedGreedyIskra(IskraDecomposer decomposer, IskraRepairer repairer, IskraFinalEvaluation evaluator,
                               ConformanceCheckingUsingAlignments conformanceChecker, Metrics metrics, double fitnessThreshold) {
        super(decomposer, repairer, evaluator, conformanceChecker, metrics, fitnessThreshold);
    }

    @Override
    public IskraResult execute(PluginContext context, ComposedModel initialModel) throws Exception {

        if (USE_PROVIDED_OBJECTS) {
            System.out.println("advanced greedy chain");
        }

        long start = System.currentTimeMillis();

        int numberOfFragments;
        int numberOfBadFragments;
        int stepNumber = 1;

        DecomposedModel decomposedModel = decomposer.decompose(context, initialModel);
        numberOfFragments = decomposedModel.getSize();

        DecomposedConformanceResult conformance = measureDecomposedConformance(decomposedModel);

        ComposedModel changeableModel = new ComposedModelImpl(initialModel.getNet(), initialModel.getLog());
        ComposedModel resultingModel;

        List<AcceptingPetriNet> allBadParts = new ArrayList<>();

        AcceptingPetriNetArray netArray = decomposedModel.getNetArray();

        for (int i = 0; i < conformance.size(); i++) {
            if (conformance.isBadPart(i)) {
                allBadParts.add(netArray.getNet(i));
            }
        }

        Set<AcceptingPetriNet> badParts = new HashSet<>(allBadParts);

        if (USE_PROVIDED_OBJECTS) {
            addPetrinetWithHighlightedBadPartsToProvidedObjects(context, initialModel.getNet().getNet(), allBadParts, stepNumber, true);
        }

        AcceptingPetriNetArray decomposedNets = decomposedModel.getNetArray();

        if (USE_PROVIDED_OBJECTS) {
            addDecompositionToProvidedObjects(
                    context, decomposedNets, decomposedModel.getLogArray(),
                    "Net decomposition on step " + stepNumber,
                    "Log decomposition on step " + stepNumber);
        }

        int indexOfTheWorstPart = getIndexOfTheWorstPart(conformance, decomposedNets);
        AcceptingPetriNet worstPart = decomposedNets.getNet(indexOfTheWorstPart);

        BadPartsWrapper wrapper = new BadPartsWrapper();
        WrappingResult wrappingResult = wrapper.wrapBadParts(context, decomposedNets, badParts);
        AcceptingPetriNetArray wrappers = wrappingResult.getBadParts();

        numberOfBadFragments = wrappers.getSize();

        badParts.clear();

        Set<AcceptingPetriNet> decomposition = new HashSet<>();

        for (int i = 0; i < wrappers.getSize(); i++) {
            badParts.add(wrappers.getNet(i));
            decomposition.add(wrappers.getNet(i));
        }

        AcceptingPetriNetArray goodParts = wrappingResult.getOtherParts();

        for (int i = 0; i < goodParts.getSize(); i++)
        {
            decomposition.add(goodParts.getNet(i));
        }

        AcceptingPetriNet biggerCluster = null;

        while (!badParts.isEmpty()) {
            if (USE_PROVIDED_OBJECTS) {
                System.out.println("number of bad parts: " + badParts.size());
            }

            if (biggerCluster == null) {
                biggerCluster = findBiggerClusterWithWorstPart(worstPart, wrappers);
                badParts.remove(biggerCluster);
                decomposition.remove(biggerCluster);
            } else {
                biggerCluster = wrap(context, changeableModel.getNet().getNet(), decomposition,
                        biggerCluster, stepNumber, badParts);
            }

            if (USE_PROVIDED_OBJECTS) {
                repair(context, initialModel, stepNumber, allBadParts, decomposedNets, biggerCluster);
            }

            stepNumber++;
        }

        if (!USE_PROVIDED_OBJECTS) {
            repair(context, initialModel, stepNumber, allBadParts, decomposedNets, biggerCluster);
        }
        decomposition.add(biggerCluster);

        decomposedNets = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        for (AcceptingPetriNet fragment : decomposition)
        {
            decomposedNets.addNet(fragment);
        }

        AcceptingPetriNet mergedRepairedNet = mergePetriNetArray(context, decomposedNets);

        resultingModel = new ComposedModelImpl(mergedRepairedNet, initialModel.getLog());

        long end = System.currentTimeMillis();

        Petrinet finalPetriNet = resultingModel.getNet().getNet();
        copyNetRemovingPluses(finalPetriNet);

        System.out.println("total number of fragments: " + numberOfFragments);
        System.out.println("Total time: " + (end - start));
        System.out.println("Size of final model: " + resultingModel.getNet().getNet().getNodes().size());
        //System.out.println("Size of wrapper: " + sizeOfWrapper);
        System.out.println("Iterations: " + (stepNumber - 1));
        System.out.println("Number of bad fragments: " + numberOfBadFragments);

        IskraTimingResultBuilder timingResultBuilder = new IskraTimingResultBuilder();
        IskraTimingResult timingResult = timingResultBuilder.build();

        IskraResultBuilder resultBuilder = new IskraResultBuilder();
        resultBuilder.decomposerName(decomposer.getPluginName()).repairerName(repairer.getPluginName());

        FinalEvaluationResult evaluationResult =
                evaluator.evaluate(context, initialModel, resultingModel, timingResultBuilder, CHAIN_NAME);

        resultBuilder.initialModel(initialModel.getNet()).finalModel(resultingModel.getNet()).
                evaluationResult(evaluationResult).timingResult(timingResult).
                decomposedNets(AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray()).decomposedLogs(EventLogArrayFactory.createEventLogArray()).reminedNets(AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray());

        return resultBuilder.build();
    }

    protected void repair(PluginContext context, ComposedModel initialModel, int stepNumber, List<AcceptingPetriNet> allBadParts, AcceptingPetriNetArray decomposedNets, AcceptingPetriNet biggerCluster) {
        Petrinet mergedPetrinetFromBiggerCluster = biggerCluster.getNet();

        Pair<Boolean, Boolean> presenceOfBoundaryPlaces = checkPresenceOfBoundaryPlaces(mergedPetrinetFromBiggerCluster);
        boolean containsInitialPlaces = presenceOfBoundaryPlaces.getFirst();
        boolean containsFinalPlaces = presenceOfBoundaryPlaces.getSecond();

        int sizeOfWrapper = mergedPetrinetFromBiggerCluster.getNodes().size();
        System.out.println("size of wrapper: " + sizeOfWrapper);
        XLog log = projectNetOnLog(mergedPetrinetFromBiggerCluster, initialModel.getLog());

        Petrinet repairedNet = repairer.repair(context, log);

        if (USE_PROVIDED_OBJECTS) {
            addPetriNetToProvidedObjects(context, repairedNet, "Cluster repaired on step " + stepNumber, true);
            addEventLogToProvidedObjects(context, log, "Projected log");
        }

        removeBoundaryPlaces(repairedNet, containsInitialPlaces, containsFinalPlaces);
        biggerCluster.init(repairedNet);

        if (USE_PROVIDED_OBJECTS) {
            decomposedNets.addNet(biggerCluster);
            AcceptingPetriNet mergedRepairedNet = mergePetriNetArray(context, decomposedNets);
            decomposedNets.removeNet(decomposedNets.getSize() - 1);

            visualizeRepairedPart(context, mergedRepairedNet.getNet(), repairedNet, stepNumber, allBadParts);
        }
    }

    protected AcceptingPetriNet wrap(PluginContext context, Petrinet entireNet, Set<AcceptingPetriNet> allClusters,
                                            AcceptingPetriNet netToWrap, int stepNumber, Set<AcceptingPetriNet> badParts) {
        AcceptingPetriNetArray biggerCluster = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

        biggerCluster.addNet(netToWrap);
        allClusters.remove(netToWrap);

        Collection<Transition> transitionsOfNetToWrap = netToWrap.getNet().getTransitions();

        List<AcceptingPetriNet> clustersToRemove = new ArrayList<>();

        for (AcceptingPetriNet cluster : allClusters) {
            Petrinet petrinet = cluster.getNet();
            Collection<Transition> transitions = petrinet.getTransitions();

            if (checkIntersection(transitionsOfNetToWrap, transitions)) {
                biggerCluster.addNet(cluster);
                clustersToRemove.add(cluster);
                badParts.remove(cluster);
            }
        }

        allClusters.removeAll(clustersToRemove);

        AcceptingPetriNet mergedBiggerCluster = mergePetriNetArray(context, biggerCluster);
        AcceptingPetriNetArray mergedClusterArray = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();
        mergedClusterArray.addNet(mergedBiggerCluster);

        if (USE_PROVIDED_OBJECTS) {
            AcceptingPetriNetArray array = AcceptingPetriNetArrayFactory.createAcceptingPetriNetArray();

            for (AcceptingPetriNet cluster : allClusters) {
                array.addNet(cluster);
            }

            addHighlightedClustersCopyToProvidedObjects(context, mergedClusterArray, array, Collections.singletonList(netToWrap), stepNumber);

            addNetWithHighlightedClustersToProvidedObjects(context, entireNet, Collections.singletonList(netToWrap), mergedClusterArray, stepNumber);
        }

        return mergedBiggerCluster;
    }

    private AcceptingPetriNet findBiggerClusterWithWorstPart(AcceptingPetriNet worstPart, AcceptingPetriNetArray wrappers) {

        Set<String> labelsOfWorstPart = new HashSet<>();

        for (Transition transition : worstPart.getNet().getTransitions()) {
            labelsOfWorstPart.add(transition.getLabel());
        }

        for (int i = 0; i < wrappers.getSize(); i++) {
            Set<String> labelsOfBadPart = new HashSet<>();

            AcceptingPetriNet acceptingPetriNet = wrappers.getNet(i);
            Petrinet net = acceptingPetriNet.getNet();

            for (Transition transition : net.getTransitions()) {
                labelsOfBadPart.add(transition.getLabel());
            }

            if (labelsOfBadPart.containsAll(labelsOfWorstPart)) {
                return acceptingPetriNet;
            }
        }

        return null;
    }

    protected void visualizeRepairedPart(PluginContext context, Petrinet entireModel, Petrinet repairedPart,
                                         int stepNumber, Collection<AcceptingPetriNet> badParts) {
        Map<String, PetrinetNode> labelsToNodes = new HashMap<>();

        for (PetrinetNode node : entireModel.getTransitions()) {
            labelsToNodes.put(node.getLabel(), node);

            AttributeMap attributeMap = node.getAttributeMap();
            attributeMap.remove(AttributeMap.FILLCOLOR);
        }

        for (PetrinetNode node : repairedPart.getTransitions()) {
            String label = node.getLabel();
            PetrinetNode correspondingNode = labelsToNodes.get(label);

            if (correspondingNode != null) {
                AttributeMap attributeMap = correspondingNode.getAttributeMap();
                attributeMap.put(AttributeMap.FILLCOLOR, Color.YELLOW);
            }
        }

        addPetrinetWithHighlightedBadPartsToProvidedObjects(context, entireModel, badParts, stepNumber, false);
    }
}
