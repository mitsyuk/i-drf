package org.processmining.iskra.plugins;

import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.iskra.hammocks.HammockTree;
import org.processmining.iskra.hammocks.HammockTreeConstructor;
import org.processmining.iskra.hammocks.HammocksFinder;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

/**
 * Created by Ivan on 30.07.2016.
 */
public class HammocksPlugin {

    @Plugin(name = "Find hammocks", returnLabels = "Array of hammocks", returnTypes = AcceptingPetriNetArray.class, parameterLabels = "Petri net")
    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov",
            email = "amitsyuk@hse.ru",
            pack = "DecomposedReplay")
    public AcceptingPetriNetArray findHammocks(UIPluginContext context, Petrinet petrinet) {
        return new HammocksFinder().findAsAcceptingPetriNetArray(petrinet);
    }

    @Plugin(name = "Find all hammocks (including non minimal)", returnLabels = "Array of hammocks", returnTypes = AcceptingPetriNetArray.class, parameterLabels = "Petri net")
    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov",
            email = "amitsyuk@hse.ru",
            pack = "DecomposedReplay")
    public AcceptingPetriNetArray findAllHammocks(UIPluginContext context, Petrinet petrinet) {
        return new HammocksFinder().findAllAsAcceptingPetriNetArray(petrinet);
    }

    @Plugin(name = "Construct hammock tree", returnLabels = "Hammocks tree", returnTypes = HammockTree.class, parameterLabels = "Petri net")
    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov",
            email = "amitsyuk@hse.ru",
            pack = "DecomposedReplay")
    public HammockTree constructHammocksTree(UIPluginContext context, Petrinet petrinet) {
        return new HammockTreeConstructor().constructHammockTree(petrinet);
    }

    @Plugin(name = "Construct hammock tree with quasi hammocks", returnLabels = "Hammocks tree with quasi hammocks", returnTypes = HammockTree.class, parameterLabels = "Petri net")
    @UITopiaVariant(affiliation = "HSE PAIS Lab",
            author = "Alex Mitsyuk, Ivan Shugurov",
            email = "amitsyuk@hse.ru",
            pack = "DecomposedReplay")
    public HammockTree constructHammocksTreeWithQuasiHammocks(UIPluginContext context, Petrinet petrinet) {
        return new HammockTreeConstructor().constructHammockTreeWithQuasiHammocks(petrinet);
    }
}
