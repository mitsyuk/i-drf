package org.processmining.iskra.types;

/**
 * Storage for replay results
 */
public interface DecomposedConformanceResult {

    /**
     * Method for obtaining information about parts. Part is bad if there is a getConformance between part and log
     *
     * @param partIndex part to be checked
     * @return if part with number partIndex is bad
     */
    boolean isBadPart(int partIndex);

    /**
     * measures conformance between part's log and model
     *
     * @param partIndex part to be checked
     * @return conformance rate between part's log and model
     */
    double getConformance(int partIndex);

    /**
     * Stores size of the decomposed model
     *
     * @return size of the decomposed model
     */
    int size();
}
