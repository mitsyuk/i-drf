package org.processmining.iskra.pluginwraps.conformance;

import nl.tue.astar.AStarException;
import org.deckfour.xes.model.XLog;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNet;
import org.processmining.acceptingpetrinet.models.AcceptingPetriNetArray;
import org.processmining.iskra.infrastucture.impl.DecomposedConformanceResultImpl;
import org.processmining.iskra.types.*;
import org.processmining.iskra.types.impl.DecomposedModelImpl;
import org.processmining.log.models.EventLogArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivan on 18.04.2016.
 */
public class DecomposedConformanceChecker {

    ConformanceCheckingUsingAlignments conformanceChecker;
    double fitnessThreshold;
    private Metrics metrics;

    public DecomposedConformanceChecker(Metrics metrics, ConformanceCheckingUsingAlignments conformanceChecker, double fitnessThreshold) {
        this.metrics = metrics;
        this.conformanceChecker = conformanceChecker;
        this.fitnessThreshold = fitnessThreshold;
    }

    public DecomposedConformanceResult measureDecomposedConformance(DecomposedModel model, boolean printResults) throws AStarException, ImpossibilityToMeasureConformanceException {
        List<Double> conformanceList = new ArrayList<>();

        AcceptingPetriNetArray netArray = model.getNetArray();
        EventLogArray logArray = model.getLogArray();

        for (int i = 0; i < netArray.getSize(); i++) {
            AcceptingPetriNet acceptingNet = netArray.getNet(i);
            XLog log = logArray.getLog(i);

            double conformance = measureConformanceOfNewNet(acceptingNet, log);

            if (printResults) {
                System.out.println("Net " + i + " conformance: " + conformance);
            }

            conformanceList.add(conformance);
        }

        return new DecomposedConformanceResultImpl(conformanceList, fitnessThreshold);
    }

    public DecomposedConformanceResult measureDecomposedConformance(AcceptingPetriNetArray netArray,
                                                                    EventLogArray logArray, boolean printResults) throws ImpossibilityToMeasureConformanceException, AStarException {
        DecomposedModel model = new DecomposedModelImpl(netArray, logArray);
        return measureDecomposedConformance(model, printResults);
    }

    protected double measureConformanceOfNewNet(AcceptingPetriNet petriNet, XLog log) throws AStarException, ImpossibilityToMeasureConformanceException {
        double evaluatedConformance;

        if (metrics.measureOnlyFitness()) {
            evaluatedConformance = conformanceChecker.measureFitnessOfNewNet(petriNet, log);
        } else {
            Conformance conformance = conformanceChecker.measureConformanceOfNewNet(petriNet, log);
            evaluatedConformance = metrics.evaluateInitialModel(conformance);
        }

        return evaluatedConformance;
    }
}
