package org.processmining.iskra.types;

import org.processmining.iskra.infrastucture.impl.IskraRepairChainImpl;

import java.util.Map;

/**
 * The basic idea of result of running experiment multiple times
 *
 * @author Dmitriy &lt;Zimy(x)&gt; Yakovlev
 */
public interface AbstractIskraCombinedResult {
    /**
     * Returns result from inner storage
     *
     * @return map chain to IskraResult
     */
    Map<IskraRepairChainImpl, IskraResult> getResults();
}
