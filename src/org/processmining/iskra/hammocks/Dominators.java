package org.processmining.iskra.hammocks;

import org.processmining.models.graphbased.directed.petrinet.elements.Transition;

import java.util.Map;
import java.util.Set;

/**
 * Created by Ivan on 29.07.2016.
 */
public class Dominators {
    private Map<Transition, Set<Transition>> postDominators;
    private Map<Transition, Set<Transition>> preDominators;

    public Dominators(Map<Transition, Set<Transition>> postDominators, Map<Transition, Set<Transition>> preDominators) {
        this.postDominators = postDominators;
        this.preDominators = preDominators;
    }

    public Map<Transition, Set<Transition>> getPostDominators() {
        return postDominators;
    }

    public Map<Transition, Set<Transition>> getPreDominators() {
        return preDominators;
    }
}
