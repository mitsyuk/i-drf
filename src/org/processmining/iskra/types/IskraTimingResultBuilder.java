package org.processmining.iskra.types;

import org.processmining.iskra.infrastucture.IskraTimingResult;

/**
 * Created by Ivan Shugurov on 13.11.2014.
 */
public class IskraTimingResultBuilder {
    private long decompositionBeginning;
    private long decompositionEnd;
    private long partialConformanceCheckingTimeBeginning;
    private long partialConformanceCheckingTimeEnd;
    private long repairTimeBeginning;
    private long repairTimeEnd;
    private long compositionTimeBeginning;
    private long compositionTimeEnd;
    private long finalEvaluationTimeBeginning;
    private long finalEvaluationTimeEnd;

    public IskraTimingResultBuilder() {

    }

    public void decompositionStart() {
        this.decompositionBeginning = getCurrentTime();
    }

    private long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public void decompositionEnd() {
        this.decompositionEnd = getCurrentTime();
    }

    public void partialConformanceCheckingStart() {
        this.partialConformanceCheckingTimeBeginning = getCurrentTime();
    }

    public void partialConformanceCheckingEnd() {
        this.partialConformanceCheckingTimeEnd = getCurrentTime();
    }

    public void repairStart() {
        this.repairTimeBeginning = getCurrentTime();
    }

    public void repairEnd() {
        this.repairTimeEnd = getCurrentTime();
    }

    public void compositionStart() {
        this.compositionTimeBeginning = getCurrentTime();
    }

    public void compositionEnd() {
        this.compositionTimeEnd = getCurrentTime();
    }

    public void finalEvaluationStart() {
        this.finalEvaluationTimeBeginning = getCurrentTime() - (finalEvaluationTimeEnd - finalEvaluationTimeBeginning);
    }

    public void finalEvaluationEnd() {
        this.finalEvaluationTimeEnd = getCurrentTime();
    }

    public IskraTimingResult build() {
        final long decompositionTime = decompositionEnd - decompositionBeginning;
        final long partialConformanceCheckingTime = partialConformanceCheckingTimeEnd - partialConformanceCheckingTimeBeginning;
        final long repairTime = repairTimeEnd - repairTimeBeginning;
        final long compositionTime = compositionTimeEnd - compositionTimeBeginning;
        final long finalEvaluationTime = finalEvaluationTimeEnd - finalEvaluationTimeBeginning;
        final long totalTime = decompositionTime + partialConformanceCheckingTime + repairTime + compositionTime + finalEvaluationTime;
        return new IskraTimingResultImpl(decompositionTime, partialConformanceCheckingTime, repairTime, compositionTime, finalEvaluationTime, totalTime);
    }

    private static class IskraTimingResultImpl implements IskraTimingResult {

        private final long decompositionTime;
        private final long partialConformanceCheckingTime;
        private final long repairTime;
        private final long compositionTime;
        private final long finalEvaluationTime;
        private final long totalTime;

        IskraTimingResultImpl(long decompositionTime, long partialConformanceCheckingTime, long repairTime, long compositionTime, long finalEvaluationTime, long totalTime) {
            this.decompositionTime = decompositionTime;
            this.partialConformanceCheckingTime = partialConformanceCheckingTime;
            this.repairTime = repairTime;
            this.compositionTime = compositionTime;
            this.finalEvaluationTime = finalEvaluationTime;
            this.totalTime = totalTime;
        }

        @Override
        public long getDecompositionTime() {
            return decompositionTime;
        }

        @Override
        public long getPartialConformanceCheckingTime() {
            return partialConformanceCheckingTime;
        }

        @Override
        public long getRepairTime() {
            return repairTime;
        }

        @Override
        public long getCompositionTime() {
            return compositionTime;
        }

        @Override
        public long getFinalEvaluationTime() {
            return finalEvaluationTime;
        }

        @Override
        public long getTotalTime() {
            return totalTime;
        }
    }
}
