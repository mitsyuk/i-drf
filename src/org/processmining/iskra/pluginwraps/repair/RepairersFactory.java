package org.processmining.iskra.pluginwraps.repair;

import org.processmining.iskra.pluginwraps.repair.impl.*;

/**
 * Factory for obtaining instances of repair
 *
 * @author Ivan Shugurov
 */
public class RepairersFactory {
    private RepairersFactory() {
    }

    public static IskraRepairer getAlphaRepairer() {
        return new AlphaRepairer();
    }

    public static IskraRepairer getGeneticRepairer() {
        return new GeneticRepairer();
    }

    public static IskraRepairer getHeuristicsRepairer() {
        return new HeuristicsRepairer();
    }

    public static IskraRepairer getILPRepairer() {
        return new ILPRepairer();
    }

    public static IskraRepairer getInductiveRepairer() {
        return new InductiveRepairer();
    }

    public static IskraRepairer[] getAllRepairers() {
        IskraRepairer[] minerInterfaces = new IskraRepairer[5];
        minerInterfaces[0] = getAlphaRepairer();
        minerInterfaces[1] = getGeneticRepairer();
        minerInterfaces[2] = getHeuristicsRepairer();
        minerInterfaces[3] = getILPRepairer();
        minerInterfaces[4] = getInductiveRepairer();
        return minerInterfaces;
    }
}
