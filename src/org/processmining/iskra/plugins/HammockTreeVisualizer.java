package org.processmining.iskra.plugins;

import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.framework.util.Pair;
import org.processmining.iskra.hammocks.HammockTree;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.jgraph.ProMJGraph;
import org.processmining.models.jgraph.ProMJGraphVisualizer;
import org.processmining.models.jgraph.visualization.ProMJGraphPanel;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.event.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ivan on 04.08.2016.
 */
@Plugin(name = "Visualize hammock tree", returnLabels = "", returnTypes = {JComponent.class}, parameterLabels = "Hammock tree")
@Visualizer
public class HammockTreeVisualizer {
    private ProMJGraphPanel visualizedNet;
    private TreeDialog treeMenu;

    @PluginVariant(requiredParameterLabels = {0})
    public JComponent visualize(UIPluginContext context, final HammockTree hammockTree) {
        ProMJGraphVisualizer visualizer = ProMJGraphVisualizer.instance();
        visualizedNet = visualizer.visualizeGraph(context, hammockTree.getOriginalNet());

        visualizedNet.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(AncestorEvent event) {
                treeMenu = new TreeDialog(hammockTree);
                treeMenu.setVisible(true);
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                treeMenu.setVisible(false);
                treeMenu = null;
            }

            @Override
            public void ancestorMoved(AncestorEvent event) {

            }
        });

        return visualizedNet;
    }

    class TreeDialog extends JFrame {
        private Pair<Petrinet, JPanel> selectedHammock;
        private HammockTree tree;
        private Map<String, PetrinetNode> namesToNodes = new HashMap<>();

        public TreeDialog(HammockTree tree) {
            super("Hammock tree");
            setResizable(false);
            this.tree = tree;

            for (PetrinetNode node : tree.getOriginalNet().getNodes()) {
                namesToNodes.put(node.getLabel(), node);
            }

            JPanel background = createColumn();

            JPanel treeRootRow = createRow();
            background.add(treeRootRow);

            JPanel treeRoot = createNode("Entire net", tree.getOriginalNet());
            treeRootRow.add(treeRoot);

            addLayer(tree.getOriginalNet(), background);

            JScrollPane scrollPane = new JScrollPane(background);
            add(scrollPane);

            pack();
        }

        private void addLayer(Petrinet parentHammock, JPanel parentContainer) {
            Map<Petrinet, Collection<Petrinet>> layers = tree.getLayers();

            if (layers.get(parentHammock) != null) {
                JPanel row = createRow();
                parentContainer.add(row);

                for (Petrinet nextRowHammock : layers.get(parentHammock)) {
                    JPanel hammockColumn = createColumn();
                    row.add(hammockColumn);

                    JPanel node = createNode(nextRowHammock.getLabel(), nextRowHammock);
                    hammockColumn.add(node);

                    addLayer(nextRowHammock, hammockColumn);
                }
            } else {

            parentContainer.add(Box.createVerticalGlue());
            }
        }

        public JPanel createRow() {
            JPanel row = new JPanel();
            row.setLayout(new BoxLayout(row, BoxLayout.X_AXIS));
            return row;
        }

        public JPanel createColumn() {
            JPanel column = new JPanel();
            column.setLayout(new BoxLayout(column, BoxLayout.Y_AXIS));

            column.setBorder(new CompoundBorder(new EmptyBorder(0, 2, 2, 2), new LineBorder(Color.BLACK)));

            return column;
        }

        public JPanel createNode(String title, final Petrinet hammock) {
            final JPanel nodePanel = new JPanel();
            nodePanel.setBorder(new BevelBorder(BevelBorder.RAISED));

            Dimension treeRootDimension = new Dimension(100, 50);
            nodePanel.setMinimumSize(treeRootDimension);
            nodePanel.setMaximumSize(treeRootDimension);
            nodePanel.setPreferredSize(treeRootDimension);

            JLabel rootLabel = new JLabel(title);
            nodePanel.add(rootLabel);

            nodePanel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (selectedHammock == null) {
                        selectHammock(hammock, nodePanel);
                    } else if (selectedHammock.getFirst() == hammock) {
                        deselectCurrentHammock();
                    } else {
                        deselectCurrentHammock();
                        selectHammock(hammock, nodePanel);
                    }
                }
            });

            return nodePanel;
        }

        private void selectHammock(Petrinet hammock, JPanel panel) {
            selectedHammock = new Pair<>(hammock, panel);
            panel.setBorder(new BevelBorder(BevelBorder.LOWERED));

            ProMJGraph graph = visualizedNet.getGraph();
            graph.getModel().beginUpdate();

            for (String nodeName : tree.getNodeNames(hammock)) {
                PetrinetNode node = namesToNodes.get(nodeName);
                node.getAttributeMap().put(AttributeMap.FILLCOLOR, Color.BLUE);
            }

            graph.getModel().endUpdate();
            graph.refresh();
            graph.revalidate();
            visualizedNet.repaint();
        }

        private void deselectCurrentHammock() {
            selectedHammock.getSecond().setBorder(new BevelBorder(BevelBorder.RAISED));

            ProMJGraph graph = visualizedNet.getGraph();
            graph.getModel().beginUpdate();

            for (String nodeName : tree.getNodeNames(selectedHammock.getFirst())) {
                PetrinetNode node = namesToNodes.get(nodeName);

                if (node instanceof Transition && ((Transition) node).isInvisible()) {
                    node.getAttributeMap().put(AttributeMap.FILLCOLOR, Color.BLACK);
                } else {
                    node.getAttributeMap().put(AttributeMap.FILLCOLOR, Color.WHITE);
                }
            }

            graph.getModel().endUpdate();
            graph.refresh();
            graph.revalidate();
            visualizedNet.repaint();

            selectedHammock = null;
        }
    }

}
