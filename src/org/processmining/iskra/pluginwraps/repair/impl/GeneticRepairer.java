package org.processmining.iskra.pluginwraps.repair.impl;

import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XLog;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.plugins.heuristicsnet.miner.genetic.gui.GeneticMinerUIInputParameters;
import org.processmining.plugins.heuristicsnet.miner.genetic.miner.GeneticMiner;
import org.processmining.plugins.heuristicsnet.miner.genetic.miner.settings.GeneticMinerSettings;
import org.processmining.plugins.heuristicsnet.miner.heuristics.converter.HeuristicsNetToPetriNetConverter;

import javax.swing.*;

/**
 * Wrapper for Genetic miner
 *
 * @author Ivan Shugurov
 */
@KeepInProMCache
public class GeneticRepairer extends IskraRepairer {
    private GeneticMinerSettings settings = new GeneticMinerSettings();
    private GeneticMinerUIInputParameters parametersUI;

    public GeneticRepairer() {
        super("Genetic miner");
    }

    @Override
    public Petrinet repair(PluginContext context, XLog log) {
        XLogInfo info = XLogInfoFactory.createLogInfo(log);
        GeneticMiner miner = new GeneticMiner(context, info, settings);
        HeuristicsNet[] minedModels = miner.mine();
        HeuristicsNet bestModel = minedModels[minedModels.length - 1];
        return (Petrinet) HeuristicsNetToPetriNetConverter.converter(context, bestModel)[0];
    }

    @Override
    public JComponent getSettingsComponent() {
        parametersUI = new GeneticMinerUIInputParameters();
        return parametersUI;
    }

    @Override
    public void saveSettings() {
        if (parametersUI != null) {
            settings = parametersUI.getSettings();
        }
    }

    @Override
    public String getComment() {
        String comment = settings.toString();
        comment += "\nstop fitness" + settings.getStopFitness() + "\n";
        return comment;
    }
}
