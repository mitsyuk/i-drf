package org.processmining.iskra.dialogs;

import com.fluxicon.slickerbox.components.NiceIntegerSlider;
import com.fluxicon.slickerbox.components.NiceSlider;
import com.fluxicon.slickerbox.factory.SlickerFactory;
import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstants;
import org.processmining.parameters.dc.DecomposeBySESEsAndBridgingParameters;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Created by user on 07.07.14.
 */
public class SESEDecomposerSettingsPanel extends JPanel {
    public SESEDecomposerSettingsPanel(final DecomposeBySESEsAndBridgingParameters parameters) {
        double size[][] = {{TableLayoutConstants.FILL}, {TableLayoutConstants.FILL, 30}};
        setLayout(new TableLayout(size));
        final NiceIntegerSlider slider = SlickerFactory.instance().
                createNiceIntegerSlider(
                        "Maximal size of the components (%)",
                        1,
                        100,
                        30,
                        NiceSlider.Orientation.HORIZONTAL);
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                parameters.setMaxSize(slider.getValue());
            }
        });
        slider.setOpaque(false);
        add(slider, "0, 0");
    }
}
