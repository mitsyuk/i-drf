package org.processmining.iskra.dialogs;

import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import ru.hse.pais.shugurov.widgets.elements.InputTextElement;
import ru.hse.pais.shugurov.widgets.panels.EmptyPanel;
import ru.hse.pais.shugurov.widgets.panels.SingleChoicePanel;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

/**
 * Created by Иван on 30.06.2014.
 */
public class IskraSettingsPanel extends EmptyPanel {

    private SingleChoicePanel<IskraDecomposer> decomposerPluginsColumn;
    private SingleChoicePanel<IskraRepairer> minerPluginsColumn;
    private InputTextElement thresholdInput;
    private double fitnessThreshold;

    public IskraSettingsPanel(
            double fitnessThreshold, Collection<IskraDecomposer> decomposers, Collection<IskraRepairer> repairers) {
        this(null, null, decomposers, repairers, fitnessThreshold);
    }

    public IskraSettingsPanel(
            IskraDecomposer chosenDecomposerInterface, IskraRepairer iskraRepairer,
            Collection<IskraDecomposer> decomposers, Collection<IskraRepairer> repairers,
            double fitnessThreshold) {
        this.fitnessThreshold = fitnessThreshold;
        if (chosenDecomposerInterface == null || iskraRepairer == null) {
            decomposerPluginsColumn = new SingleChoicePanel<IskraDecomposer>(decomposers);
            minerPluginsColumn = new SingleChoicePanel<IskraRepairer>(repairers);
        } else {
            decomposerPluginsColumn = new SingleChoicePanel<IskraDecomposer>(decomposers, chosenDecomposerInterface);
            minerPluginsColumn = new SingleChoicePanel<IskraRepairer>(repairers, iskraRepairer);
        }

        JPanel header = new JPanel(new GridLayout(1, 2));
        header.setMaximumSize(new Dimension(2000, 200));
        JPanel contentPanel = new JPanel(new GridLayout(1, 2));
        contentPanel.add(decomposerPluginsColumn);
        contentPanel.add(minerPluginsColumn);


        header.add(new JLabel("Decomposition plugins", SwingConstants.CENTER));
        header.add(new JLabel("Repair plugins", SwingConstants.CENTER));
//        JTextPane explanation = new JTextPane();
//        StyledDocument styledDocument = explanation.getStyledDocument();
//        SimpleAttributeSet centerAttribute = new SimpleAttributeSet();
//        StyleConstants.setAlignment(centerAttribute, StyleConstants.ALIGN_CENTER);
//        styledDocument.setParagraphAttributes(0, styledDocument.getLength(), centerAttribute, false);
//        explanation.setText("text area \n another paragraph \n yet another paragraph");
//        explanation.setEditable(false);
//        explanation.setMaximumSize(new Dimension(2000, 2000));
//        explanation.setBackground(new Color(60, 60, 60));
//        explanation.setForeground(TypicalColors.TEXT_COLOR);
        thresholdInput = new InputTextElement("Fitness fitnessThreshold", Double.toString(fitnessThreshold));

        add(header);
        add(contentPanel);
        add(thresholdInput);
        //add(explanation);
    }

    public double getFitnessThreshold() {
        return fitnessThreshold;
    }

    public boolean verify() {
        if (decomposerPluginsColumn.getChosenOption() == null) {
            JOptionPane.showMessageDialog(null, "You have to choose decomposition plugin", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (minerPluginsColumn.getChosenOption() == null) {
            JOptionPane.showMessageDialog(null, "You have to choose mining plugin", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        try {
            fitnessThreshold = Double.parseDouble(thresholdInput.getValue());
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Incorrect fitnessThreshold", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (fitnessThreshold < 0) {
            JOptionPane.showMessageDialog(null, "Threshold must be greater or equal to 0", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (fitnessThreshold > 1) {
            JOptionPane.showMessageDialog(null, "Threshold must be less or equal to 0", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    public IskraDecomposer getChosenDecompositionWrap() {
        return decomposerPluginsColumn.getChosenOption();
    }

    public IskraRepairer getChosenMinerPlugin() {
        return minerPluginsColumn.getChosenOption();
    }

}
