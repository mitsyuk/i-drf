package org.processmining.iskra.plugins;

import org.deckfour.uitopia.api.event.TaskListener;
import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.iskra.dialogs.MetricsPanel;
import org.processmining.iskra.types.Metrics;
import org.processmining.iskra.pluginwraps.IskraFinalEvaluation;
import org.processmining.iskra.pluginwraps.decomposition.IskraDecomposer;
import org.processmining.iskra.pluginwraps.repair.IskraRepairer;
import org.processmining.iskra.types.ComposedModel;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.plugins.connectionfactories.logpetrinet.MappingPanel;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;
import org.processmining.plugins.petrinet.replayer.algorithms.IPNReplayParamProvider;
import org.processmining.plugins.petrinet.replayer.algorithms.IPNReplayParameter;
import org.processmining.plugins.petrinet.replayer.ui.PNAlgorithmStep;
import ru.hse.pais.shugurov.widgets.panels.MultipleChoicePanel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Ivan Shugurov on 08.01.2015.
 */
public class IskraBase {

    protected static final double DEFAULT_FITNESS_THRESHOLD = 1.;
    protected double fitnessThreshold = DEFAULT_FITNESS_THRESHOLD;

    protected UIPluginContext context;
    protected Petrinet petrinet;
    protected XLog log;
    protected Marking initialMarking = new Marking();
    protected Marking finalMarking = new Marking();
    protected IskraDecomposer decomposer;
    protected IskraRepairer repairer;
    protected PNAlgorithmStep algorithmStep;
    protected IPNReplayParameter replayParameter;
    protected TransEvClassMapping mapping;
    protected IskraFinalEvaluation evaluator; //TODO может вообще удалить поле?
    private int markingStep = 0;
    private int replayerConfigurationStep = 0;
    protected Metrics metrics = new Metrics();

    // Create markings for the initial model if there are no ones
    //
    protected TaskListener.InteractionResult specifyMarking() {
        if (markingStep < 0) {
            markingStep = 0;
        }
        if (markingStep > 1) {
            markingStep = 1;
        }

        TaskListener.InteractionResult interactionResult = TaskListener.InteractionResult.CANCEL;

        while (true) {
            switch (markingStep) {
                case 0:
                    interactionResult = specifyInitialMarking();
                    break;
                case 1:
                    interactionResult = specifyFinalMarking();
                    break;
                default:
                    return interactionResult;
            }
            switch (interactionResult) {
                case PREV:
                    markingStep--;
                    break;
                case NEXT:
                    markingStep++;
                    break;
                default:
                    return interactionResult;
            }
        }
    }

    // Creation of an initial marking for the initial model
    //
    protected TaskListener.InteractionResult specifyInitialMarking() {
        Collection<Place> possibleInitialPlaces = new HashSet<Place>();
        possibleInitialPlaces.addAll(petrinet.getPlaces());
        possibleInitialPlaces.removeAll(finalMarking.baseSet());



        MultipleChoicePanel<Place> markingPanel = new MultipleChoicePanel<Place>(possibleInitialPlaces, initialMarking.baseSet());

        while (true) {
            TaskListener.InteractionResult interactionResult = context.showWizard("Initial marking", false, false, markingPanel);
            switch (interactionResult) {
                case NEXT:
                    initialMarking.clear();
                    initialMarking.addAll(markingPanel.getChosenOptionsAsSet());
                    if (initialMarking.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "You have to choose at least one initial place", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        return interactionResult;
                    }
                    break;
                case PREV:
                    initialMarking.clear();
                    initialMarking.addAll(markingPanel.getChosenOptionsAsSet());
                default:
                    return interactionResult;
            }
        }
    }

    protected TaskListener.InteractionResult specifyFinalMarking() {
        Collection<Place> possibleFinalPlaces = new HashSet<Place>();
        possibleFinalPlaces.addAll(petrinet.getPlaces());
        possibleFinalPlaces.removeAll(initialMarking.baseSet());

        MultipleChoicePanel<Place> markingPanel = new MultipleChoicePanel<Place>(possibleFinalPlaces, finalMarking.baseSet());

        while (true) {
            TaskListener.InteractionResult interactionResult = context.showWizard("Final marking", false, false, markingPanel);
            switch (interactionResult) {
                case NEXT:
                    finalMarking.clear();
                    finalMarking.addAll(markingPanel.getChosenOptionsAsSet());
                    if (finalMarking.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "You have to choose at least one final place", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        return interactionResult;
                    }
                    break;
                case PREV:
                    finalMarking.clear();
                    finalMarking.addAll(markingPanel.getChosenOptionsAsSet());
                default:
                    return interactionResult;
            }
        }
    }

    protected TaskListener.InteractionResult configureDecomposerSettings(TaskListener.InteractionResult defaultInteractionResult, ComposedModel composedModel) {
        JComponent settingsComponent = decomposer.getSettingsComponent();
        if (settingsComponent == null) {
            return defaultInteractionResult;
        } else {
            return context.showWizard("Decomposer settings", false, false, settingsComponent);
        }
    }


    protected TaskListener.InteractionResult configureRepairerSettings(TaskListener.InteractionResult defaultInteractionResult) {
        JComponent settingsComponent = repairer.getSettingsComponent();
        if (settingsComponent == null) {
            return defaultInteractionResult;
        } else {
            TaskListener.InteractionResult interactionResult = context.showWizard("Repairer settings", false, false, settingsComponent);
            repairer.saveSettings();
            return interactionResult;
        }
    }

    protected TaskListener.InteractionResult configureReplayer(boolean isLastScreen) {
        if (replayerConfigurationStep < 0) {
            replayerConfigurationStep = 0;
        }
        if (replayerConfigurationStep > 1) {
            replayerConfigurationStep = 1;
        }
        TaskListener.InteractionResult result = TaskListener.InteractionResult.CANCEL;
        while (true) {
            switch (replayerConfigurationStep) {
                case 0:
                    if (algorithmStep == null) {
                        algorithmStep = new PNAlgorithmStep(context, petrinet, log, mapping);
                    }
                    result = context.showWizard("Replay in Petrinet", false, false, algorithmStep);
                    break;
                case 1:
                    IPNReplayParamProvider paramProvider = algorithmStep.getAlgorithm().constructParamProvider(petrinet,
                            log, mapping, initialMarking, new Marking[]{finalMarking});
                    JComponent paramsComponent = paramProvider.constructUI();
                    result = context.showWizard("Replay in Petrinet", false, isLastScreen, paramsComponent);
                    replayParameter = paramProvider.constructReplayParameter(paramsComponent);   //TODO what is it?
                    replayParameter.setInitialMarking(initialMarking);
                    replayParameter.setFinalMarkings(finalMarking);
                    break;
                default:
                    return result;
            }
            switch (result) {
                case NEXT:
                    replayerConfigurationStep++;
                    break;
                case PREV:
                    replayerConfigurationStep--;
                    break;
                default:
                    return result;
            }
        }
    }

    protected TaskListener.InteractionResult configureClassMapping() {
        List<XEventClassifier> classList = new ArrayList<>(log.getClassifiers());
        // add default classifiers
        if (!classList.contains(XLogInfoImpl.RESOURCE_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.RESOURCE_CLASSIFIER);
        }
        if (!classList.contains(XLogInfoImpl.NAME_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.NAME_CLASSIFIER);
        }
        if (!classList.contains(XLogInfoImpl.STANDARD_CLASSIFIER)) {
            classList.add(0, XLogInfoImpl.STANDARD_CLASSIFIER);
        }

        MappingPanel connectionFactoryUI = new MappingPanel(log, petrinet, classList.toArray());
        TaskListener.InteractionResult interactionResult = context.showWizard("Mapping Petrinet - Event Class of Log", false, false, connectionFactoryUI) /*TODO не показываю предупреждения, если не проставлены соответствия для всех переходов*/;

        if (interactionResult == TaskListener.InteractionResult.NEXT || interactionResult == TaskListener.InteractionResult.FINISHED) {
            XLogInfo summary = XLogInfoFactory.createLogInfo(log, connectionFactoryUI.getSelectedClassifier());
            XEventClasses eventClasses = summary.getEventClasses();
            Collection<XEventClass> colEventClasses = new HashSet<XEventClass>(eventClasses.getClasses());
            colEventClasses.removeAll(connectionFactoryUI.getMap().values());
        }
        //TODO что-то странное
        XLogInfo summary = XLogInfoFactory.createLogInfo(log, connectionFactoryUI.getSelectedClassifier());
        XEventClasses eventClasses = summary.getEventClasses();
        Collection<XEventClass> colEventClasses = new HashSet<XEventClass>(eventClasses.getClasses());
        colEventClasses.removeAll(connectionFactoryUI.getMap().values());

        mapping = connectionFactoryUI.getMap();

        return interactionResult;
    }

    protected TaskListener.InteractionResult configureMetrics() {
        boolean isInputCorrect = false;

        MetricsPanel metricsPanel = new MetricsPanel(metrics);
        TaskListener.InteractionResult interactionResult = TaskListener.InteractionResult.CANCEL;

        while (!isInputCorrect) {
            interactionResult = context.showWizard("Metrics settings", false, true, metricsPanel);
            isInputCorrect = metricsPanel.verify();

            switch (interactionResult) {
                case NEXT:
                case FINISHED:
                    if (!isInputCorrect) {
                        JOptionPane.showMessageDialog(null, "Incorrect input occurs", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                default:
                    return interactionResult;
            }
        }

        return interactionResult;
    }

}
